#include <kraft_rrbot_control/Kraft.h>


using namespace ROSKraft;

int main(int argc, char** argv)
{	
  	ros::init(argc, argv, "kraft_shutdown_hydraulics");
  	ros::NodeHandle nh;
	
	ROSKraft::Kraft *robot;
    robot=new ROSKraft::Kraft(nh);
    robot->closedHydraulics();
    return 0;
}