/*
 * Created by SharpDevelop.
 * User: Daniel
 * Date: 1/11/2012
 * Time: 8:28 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Net.Sockets;

namespace SplitSitter
{
	class Program
	{

		
		public static void Main(string[] args)
		{
			int InUDPPort = 51500;
			int EchoUDPPort = 12347;
		
			Console.WriteLine("SubseaSitter Starting!");
			/// hint: nc -uvv 127.0.0.1 12346 to send a packet for testing
			/// 
			
			Kraft kft;
			
			if (args.Length > 0)
				kft = new Kraft(args[0]); // log to this path
			else
				kft = new Kraft(); // no logging
			

			
			kft.Start();
			
			System.Threading.Thread.Sleep(2000); // wait till we have some feedback from arm
			
			
			System.Net.Sockets.UdpClient mmSock;
			System.Net.IPEndPoint mmRecvpt;
			
			// Set up a UDP socket to use for communication with the arm.
			// The UdpClient object is used for both TX and RX.
			// The port specified here filters the recv pkts, also becomes the "source" for TX.
			mmSock = new UdpClient(InUDPPort);
			
			// Create an IPEndPoint to receive messages. A port of '0' means "any available" and does NOT affect the received packets.
			mmRecvpt = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);//InUDPPort);//1501) ;
			// later, we can get new packets with this command:   data = kftSock.Receive(ref kftRecvpt);	
			
			byte[] data;
			
			
			// First packet only, get source IP address.
			data = mmSock.Receive(ref mmRecvpt);
			System.Net.IPAddress LogEchoDestination = mmRecvpt.Address;
			System.Console.WriteLine("Got first Master update from IP address: " + LogEchoDestination.ToString());
			
			// now we can initialize the state-logging echo...
			//Sender EchoSender = new Sender(new System.Net.IPEndPoint(LogEchoDestination, EchoUDPPort));
			Sender EchoSender = new Sender(new System.Net.IPEndPoint(System.Net.IPAddress.Parse("192.168.10.255"), EchoUDPPort));
				
			while (true)
			{
	
				/*
				Console.Write("Got UDP: ");
				foreach (byte b in data)
					Console.Write(b.ToString("X2") + " ");
				Console.WriteLine();
				*/
				
				// NOW DECODE THE DOWNLINK PACKET
			
				if ((data[0]&0x01) == 0x01)
					{
						if (!kft.HydraulicsEnabled)
							Console.WriteLine("Enabling Hydraulics!");
						kft.HydraulicsEnabled = true;
					}
					else
					{
						if (kft.HydraulicsEnabled)
							Console.WriteLine("Disabling Hydraulics!");
						kft.HydraulicsEnabled = false;
					}
				
				//kft.GripClosed = ((data[0]&0x02) == 0x02);
				
				int grip16 = (data[8] & 0xF0) / 16; // grip 4 bits, possible values 0-15
				// center value is 39? higher is close, lower is open.
				// on 4 Mar 2012: 40 is strong handshake, 36 opens slowly.
				// 39 opens very slowly, 40 is stopped, 41 closes extremely slowly.
				// 40 seems to be the most reliable neutral value.
				
				kft.GripForce = 20 + 5*grip16; // this is nice and snappy.
				//kft.GripForce = 30 + grip16; // for testing only, too slow.
				
				//Console.WriteLine("setting gripforce " + kft.GripForce);

				kft.WristSpeed = data[9]; // wrist rotate speed 8 bits
				kft.Target[0] = data[1]+((data[2]&0x0F)*256); // az
				kft.Target[1] = data[3]+((data[2]&0xF0)*16); // elevator
				kft.Target[2] = data[4]+((data[5]&0x0F)*256); // elbow
				kft.Target[3] = data[6]+((data[5]&0xF0)*16); // pitch
				kft.Target[4] = data[7]+((data[8]&0x0F)*256); // yaw
				//Note there are seven unused spare bits in data[0], bits 1-7 (only the first bit, bit 0, is used.
				
				

				// Echo back a subsea state packet.
				// Note 'data' is overwritten.
				data = System.Text.Encoding.ASCII.GetBytes("KSS " + kft.LogString);
				EchoSender.Send(data);
				
				// Get the next packet
				data = mmSock.Receive(ref mmRecvpt);
			}
			
			//Console.Write("Press any key to continue . . . ");
			//Console.ReadKey(true);
			//kft.Dispose();
		}
	}
}