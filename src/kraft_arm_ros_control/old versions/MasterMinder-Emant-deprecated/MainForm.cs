﻿/*
 * Created by SharpDevelop.
 * User: dgi
 * Date: 10/19/2011
 * Time: 11:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SplitSitter;

namespace SplitSitter
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		TrackBar[] AxisBar = new TrackBar[7];
		Label[] AxisLabel = new Label[7];
		
		SplitSitter.MiniMaster mm;
		
		//int[] SafePosition = new int[] {1,1,1,1,1,1,1}; // fill this in!
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//this.SuspendLayout();
			
			// remove this panel so it shows up at the top.
			this.Controls.Remove(this.groupBox1);
			
			for (int i = 0; i<AxisBar.Length; i++)
			{
				AxisBar[i] = new TrackBar();
				AxisLabel[i] = new Label();
				AxisBar[i].Dock = System.Windows.Forms.DockStyle.Top;
				AxisLabel[i].Dock = System.Windows.Forms.DockStyle.Top;
				AxisBar[i].Tag = i;
				AxisLabel[i].Text = MiniMaster.JointNames[i];
				AxisBar[i].TabIndex = 4+i;
				this.Controls.Add(AxisBar[i]);
				this.Controls.Add(AxisLabel[i]);
				AxisBar[i].Minimum = MiniMaster.CommandMin[i];
				AxisBar[i].Maximum = MiniMaster.CommandMaximum[i];
				AxisBar[i].Value = MiniMaster.CommandCenter[i];
				AxisBar[i].TickFrequency = 100;
			}
			
			// add this last so they show up at the top
			this.Controls.Add(this.groupBox1);
			
			this.PerformLayout();
			
			mm = new MiniMaster(); // now ready to GetAnalogs()
			
			
			timer1.Enabled = true; // start updating GUI
			
			// spawn new process to get analogs, update GUI, and emit UDPs
						
		}
		
				
		void Timer1Tick(object sender, EventArgs e)
		{
			checkBoxEnable.Checked = mm.HydEnabled;
			checkBoxIndex.Checked = mm.IndexActive;
			
			for (int i=0; i<6; i++)
			{
				AxisLabel[i].Text = MiniMaster.JointNames[i] + " = " + AxisBar[i].Value;
				AxisBar[i].Value = mm.CumulativePosition[i];
			}
			if (mm.GripClosed)
				AxisBar[6].Value = MiniMaster.CommandMaximum[6];
			else
				AxisBar[6].Value = MiniMaster.CommandMin[6];
			//this.Text = pots[5].ToString();
		}
		
		
		
		protected override void OnClosed(EventArgs e)
		{
			// this is not necessary for Windows since Thread.IsBackground works as intended. 
			// But maybe on Mono it will be necessary to use OnClosed.
			//if (kft != null)
			//{
			//	kft.Dispose();
			//	kft=null;
			//}
		    base.OnClosed(e);
		}
		

		

	}
}
