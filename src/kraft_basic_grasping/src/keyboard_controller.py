#!/usr/bin/env python
import rospy
import sys
import getch
from std_msgs.msg import Float64, Float64MultiArray
from sensor_msgs.msg import JointState
from copy import deepcopy

JOINTS = ['Slew', 'Shoulder', 'Elbow', 'Wrist_Pitch', 'Wrist_Yaw', 'Grip_Roll']

class KeyboardController(object):

    def __init__(self, **kwargs):
        self.pub = rospy.Publisher('/rrbot/joint_position_controller/command', Float64MultiArray, queue_size=10)
        self.sub = rospy.Subscriber('/rrbot/joint_states', JointState, self.handle_joint_states, tcp_nodelay=True, queue_size=10)
        self.current_joint_states = []
        self.des_joint_states = []
        self.delta = 0.02
        self.rate = rospy.Rate(100)
        self.init = False

    def run(self):
        print('Controls: Slew <u,j>, Shoulder <i,k>, Elbow <o,l>, Wrist_Pitch <e,d>, Wrist_Yaw <r,f>, Wrist_Roll <t,g>, Gripper <y,h>')
        exit = False
        while not exit and not rospy.is_shutdown():
            if self.init:
                char = getch.getch()
                print(char)
                if (char == 'q'):
                    exit = True
                # Slew
                elif (char == 'u'):
                    self.des_joint_states[0] += self.delta
                elif (char == 'j'):
                    self.des_joint_states[0] -= self.delta
                # Shoulder
                elif (char == 'i'):
                    self.des_joint_states[1] += self.delta
                elif (char == 'k'):
                    self.des_joint_states[1] -= self.delta
                # Elbow
                elif (char == 'o'):
                    self.des_joint_states[2] += self.delta
                elif (char == 'l'):
                    self.des_joint_states[2] -= self.delta
                # Wrist_Pitch
                elif (char == 'e'):
                    self.des_joint_states[3] += self.delta
                elif (char == 'd'):
                    self.des_joint_states[3] -= self.delta
                # Wrist_Yaw
                elif (char == 'r'):
                    self.des_joint_states[4] += self.delta
                elif (char == 'f'):
                    self.des_joint_states[4] -= self.delta
                # Wrist_Roll
                elif (char == 't'):
                    self.des_joint_states[5] += self.delta
                elif (char == 'g'):
                    self.des_joint_states[5] -= self.delta
                # Gripper open close
                elif (char == 'y'):
                    self.des_joint_states[6] = 1
                elif (char == 'h'):
                    self.des_joint_states[6] = 0
                else:
                    self.rate.sleep()
                    continue
                msg = Float64MultiArray()
                if len(self.des_joint_states) == 7:
                    self.des_joint_states = self.des_joint_states[:6]
                msg.data = self.des_joint_states
                print("Current: {}".format(self.current_joint_states))
                print("Desired: {}".format(self.des_joint_states))
                self.pub.publish(msg)

            self.rate.sleep()

    def handle_joint_states(self, data):
        fb = {}
        for i, name in enumerate(data.name):
            fb[name] = data.position[i]
        self.current_joint_states = [fb[x] for x in JOINTS]
        if not self.init:
            self.des_joint_states = deepcopy(self.current_joint_states)
            self.init = True


def main():
    # ROS_WARNING("begining testing ****************************************************************");
    try:
        rospy.init_node('keyboard_controller', anonymous=True)
        print("here")
        controller = KeyboardController()
        controller.run()
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == '__main__':
    main()