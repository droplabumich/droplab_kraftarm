﻿/*
 * Created by SharpDevelop.
 * User: dgi
 * Date: 10/19/2011
 * Time: 11:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SplitSitter
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBoxIndex = new System.Windows.Forms.CheckBox();
			this.checkBoxEnable = new System.Windows.Forms.CheckBox();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxIndex);
			this.groupBox1.Controls.Add(this.checkBoxEnable);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(264, 108);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Status Bits";
			// 
			// checkBoxIndex
			// 
			this.checkBoxIndex.Location = new System.Drawing.Point(38, 53);
			this.checkBoxIndex.Name = "checkBoxIndex";
			this.checkBoxIndex.Size = new System.Drawing.Size(146, 24);
			this.checkBoxIndex.TabIndex = 3;
			this.checkBoxIndex.Text = "Index";
			this.checkBoxIndex.UseVisualStyleBackColor = true;
			// 
			// checkBoxEnable
			// 
			this.checkBoxEnable.Location = new System.Drawing.Point(38, 23);
			this.checkBoxEnable.Name = "checkBoxEnable";
			this.checkBoxEnable.Size = new System.Drawing.Size(146, 24);
			this.checkBoxEnable.TabIndex = 2;
			this.checkBoxEnable.Text = "Enable Hydraulics";
			this.checkBoxEnable.UseVisualStyleBackColor = true;
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(264, 602);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainForm";
			this.Text = "MasterMinder";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox checkBoxEnable;
		private System.Windows.Forms.CheckBox checkBoxIndex;
		
		

	}
}
