;; Auto-generated. Do not edit!


(when (boundp 'mocap_optitrack::RigidBodyMsg)
  (if (not (find-package "MOCAP_OPTITRACK"))
    (make-package "MOCAP_OPTITRACK"))
  (shadow 'RigidBodyMsg (find-package "MOCAP_OPTITRACK")))
(unless (find-package "MOCAP_OPTITRACK::RIGIDBODYMSG")
  (make-package "MOCAP_OPTITRACK::RIGIDBODYMSG"))

(in-package "ROS")
;;//! \htmlinclude RigidBodyMsg.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass mocap_optitrack::RigidBodyMsg
  :super ros::object
  :slots (_header _id _name _tracking_valid _mrk_mean_error _pose _markers ))

(defmethod mocap_optitrack::RigidBodyMsg
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:id __id) 0)
    ((:name __name) "")
    ((:tracking_valid __tracking_valid) nil)
    ((:mrk_mean_error __mrk_mean_error) 0.0)
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    ((:markers __markers) (let (r) (dotimes (i 0) (push (instance geometry_msgs::Point :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _id (round __id))
   (setq _name (string __name))
   (setq _tracking_valid __tracking_valid)
   (setq _mrk_mean_error (float __mrk_mean_error))
   (setq _pose __pose)
   (setq _markers __markers)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:tracking_valid
   (&optional __tracking_valid)
   (if __tracking_valid (setq _tracking_valid __tracking_valid)) _tracking_valid)
  (:mrk_mean_error
   (&optional __mrk_mean_error)
   (if __mrk_mean_error (setq _mrk_mean_error __mrk_mean_error)) _mrk_mean_error)
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:markers
   (&rest __markers)
   (if (keywordp (car __markers))
       (send* _markers __markers)
     (progn
       (if __markers (setq _markers (car __markers)))
       _markers)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int32 _id
    4
    ;; string _name
    4 (length _name)
    ;; bool _tracking_valid
    1
    ;; float64 _mrk_mean_error
    8
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ;; geometry_msgs/Point[] _markers
    (apply #'+ (send-all _markers :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int32 _id
       (write-long _id s)
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; bool _tracking_valid
       (if _tracking_valid (write-byte -1 s) (write-byte 0 s))
     ;; float64 _mrk_mean_error
       (sys::poke _mrk_mean_error (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;; geometry_msgs/Point[] _markers
     (write-long (length _markers) s)
     (dolist (elem _markers)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int32 _id
     (setq _id (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _tracking_valid
     (setq _tracking_valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float64 _mrk_mean_error
     (setq _mrk_mean_error (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;; geometry_msgs/Point[] _markers
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _markers (let (r) (dotimes (i n) (push (instance geometry_msgs::Point :init) r)) r))
     (dolist (elem- _markers)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get mocap_optitrack::RigidBodyMsg :md5sum-) "1948c36024f60d1e6aebe1e39ed81013")
(setf (get mocap_optitrack::RigidBodyMsg :datatype-) "mocap_optitrack/RigidBodyMsg")
(setf (get mocap_optitrack::RigidBodyMsg :definition-)
      "# Optitrack rigid body
Header                header
int32                 id
string                name
bool                  tracking_valid
float64               mrk_mean_error
geometry_msgs/Pose    pose
geometry_msgs/Point[] markers

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :mocap_optitrack/RigidBodyMsg "1948c36024f60d1e6aebe1e39ed81013")


