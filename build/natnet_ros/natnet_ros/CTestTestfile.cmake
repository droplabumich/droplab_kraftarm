# CMake generated Testfile for 
# Source directory: /home/kraft/workspace/droplab_kraftarm/src/natnet_ros/natnet_ros
# Build directory: /home/kraft/workspace/droplab_kraftarm/build/natnet_ros/natnet_ros
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_natnet_ros_rostest_test_client.test "/home/kraft/workspace/droplab_kraftarm/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/kraft/workspace/droplab_kraftarm/build/test_results/natnet_ros/rostest-test_client.xml" "--return-code" "/opt/ros/kinetic/share/rostest/cmake/../../../bin/rostest --pkgdir=/home/kraft/workspace/droplab_kraftarm/src/natnet_ros/natnet_ros --package=natnet_ros --results-filename test_client.xml --results-base-dir \"/home/kraft/workspace/droplab_kraftarm/build/test_results\" /home/kraft/workspace/droplab_kraftarm/src/natnet_ros/natnet_ros/test/client.test ")
