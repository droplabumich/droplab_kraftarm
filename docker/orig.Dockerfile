FROM ros:indigo-perception

# # install GLX-Gears
# RUN apt-get update && apt-get install -y \
#     mesa-utils && \
#     rm -rf /var/lib/apt/lists/*

# nvidia-docker hooks
LABEL com.nvidia.volumes.needed="nvidia_driver"
ENV PATH /usr/local/nvidia/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}

# install ros packages
RUN apt-get update \
    && echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc \
    && apt-get -y install ros-indigo-moveit \
    && apt-get -y install ros-indigo-ros-control \
    && apt-get -y install ros-indigo-ros-controllers \
    && apt-get -y install ros-indigo-rosparam-shortcuts \
    && apt-get -y install ros-indigo-robot-state-publisher \
    && apt-get -y install ros-indigo-diagnostic-updater \
    && apt-get -y install ros-indigo-self-test \
    && apt-get -y install libgflags-dev

WORKDIR /home