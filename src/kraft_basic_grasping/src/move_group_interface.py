#!/usr/bin/env python
## BEGIN_SUB_TUTORIAL imports
##
## To use the python interface to move_group, import the moveit_commander
## module.  We also import rospy and some messages that we will use.
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import numpy as np
import time
import getch
import tf
from pyquaternion import Quaternion
from geometry_msgs.msg import PoseStamped, Pose, Quaternion
from std_msgs.msg import Int8
from sensor_msgs.msg import JointState
from visualization_msgs.msg import Marker
from moveit_msgs.msg import Constraints, JointConstraint, OrientationConstraint
from moveit_commander.conversions import pose_to_list

TOPIC_DTP = 'kraft_arm/move_group/display_planned_path'
TOPIC_POSE = "kraft_arm/des_pose" # topics for desired pose
TOPIC_POSE_DISP = "kraft_arm/pose_display"
TOPIC_DES_JOINTS = "kraft_arm/des_joint" # topic for desired joint position
TOPIC_DEF_POS = "kraft_arm/def_pos" # topic for pre-defined actions
TOPIC_GRIPPER = "kraft_arm/gripper"
TOPIC_CURREN_POSE = "kraft_arm/ee_pose"

noDOF=6 # variable discribing number of joints
ACTION = 0 #zero = stay at current position; one = go to stow position


from std_msgs.msg import String

def all_close(goal, actual, tolerance):
  """
  Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
  @param: goal       A list of floats, a Pose or a PoseStamped
  @param: actual     A list of floats, a Pose or a PoseStamped
  @param: tolerance  A float
  @returns: bool
  """
  all_equal = True
  if type(goal) is list:
    for index in range(len(goal)):
      if abs(actual[index] - goal[index]) > tolerance:
        return False

  elif type(goal) is PoseStamped:
    return all_close(goal.pose, actual.pose, tolerance)

  elif type(goal) is Pose:
    return all_close(pose_to_list(goal), pose_to_list(actual), tolerance)

    return True


class ManipulationClass(object):

    def __init__(self, **kwargs):
        # self.name = name
        # self.rate = rate

        self.topic_output = kwargs.get('topic_output', TOPIC_POSE)

        # timing
        # self.dt = 1.0 / self.rate
        # self.loop = rospy.Rate(self.rate)

        #desired posed
        self.pose = PoseStamped()
        self.pose.pose.position.x = 0.7922
        self.pose.pose.position.y = -0.5546
        self.pose.pose.position.z = 0.35
        self.pose.pose.orientation.x = -0.6287 #, 0, 0.6896592, 0.6448485
        self.pose.pose.orientation.y = 0.147337521
        self.pose.pose.orientation.z = 0.752134
        self.pose.pose.orientation.w = 0.13155

        self.open_gripper = 0

        self.des_joint = np.zeros(noDOF)
        #defined action
        self.action = ACTION
        self.index = 0

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

        ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
        ## the robot:
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
        ## to the world surrounding the robot:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to one group of joints.  In this case the group is the joints in the Panda
        ## arm so we set ``group_name = panda_arm``. If you are using a different robot,
        ## you should change this value to the name of your robot arm planning group.
        ## This interface can be used to plan and execute motions on the Panda:
        group_name = "arm"
        group = moveit_commander.MoveGroupCommander(group_name)

        ## We create a `DisplayTrajectory`_ publisher which is used later to publish
        ## trajectories for RViz to visualize:
        display_trajectory_publisher = rospy.Publisher(TOPIC_DTP, moveit_msgs.msg.DisplayTrajectory, queue_size=20)

        # We can get the name of the reference frame for this robot:
        planning_frame = group.get_planning_frame()
        print "============ Reference frame: %s" % planning_frame

        # We can also print the name of the end-effector link for this group:
        eef_link = group.get_end_effector_link()
        print "============ End effector: %s" % eef_link

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print "============ Robot Groups:", robot.get_group_names()

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print "============ Printing robot state"
        print robot.get_current_state()

        # import pdb; pdb.set_trace()

        ## END_SUB_TUTORIAL

        # Misc variables
        self.box_name = ''
        self.robot = robot
        self.scene = scene
        self.group = group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names


        # ros interface
        self.sub_des_pose = rospy.Subscriber(TOPIC_POSE, PoseStamped, self.handle_des_pose, tcp_nodelay=True, queue_size=10)
        # subscriber for desired joints
        self.sub_des_joint = rospy.Subscriber(TOPIC_DES_JOINTS, JointState, self.handle_des_joints, tcp_nodelay=True, queue_size=10)
        # subscriber for defined position
        self.sub_def_pos = rospy.Subscriber(TOPIC_DEF_POS, Int8, self.handle_def_pos, tcp_nodelay=True,queue_size=10)
        self.pub_rviz_marker = rospy.Publisher(TOPIC_POSE_DISP, Marker, tcp_nodelay=True, queue_size=100)
        self.pub_gripper = rospy.Publisher(TOPIC_GRIPPER, Int8, tcp_nodelay=True, queue_size=100)
        self.pub_ee_pose = rospy.Publisher(TOPIC_CURREN_POSE, PoseStamped, tcp_nodelay=True, queue_size=100)
	
    # function handle for desired position
    def handle_des_pose(self, data):
        self.pose.pose.position.x = data.pose.position.x
        self.pose.pose.position.y = data.pose.position.y
        self.pose.pose.position.z = data.pose.position.z
        self.pose.pose.orientation.x = data.pose.orientation.x
        self.pose.pose.orientation.y = data.pose.orientation.x
        self.pose.pose.orientation.z = data.pose.orientation.z
        self.pose.pose.orientation.w = data.pose.orientation.w
    
    # function handle for desired joint position
    def handle_des_joints(self, data):
            if data.name[0] == 'Slew':
                self.des_joint[0] = data.position[0]
                self.index = 0
            if data.name[0] == 'Shoulder':
                self.des_joint[1] = data.position[0]
                self.index = 1
            if data.name[0] == 'Elbow':
                self.des_joint[2] = data.position[0]
                self.index = 2
            if data.name[0] == 'Wrist_Pitch':
                self.des_joint[3] = data.position[0]
                self.index = 3
            if data.name[0] == 'Wrist_Yaw':
                self.des_joint[4] = data.position[0]
                self.index = 4
            if data.name[0] == 'Grip_Roll':
                self.des_joint[5] = data.position[0]
                self.index = 5

    def set_des_joints_from_user(self):
        print('Current joint values: {}'.format(self.group.get_current_joint_values()))
        print('Enter comma separated list of the {} target joint values. Then press Enter ...'.format(len(self.des_joint)))
        userinput = raw_input()
        target = [float(x.strip()) for x in userinput.split(',')]
        if len(target) != len(self.des_joint):
            print("Must input values for {} joints".format(len(self.des_joint)))
            return
        self.des_joint = target
        self.go_to_joint_angles()

    def keyboard_controller(self):
        print('Controls: Slew <u,j>, Shoulder <i,k>, Elbow <o,l>, Wrist_Pitch <e,d>, Wrist_Yaw <r,f>')
        self.des_joint = self.group.get_current_joint_values()
        exit = False
        delta = 0.1
        while ~exit:
            char = getch.getch()
            if (char == 'q'):
                exit = True
            # Slew
            elif (char == 'u'):
                self.des_joint[0] += delta
            elif (char == 'j'):
                self.des_joint[0] -= delta
            # Shoulder
            if (char == 'i'):
                self.des_joint[0] += delta
            elif (char == 'k'):
                self.des_joint[0] -= delta
            # Elbow
            if (char == 'o'):
                self.des_joint[0] += delta
            elif (char == 'l'):
                self.des_joint[0] -= delta
            # Wrist_Pitch
            if (char == 'e'):
                self.des_joint[0] += delta
            elif (char == 'd'):
                self.des_joint[0] -= delta
            # Wrist_Yaw
            if (char == 'r'):
                self.des_joint[0] += delta
            elif (char == 'f'):
                self.des_joint[0] -= delta
            self.go_to_joint_angles()


    # function handle for desired action position 
    def handle_def_pos(self, data):
        self.action = data.data

    def send_gripper_command(self):
        self.gripper = Int8()
        self.gripper.data = self.open_gripper
        self.pub_gripper.publish(self.gripper)

    def send_ee_pose(self, current_pose):
        self.curren_ee_pose = PoseStamped()
        self.curren_ee_pose.header.stamp = rospy.get_rostime()
        self.curren_ee_pose.pose = current_pose
        self.pub_ee_pose.publish(self.curren_ee_pose)

    # Execute action: stay in current position
    def stay(self):
        group = self.group
        joint_values = group.get_current_joint_values()
        print "joint_values",joint_values
        group.set_goal_joint_tolerance(0.3)
        group.go(joint_values,wait=True)
        group.stop()
        current_joints = self.group.get_current_joint_values()
        return all_close(joint_values, current_joints, 0.01)
    # Execute action: go to stow position
    def stow(self):
        group = self.group
        joint_values = group.get_current_joint_values()
        joint_values[0] = 0.0
        joint_values[1] = -0.1
        joint_values[2] = .1
        joint_values[3] = 0
        joint_values[4] = -0.1
        # joint_values[5] = 0.0
        group.go(joint_values, wait=True)
        group.stop()

        current_joints = self.group.get_current_joint_values()

        return all_close(joint_values, current_joints, 0.01)
    # choose action stay or stow
    def choose_action(self):
        self.stow()
        # if self.action == 0:
        #     self.stay()
        # elif self.action == 1:
        #     self.stow()
    # go to a specified joint angle
    def go_to_joint_angles(self):
        group = self.group
        joint_values = group.get_current_joint_values()
        for i in range(noDOF):
            joint_values[i] = self.des_joint[i]
        group.set_goal_joint_tolerance(0.1) #make tolerance bigger if arm not responding

        group.go(joint_values, wait=True)
        group.stop()

        current_joints = self.group.get_current_joint_values()

        return all_close(joint_values, current_joints, 0.01)

    def go_to_specific_joint(self):
        group = self.group
        joint_values = group.get_current_joint_values()
        joint_values[self.index] = self.des_joint[self.index]
        group.set_goal_joint_tolerance(0.1)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

        current_joints = self.group.get_current_joint_values()

        return all_close(joint_values, current_joints, 0.01)


    def go_to_pose_goal(self):
        group = self.group
        ## Planning to a Pose Goal
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.x += 0.1
        group.set_pose_target(pose_goal)
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        #group.set_goal_tolerance(0.35)
        #group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose

        self.send_ee_pose(current_pose)

        return plan, all_close(pose_goal, current_pose, 0.01)

    # def open_close_grasp(self):

    def demo_kitty_go_to(self):
        group = self.group
        group.set_start_state_to_current_state()
        #pose_goal = group.get_current_pose()
        pose_goal = Pose()
        pose_goal.orientation.w = 1
        pose_goal.position.x = self.pose.pose.position.x
        pose_goal.position.y = self.pose.pose.position.y
        pose_goal.position.z = self.pose.pose.position.z - 0.3
        pose_goal.orientation.x = 0
        pose_goal.orientation.y = 0
        pose_goal.orientation.z = 0

        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.43)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)


    def demo_kitty_open_gripper(self):
        self.open_gripper = 1
        self.send_gripper_command()

    def demo_kitty_close_gripper(self):
        self.open_gripper = 0
        self.send_gripper_command()

    def demo_kitty_follow_the_sequence(self):
        group = self.group
        # move up
        print "Step 4.1 Move up 0.2"
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.z = pose_goal.pose.position.z + 0.3
        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)

        #time.sleep(3)

        #move in x
        print "Step 4.2 Move in x 0.4"
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.x = pose_goal.pose.position.x + 0.5
        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)


        #time.sleep(3)
        print "Step 4.3 Move down 0.2 and release"
        group = self.group
        # move up
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.z = pose_goal.pose.position.z - 0.3
        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)

        #time.sleep(3)

        print "Step 4.4 Move up 0.2"
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.z = pose_goal.pose.position.z + 0.3
        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)

#        time.sleep(3)

        print "Step 4.5 Move in y 0.4"
        # move in y
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.y = pose_goal.pose.position.y + 0.5
        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)

#        time.sleep(3)
        print "Step 4.3 Move down 0.2"
        # move up
        group.set_start_state_to_current_state()
        pose_goal = group.get_current_pose()
        pose_goal.pose.position.z = pose_goal.pose.position.z - 0.2
        group.set_pose_target(pose_goal)
        group.set_start_state_to_current_state()
        group.set_pose_reference_frame(self.planning_frame)
        group.allow_replanning(True)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        # group.set_goal_tolerance(0.35)
        # group.set_planner_id("PRMstarkConfigDefault") #working planners: ESTkConfigDefault, RRTstarkConfigDefault PRMstarkConfigDefault
        group.set_planning_time(10)
        ## Now, we call the planner to compute the plan and execute it.
        plan = group.go(wait=True)
        group.stop()
        group.clear_pose_targets()
        current_pose = self.group.get_current_pose().pose
        self.send_ee_pose(current_pose)

    def demo_kitty_take2(self):
        # print "Close gripper"
        #self.demo_kitty_close_gripper()
        #time.sleep(1)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[2] = 1.2472 #1.0247
        joint_values[3] = 0.7853
        joint_values[0] = 0.7853
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

    def demo_kitty_take3(self):
        self.demo_kitty_take2()
 #       time.sleep(2)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[1] = -1.01161
        joint_values[3] = 0.649
        joint_values[4] = -0.1
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

    def demo_kitty_take4(self):
        self.demo_kitty_take3()
  #      time.sleep(6)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[1] = -0.75
        joint_values[0] = 0.4
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

    def demo_kitty_take5(self):
        self.demo_kitty_take4()
   #     time.sleep(2)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[1] = -1.01161
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

    def demo_kitty_take6(self):
        self.demo_kitty_take5()
    #    time.sleep(2)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[1] = -0.75
        joint_values[0] = 0.6
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

    def demo_kitty_take7(self):
        self.demo_kitty_take6()
     #   time.sleep(2)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[1] = -1.01161
        joint_values[0] = 0.4
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()

    def demo_kitty_take8(self):
        self.demo_kitty_take7()
      #  time.sleep(2)
        group = self.group
        joint_values = group.get_current_joint_values()
        group.set_start_state_to_current_state()
        joint_values[1] = 0.0
        group.set_goal_joint_tolerance(0.01)  # make tolerance bigger if arm not responding
        group.go(joint_values, wait=True)
        group.stop()


        current_joints = self.group.get_current_joint_values()


    def demo_kitty(self):
        # print "STEP1. Open gripper"
        # self.demo_kitty_open_gripper()
        # time.sleep(3)
        print "STEP2. GO AND GRAB OBJECT"
        self.demo_kitty_go_to()
       # time.sleep(5)
        print "STEP3. Close gripper"
        self.demo_kitty_close_gripper()
       # time.sleep(3)
        print "STEP4. Follow sequence"
        self.demo_kitty_follow_the_sequence()

    def plan_cartesian_path(self, scale=1):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.x = self.pose.pose.position.x
        wpose.position.y = self.pose.pose.position.y  # First move up (z)
        wpose.position.z = self.pose.pose.position.z  # and sideways (y)
        wpose.orientation.w = self.pose.pose.orientation.w
        wpose.orientation.x = self.pose.pose.orientation.x
        wpose.orientation.y = self.pose.pose.orientation.y
        wpose.orientation.z = self.pose.pose.orientation.z
        waypoints.append(copy.deepcopy(wpose))

        # robot:
        print "============ Desired pose x", wpose.position.x

        #print self.robot.get_current_state()

       # wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
        wpose.position.x += 0.01
        waypoints.append(copy.deepcopy(wpose))
        print "============ Desired pose x", waypoints[-2].position.x

        wpose.position.y -=  0.01  # Third move sideways (y)
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
            0.01,  # eef_step
            0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        return plan, fraction

    def plan_cartesian_restricted(self):
        group = self.group
        wpose = group.get_current_pose().pose
        # Define constraints 
        self.upright_constraints = Constraints()
        #1. Constraints to the joint
        joint_constraint = JointConstraint()
        
        joint_constraint.position = 0.0
        joint_constraint.tolerance_above = 0.1
        joint_constraint.tolerance_below =-0.1
        joint_constraint.weight = 1
        joint_constraint.joint_name = "Wrist_Pitch"



        #2. Constraints to end-effector orientation
        orientation_constraint = OrientationConstraint()
        #orientation_constraint.header = wpose.header
        orientation_constraint.link_name = group.get_end_effector_link()
        orientation_constraint.orientation =  Quaternion(0,0,0,1)
        orientation_constraint.absolute_x_axis_tolerance = 0.1
        orientation_constraint.absolute_y_axis_tolerance = 0.1
        orientation_constraint.absolute_z_axis_tolerance = 0.1
        orientation_constraint.weight = 1

        self.upright_constraints.name = "upright_movement"
        self.upright_constraints.joint_constraints.append(joint_constraint)
        self.upright_constraints.orientation_constraints.append(orientation_constraint)


        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.01)
        group.set_start_state_to_current_state()
        group.set_path_constraints(self.upright_constraints) 


        waypoints = []
        wpose.position.x += 0.5 
        wpose.position.z += 0.2
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        rospy.sleep(2)
        self.execute_plan(plan)
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        

        # Note: We are just planning, not asking move_group to actually move the robot yet:

        return plan, fraction

    def wait_for_state_update(self, box_name, scene, box_is_known=False, box_is_attached=False, timeout=4):
    
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            attached_objects = scene.get_attached_objects([box_name])
            is_attached = len(attached_objects.keys()) > 0

            is_known = box_name in scene.get_known_object_names()

            if (box_is_attached == is_attached) and (box_is_known == is_known):
                    print "Passed test"
                    return True

        rospy.sleep(0.1)
        seconds = rospy.get_time()

        return False

    def plan_cartesian_object_in_scene(self):
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group = self.group
        timeout = 5

        box_pose = PoseStamped()
        box_pose.header.frame_id = "Inner_Grip"
        box_pose.pose.orientation.w = 1.0
        box_name = "box"
        self.wait_for_state_update(box_name, scene, box_is_known=False, timeout=timeout)
        print group.get_end_effector_link()

        scene.add_box(box_name, box_pose, size=(0.1, 0.1, 0.1))
        grasping_group = 'arm'
        touch_links = robot.get_link_names(group=grasping_group)
        scene.attach_box(group.get_end_effector_link(), box_name, touch_links=touch_links)
        self.wait_for_state_update(box_name, scene, box_is_attached=True, box_is_known=False, timeout=timeout)

        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.01)
        group.set_start_state_to_current_state()

        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.x += 0.5 
        wpose.position.z += 0.2
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        rospy.sleep(2)
        self.execute_plan(plan)
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        

    def plan_cartesian_waypoint_x_negative(self):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.x -= 0.2 # First move up (z)
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:

        return plan, fraction

    def plan_cartesian_waypoint_y_negative(self):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.y -= 0.2
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_waypoint_x_positive(self):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.x += 0.2 # First move up (z)
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_waypoint_y_positive(self):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.y += 0.2
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_waypoint_z_down(self):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.z -= 0.2
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_waypoint_z_up(self):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = group.get_current_pose().pose
        wpose.position.z += 0.2
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def plan_cartesian_grid(self):
        print "================STEP1. Go to Initial location"
        cartesian_plan, fraction = self.plan_cartesian_path()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        rospy.sleep(2)
        print "================STEP1.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP1.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zup)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)         
        rospy.sleep(2)
        print "===============STEP2. Go to point 1 (y - negative)"
        cartesian_plan_yneg, fraction = self.plan_cartesian_waypoint_y_negative()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_yneg)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP2.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        rospy.sleep(2)
        print "================STEP2.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zup)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "===============STEP3. Go to point 2 (x - negative)"
        cartesian_plan_xneg, fraction = self.plan_cartesian_waypoint_x_negative()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_xneg)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP3.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        rospy.sleep(2)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        print "================STEP3.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zup)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "===============STEP4. Go to point 3 (y - positive)"
        cartesian_plan_ypos, fraction = self.plan_cartesian_waypoint_y_positive()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_ypos)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP4.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP4.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zup)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "===============STEP5. Go to point 4 (x - positive)"
        cartesian_plan_xpos, fraction = self.plan_cartesian_waypoint_x_positive()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_xpos)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP5.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP5.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zup)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "===============STEP6. Go to point 5 (y - positive)"
        cartesian_plan_yypos, fraction = self.plan_cartesian_waypoint_y_positive()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_yypos)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP6.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP6.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        self.execute_plan(cartesian_plan_zup)
        rospy.sleep(2)
        print "===============STEP7. Go to point 6 (x - negative)"
        cartesian_plan_xxneg, fraction = self.plan_cartesian_waypoint_x_negative()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_xxneg)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP7.1. Go down"
        cartesian_plan_zdow, fraction = self.plan_cartesian_waypoint_z_down()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zdow)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)
        print "================STEP7.2. Go up"
        cartesian_plan_zup, fraction = self.plan_cartesian_waypoint_z_up()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan_zup)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)        
        rospy.sleep(2)

    def display_trajectory(self, plan):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        ## BEGIN_SUB_TUTORIAL display_trajectory
        ##
        ## Displaying a Trajectory
        ## ^^^^^^^^^^^^^^^^^^^^^^^
        ## You can ask RViz to visualize a plan (aka trajectory) for you. But the
        ## group.plan() method does this automatically so this is not that useful
        ## here (it just displays the same trajectory again):
        ##
        ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        ## We populate the trajectory_start with our current robot state to copy over
        ## any AttachedCollisionObjects and add our plan to the trajectory.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish

        display_trajectory_publisher.publish(display_trajectory)

    def display_object_pose(self):
        self.marker_lifetime = rospy.Duration(0.0)  # 0 = Marker never expires
        self.muted = False
        self.alpha = 1.0

        # Set default parameters for Cylinder Marker
        self.cylinder_marker = Marker()
        self.cylinder_marker.header.frame_id = self.planning_frame
        self.cylinder_marker.ns = "Cylinder"  # unique ID
        self.cylinder_marker.action = Marker().ADD
        self.cylinder_marker.type = Marker().ARROW
        self.cylinder_marker.pose.position.x = self.pose.pose.position.x
        self.cylinder_marker.pose.position.y = self.pose.pose.position.y
        self.cylinder_marker.pose.position.z = self.pose.pose.position.z
        self.cylinder_marker.pose.orientation.x = self.pose.pose.orientation.x
        self.cylinder_marker.pose.orientation.y = self.pose.pose.orientation.x
        self.cylinder_marker.pose.orientation.z = self.pose.pose.orientation.x
        self.cylinder_marker.pose.orientation.w = 1.0
        self.cylinder_marker.lifetime = self.marker_lifetime
        self.cylinder_marker.scale.x = 0.1
        self.cylinder_marker.scale.y = 0.05
        self.cylinder_marker.scale.z = 0.05
        self.cylinder_marker.color.r = 1
        self.cylinder_marker.color.g = 0
        self.cylinder_marker.color.b = 0
        self.cylinder_marker.color.a = self.alpha
        self.pub_rviz_marker.publish(self.cylinder_marker)

        # Create the Rviz Marker Publisher


        rospy.loginfo("Publishing Rviz markers on topic '%s'", TOPIC_POSE_DISP)


    def execute_plan(self, plan):
        group = self.group
        group.execute(plan, wait=True)
        # robot:
        print "============ Printing robot state"
        print self.robot.get_current_state()

    def loop(self):
        rate = rospy.Rate(100)  # 10hz
        while not rospy.is_shutdown():

            self.display_object_pose()
           #self.send_gripper_command()
            group = self.group
            current_pose = group.get_current_pose().pose
            self.send_ee_pose(current_pose)
            #print '============ Press `Enter` to execute a movement using a pose goal ...'
            #raw_input()

            print '============ Press p for pose goal, j for joint goal execution, s for stow, a for setting full joint goal, o for adding an object to the scene, k for entering keyboard control, r for restricted plan or d for demo. Then press Enter ...'
            userinput = raw_input().lower()

            if (userinput[0] == 's'):
            #    goal_plan, pose_error = self.go_to_pose_goal()
                joint_tollerance = self.choose_action()
            elif (userinput[0] == 'd'):
                self.plan_cartesian_grid()
            elif( userinput[0] == 'j'):
                joint_tollerance = self.go_to_joint_angles()
                print "Joint errors ", joint_tollerance
            elif( userinput[0] == 'a'):
                joint_tollerance = self.set_des_joints_from_user()
                print "Joint errors ", joint_tollerance
            elif( userinput[0] == 'k'):
                joint_tollerance = self.keyboard_controller()
            elif (userinput[0] == 'r'):
                joint_tollerance = self.plan_cartesian_restricted()
            elif (userinput[0]== 'o'):
                joint_tollerance = self.plan_cartesian_object_in_scene()
            else:
                print('please press s, j, o, k, r, or d. You entered an unkown command')

            rate.sleep()



def main():
       # ROS_WARNING("begining testing ****************************************************************");
        try:
            #time.sleep(2)
            planning = ManipulationClass()

            planning.loop()

            #print '============ Press `Enter` to execute a movement using a pose goal ...'
            # raw_input()
            # goal_plan, pose_error = planning.go_to_pose_goal()
            # print "*************** Goal error: ", pose_error



            # print '============ Press `Enter` to set action ...'
            # raw_input()
            # joint_tollerance = planning.choose_action()




            print "============ Demo complete!"
        except rospy.ROSInterruptException:
            return
        except KeyboardInterrupt:
            return

if __name__ == '__main__':
        main()



 # def display_object_pose(self):
 #        self.marker_lifetime = rospy.Duration(0.0)  # 0 = Marker never expires
 #        self.muted = False
 #        self.alpha = 1.0
 #
 #        # Set default parameters for Cylinder Marker
 #        self.cylinder_marker = Marker()
 #        self.cylinder_marker.header.frame_id = self.planning_frame
 #        self.cylinder_marker.ns = "Cylinder"  # unique ID
 #        self.cylinder_marker.action = Marker().ADD
 #        self.cylinder_marker.type = Marker().ARROW
 #        self.cylinder_marker.pose.position.x = self.pose.pose.position.x
 #        self.cylinder_marker.pose.position.y = self.pose.pose.position.y
 #        self.cylinder_marker.pose.position.z = self.pose.pose.position.z
 #        self.cylinder_marker.pose.orientation.x = self.pose.pose.orientation.x
 #        self.cylinder_marker.pose.orientation.y = self.pose.pose.orientation.x
 #        self.cylinder_marker.pose.orientation.z = self.pose.pose.orientation.x
 #        self.cylinder_marker.pose.orientation.w = 1.0
 #        self.cylinder_marker.lifetime = self.marker_lifetime
 #        self.cylinder_marker.scale.x = 0.1
 #        self.cylinder_marker.scale.y = 0.05
 #        self.cylinder_marker.scale.z = 0.05
 #        self.cylinder_marker.color.r = 1
 #        self.cylinder_marker.color.g = 0
 #        self.cylinder_marker.color.b = 0
 #        self.cylinder_marker.color.a = self.alpha
 #        self.pub_rviz_marker.publish(self.cylinder_marker)


