#! /bin/bash

xhost +
nvidia-docker run -it \
	-v /home/kraft/workspace:/home/workspace:rw \
	--env QT_X11_NO_MITSHM=1 \
	--env="DISPLAY" \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--device /dev/bus/usb:/dev/bus/usb \
	--device /dev/input \
	kraft_arm:dockerfile

	# --net kraftnet \