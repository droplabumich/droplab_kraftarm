﻿/*
 * Created by SharpDevelop.
 * User: Daniel
 * Date: 5/9/2012
 * Time: 9:53 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace UdpLog
{
	class Program
	{
		class LogInfo
		{
			public int Port;
			public string FileExtension;
			public string FilePath;
			
			public LogInfo(int pt, string fe, string fp)
			{
				Port = pt;
				FileExtension = fe;
				FilePath = fp;
			}
		}
		
		public static void Main(string[] args)
		{
			string ExeName = System.AppDomain.CurrentDomain.FriendlyName;
			if (args.Length != 3)
				Console.WriteLine("Usage of " + ExeName + ": Please specify a single port number to log. Specify file extension. The third argument is a path for the resulting log file.");
			else
			{
				//foreach (string s in args)
				LogInfo li = new LogInfo(int.Parse(args[0]), args[1], args[2]);
				
				{
					System.Threading.Thread td = new System.Threading.Thread(Program.DoLog);
					td.IsBackground = true; // Auxiliary threads die when main loop exits.
					td.Start(li);
				}
				char ans = ' ';
				while (ans != 'q')
				{
					Console.WriteLine(ExeName + " started. Press 'q' to stop.");
					ans = Console.ReadKey().KeyChar;
				}
			}
		}
		
		public static void DoLog(object o)
		{
			LogInfo li = (LogInfo) o;
			
			DgiUtil.MultiLogger log = new DgiUtil.MultiLogger(li.FileExtension, li.FilePath);
	
			// The UdpClient object is used for RX.
			// The port specified here filters the recv pkts.
			// (also would be the "source" for TX if we were to send).
			System.Net.Sockets.UdpClient lgSock = new System.Net.Sockets.UdpClient(li.Port);
			
			// Create an IPEndPoint to receive messages. A port of '0' means "any available" and does NOT affect the filtering of incoming packets.
			System.Net.IPEndPoint lgRecvpt = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);//InUDPPort);//1501) ;
			// later, we can get new packets with this command:   data = lgSock.Receive(ref lgRecvpt);
			
			Console.WriteLine("Logging port " + li.Port.ToString()  + " to " + log.FileExtension + " files at " + log.LogPath + '.');

			
						
			// first time only display the source IP address.
			byte[] data = lgSock.Receive(ref lgRecvpt);
			System.Net.IPAddress ipa = lgRecvpt.Address;
			System.Console.WriteLine("Got msg from IP: " + ipa.ToString());
			
			LogPretty(log, data);
			while (true)
			{
				data = lgSock.Receive(ref lgRecvpt);
				LogPretty(log, data);
			}
		}
		
		private static void LogPretty(DgiUtil.MultiLogger ml, byte[] inraw )
		{
			if (inraw.Length < 3)
				ml.Log("UDP", System.Text.Encoding.ASCII.GetString(inraw));
			else
			{
				string indata = System.Text.Encoding.ASCII.GetString(inraw);
				ml.Log(indata.Substring(0,3), indata.Substring(3));
			}
		}
	}
}