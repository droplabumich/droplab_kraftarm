#ifndef KRAFT_H
#define KRAFT_H

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <arpa/inet.h>
#include <streambuf>
#include <cstring>
#include <cstdint>
#include <cmath>
#include <fcntl.h>

#include <ros/ros.h>

// ROS parameter loading
#include <rosparam_shortcuts/rosparam_shortcuts.h>

#define _USE_MATH_DEFINES

// #define KRAFT_IP "192.168.10.151"

using namespace std;


int mode;
namespace ROSKraft
{	
	class Kraft
	{
		public: Kraft(ros::NodeHandle& nh) : nh_(nh)
		{
			// Load rosparams
			ros::NodeHandle rpsnh(nh, name_);
			std::size_t error = 0;
			error += !rosparam_shortcuts::get(name_, rpsnh, "robot_ip", robot_ip_);
			rosparam_shortcuts::shutdownIfError(name_, error);
			
			Start();
			closedHydraulics();
			for (int i=0; i<10; i++){
				sendIdle();
				UdpRx();
				usleep(20000);
			}

			setHydraulicsEnabled(true);
			cout << "Initial command Values: \n";
			for (int i=0;i<7;i++)
				cout << dec << (unsigned int) Command[i] << " ";
			cout << endl;
		}


		protected:
		// Startup and shutdown of the internal node inside a roscpp program
		ros::NodeHandle nh_;

		// Name of this class
		std::string name_ = "kraft";

		// IP address of RSD
		std::string robot_ip_;

        //mode = 1;
		private: 
		int TxAck=0; // good exchanges
		int TxNack=0; // bad exchanges
		int RxAck=0; // good exchanges
		int RxNack=0; // bad exchanges
		int UdpClient=0;
		bool RxReady = false;
		const string JointNames[7] = {"Azimuth","Elevator","Elbow", "Pitch","Yaw","Rotate","Grip"};
		static const int NumAxes = 7;
		const int CommandMin[7] = {433, 1677, 1400, 1105, 1389, 48, 0};  //"Rotate"  is commanded in velocity
		const int CommandMaximum[7] = {3654, 2719, 2804, 2901, 2769, 4092, 0};  //"Rotate"  is commanded in velocity
		const float MinAngle[7] = {-135, 0, 0,  -75, -50, -135, 0};
		const float MaxAngle[7] = {135, 90, 120, 75,  50, 225, 10};
		const int CommandZero[7] = {2007, 2719, 1400, 2003, 2079, 1605, 0}; // "Rotate" = 128 is not moving
		double joint_angles[7] = {0, 0, 0, 0, 0, 0, 0};
		double angle_degrees[7] = {0, 0, 0, 0, 0, 0, 0};  //60, -40, 100, 0, 0, 0, 0
		double angle_rad_prev[7] = {0, 0, 0, 0, 0, 0, 0};  //60, -40, 100, 0, 0, 0, 0
		double angle_reached_thresh[7] = {.01, .01, .01, .01, .2, 0, 0};  //60, -40, 100, 0, 0, 0, 0
		//double angle_rad[7] = {0, 0, 0, 0, 0, 0, 0};  //60, -40, 100, 0, 0, 0, 0
		int greeper_state = 1; //0 - open; 1- closed
		int Target[NumAxes]; // the target for the trajectory, same units as command
		int TargetPots[NumAxes];

		//PID related
		const float dt = 0.0001;
		const int VelMin[2] = {0, 0};
		const int VelMax[2] = {255, 128};
		const int ZeroVel[2] = {128, 0};
 		const float Kp[2] = {0.25, 0};
		const float Kd[2] = {0.00, 0};
		const float Ki[2] = {0.001, 0};
		const float limit_int[2] = {0.001, 0.001};
		float error_pos[2], error_der[2], error_int[2], vel[2];
		float error_prev[2] = {0, 0};

		
		//public static float[] Acceleration = new float[]{ 0.04f, 0.5f,          0.1f,    0.1f,  0.08f};
		// note tested acceleration 1 is way too high. 0.1 is fast. 0.01 is slow but usable (for yaw 1/17/2012)
		// 2012/1/22: try reducing elevator from 0.5 down to 0.08. Yaw from 0.08 down to 0.03.
		// 2012/3/19: changed from an acceleration array to a single scalar value.

		// Note: Acceleration must be low enough that the commands to the arm are stepped by increments of 1.
		// It seems that the arm expects continous command values starting from the arm's current position and incrementing to the
		// target position, like would be commanded using a joystick with continous transmission.
		
		float Acceleration = 0.01;
		// Update 2012/3/19. All of this assumes a nominal 10 ms subsea update rate, with a 8 ms Sleep in the TX loop.
		// Found that there is probably no need to go slower than 0.1.
		// 2 is plenty fast enough to do "shaking" maneuvers, only appropriate for a master period of 10 ms.
		// 1 is good for 20 ms. scale to 0.1 at 100 ms.
		// Therefore, the formula should be: Acceleration = 20 / MasterPeriodMs; // 1 for 20 ms, 0.2 for 100ms
		// Acceleration is continuously updated in the log thread, consistent with the Master update rate.
		

		public: float AngleRange(int axis)
		{
			return MaxAngle[axis] - MinAngle[axis];
		}

		public: int CommandSpan(int axis)
		{
			return CommandMaximum[axis] - CommandMin[axis];
		}
		
		private: int Command[NumAxes]; // the number sent to RSD, 12 bit, equal to Fbk*2
		protected: int Feedback[NumAxes]; //the number returnd from RSD, 11 bit
		//public int[] Target = new int[;
		
		//public int yawTgt=1600; // safe place for yaw
		public: float Speed[NumAxes] = {0,0,0,0,0,0,0}; // need this for path planning
		
		
		// These comms statistics are periodically refreshed in the LogLoop, but
		// individual periods are checked against min/max in their respective loops.
		long double SubseaPreviousRx = time(0)*1000;
		int SubseaPeriodMax = 0;
		int SubseaPeriodMin = 999;
		
		long double MasterPreviousUpdate = time(0)*1000;
		int MasterPeriodMax = 0;
		int MasterPeriodMin = 999;
		
		
		private: bool hydraulicsEnabled = false;
		public: bool getHydraulicsEnabled()
		{
			return hydraulicsEnabled;
		}
		public: void setHydraulicsEnabled(bool value)
		{
			if ((value) && (!hydraulicsEnabled))
			{
				for (int i =0; i<5; i++)
				{
					Command[i] = Feedback[i]*2;
				}
				Command[5] = 128;

			}
			hydraulicsEnabled = value;
		}

		public: void shutdown()
		{
			setHydraulicsEnabled(false);
			//UdpTx(angle_rad);
			for (int i=0;i<100;i++) {
				UdpRx();
				sendIdle();
				usleep(100);		
			}

		}
		
		// WristSpeed is going to be used in an unexpected way - to spy on the master recv to get the MasterRate.
		public: int getWristSpeed()
		{
			return Command[5];
		}
		public: void setWristSpeed(int value)
		{
			Command[5] = value;
			
			
			long double MasterRecvNow = time(0)*1000;
			int Elapsed = (int) (MasterRecvNow - MasterPreviousUpdate);
			if (MasterPeriodMax < Elapsed)
				MasterPeriodMax = Elapsed;
			if (MasterPeriodMin > Elapsed)
				MasterPeriodMin = Elapsed;
			MasterPreviousUpdate = MasterRecvNow;	
		}
		
		// GripForce is a value 0-128
		// 0 = open
		// 128 = closed
		// there is a neutral value which happens to be 39 or 40 depending on the day.
		public: int getGripForce()
		{
			return Command[6];
		}
		public: void setGripForce(int value)
		{
			Command[6] = value;
		}

		public: bool GripOnLostComms = true;
		private: uint8_t SequenceCounter = 0x10; // set by subsea, echoed by topside
		
		public: int CycleCounter = 0; // counts UPD exchange cycles
		
		// DgiUtil.MultiLogger kss; // used only in udp threads
		
		// bool UnixOS = false;
		// readonly bool EnableLogging; // Readonly means it can only be changed in the constructor, but not later.
		// readonly string LogPath;

		
		private: void Start()
		{
			//cout << " initializing RSD comms." << endl;
			
			// Initialize UDP socket
			if ((UdpClient = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
				cerr << "cannot create socket" << endl;
			fcntl(UdpClient, F_SETFL, O_NONBLOCK); // set non-blocking
			/* bind to an arbitrary return address */
			/* because this is the client side, we don't care about the address */
			/* since no application will initiate communication here - it will */
			/* just send responses */
			/* INADDR_ANY is the IP address and 0 is the socket */
			/* htonl converts a long integer (e.g. address) to a network representation */
			/* htons converts a short integer (e.g. port) to a network representation */
			struct sockaddr_in myaddr;
			memset((char *)&myaddr, 0, sizeof(myaddr));
			myaddr.sin_family = AF_INET;
			myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
			myaddr.sin_port = htons(0);
			
			if (bind(UdpClient, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0)
				cerr << "bind failed" << endl;

			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			//cout << "Start streaming to subsea RSD . . . " << endl;
			// send a message to the server
			uint8_t WakeupBuffer[] = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
			int ret;
			ret = sendto(UdpClient, WakeupBuffer, 8, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
			if (ret < 0)	
				cerr << "sendto failed\n";
			else
				cout << "Sent bytes: " << ret << endl;

			RxReady = true; // objects are initialized and ready for Rx Thread to use them.
			

			// Now do experiments:
			//for (int i=0; i<5; i++)
			//	Console.WriteLine(JointNames[i] + " span = " + CommandSpan(i));
			
			// can put these declarations outside Main() as properties if needed for disposing.
			// System.Threading.Thread UdpTxThread; // MUST RUN FAST!! gets started by Start() and killed by Dispose()
			// System.Threading.Thread UdpRxThread; // MUST RUN FAST!! gets started by Start() and killed by Dispose()
			// System.Threading.Thread LogThread; // This thread does not need to be fast.
			
			// UdpTxThread = new System.Threading.Thread(new System.Threading.ThreadStart(UdpTxLoop));	
			// UdpTxThread.IsBackground = true; // makes it die when parent is disposed.
			// UdpTxThread.Start();

			// UdpRxThread = new System.Threading.Thread(new System.Threading.ThreadStart(UdpRxLoop));	
			// UdpRxThread.IsBackground = true; // makes it die when parent is disposed.
			// UdpRxThread.Start();
			

			// LogThread = new System.Threading.Thread(new System.Threading.ThreadStart(LogLoop));	
			// LogThread.IsBackground = true; // makes it die when parent is disposed.
			// LogThread.Start();

		}

		public: void SendWakeup()
		{
			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			//cout << "Start streaming to subsea RSD . . . " << endl;
			// send a message to the server
			uint8_t WakeupBuffer[] = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
			int ret;
			ret = sendto(UdpClient, WakeupBuffer, 8, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
			if (ret < 0)	
				cerr << "sendto failed\n";
			// else
				// cout << "Sent bytes: " << ret << endl;

			RxReady = true; // objects are initialized and ready for Rx Thread to use them.
		}

		public: void sendIdle()
		{
			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			//cout << "Start streaming to subsea RSD . . . " << endl;
			// send a message to the server
			uint8_t OutBuffer[] = { 0x55,1,0,1,1,0,1,1,0,0x80, 0x6b, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle
			FinalizePacket(OutBuffer);
			int ret;
			ret = sendto(UdpClient, OutBuffer, 18, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
			if (ret < 0)	
				cerr << "sendto failed\n";
			// else
				// cout << "Sent bytes: " << ret << endl;
		}
		

		public: void UdpTx(double angle_rad[7])
		{
			// bool new_pose = false;
			// for (int i; i<7; i++)
			// {
			// 	if (angle_rad_prev[i] == angle_rad[i]) {
			// 		new_pose = true;
			// 	}
			// 	angle_rad_prev[i] = angle_rad[i];
			// }

			if (true)
			{
				struct sockaddr_in servaddr; /* server address */
				/* fill in the server's address and data */
				memset((char*)&servaddr, 0, sizeof(servaddr));
				servaddr.sin_family = AF_INET;
				//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
	            inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
				servaddr.sin_port = htons(1500);

				/// UPDATE 1/25/2012: Microsoft implementation of UDPClient is really difficult to understand. It is much clearer to use raw sockets, as documented in Mono docs. Converted microsoft calls to the mono/ubuntu format. Will they still work in windows? (May-9-2012, Answer: No.)
				// Set up a UDP socket to use for communication with the arm.
				// The UdpClient object is used for both TX and RX.
				// The port specified here [becomes the "source" for TX, and also] filters the RX. This isn't the case for linux though, which assigns a random source.
				// Update 3/19/2012: On linux this constructor does not set the source port, it only filters the incoming packets.
				// We're using a different method for RX, where these rx data structures are completely separate from the sender.
				
				//kftRxSock = new UdpClient(1500);
				
				// this is an alternate way to do it, using only raw sockets, so as to be able to control the source port.
				
				//kftTxSnd = new Sender(new System.Net.IPEndPoint(IPAddress.Parse("192.168.10.151"),1500));
				
				//kftTxSnd = new Sender(new System.Net.IPEndPoint(IPAddress.Parse("127.0.0.1"),1500));
				
				// This is where we specify the destination for TX only. [[DEPRECATED]]
				//kftTxSock.Connect("192.168.10.151", 15000); // normal address for "RSD"
				//kftTxSock.Connect("127.0.0.1", 1500); // normal address for "RSD"
				
				
				// set up receive timeout - default is infinity - we will use a short timeout instead
				// NO TIMEOUT kftSock.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 8);
				//kftSock.Client.ReceiveTimeout = 25; this does not work
				// Create an IPEndPoint to receive messages. A port of '0' means "any available".
				
				//kftRecvpt = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);//1501) ;
				
				// later, we can get new packets with this command:   data = kftRxSock.Receive(ref kftRecvpt);

				//int RecentFbIdx = 0x00; now called sequencecounter
				
				
				// Declare some uint8_t arrays to use for UDP messages.
				//var WakeupBuffer = new uint8_t[] {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
				// Note: Leave a zero uint8_t at the end for the checksum which will replace it.
				//var OutBuffer[18] = new uint8_t[18];
				//uint8_t OutBuffer[] = { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 };
				//uint8_t OutBuffer[] = { 0x55,1,0,1,1,0,1,1,0,0x80, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle 
				//uint8_t OutBuffer[] = { 0x55,0xd4,0xa2,0xdc,0x6a,0x85,0x15,0x00,0x08,0x97, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x1a, 0, 0}; // enabled
				//OutBuffer = new uint8_t[] { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 }; // gripping?
				//uint8_t OutBuffer[] = { 0x55, 0x01, 0x00, 0x01, 0x00, 0x01, 0x01, 0x00, 0x80, 0x4d, 0x85, 0x8c, 0x0c, 0xf4, 0x0a, 0x33, 0x75 };
				uint8_t OutBuffer[] = { 0x55, 0x31, 0x99, 0xa6, 0x17, 0x7a, 0x18, 0x64, 0x09, 0x88, 0x00, 0x85, 0x8c, 0x0c, 0xf4, 0x1a, 0x87, 0x15 };
				//uint8_t OutBuffer[] = { 0x15, 0x87, 0x1a, 0xf4, 0x0c, 0x8c, 0x85, 0x00, 0x88, 0x09, 0x64, 0x18, 0x7a, 0x17, 0xa6, 0x98, 0x0b, 0x55 };
				//int angle_values[7] = {70, 70, 40, 70, 60, 100, 0};

				//Command[5] = 128; // no rotate
				//Command[6] = 255; // close gripper

				
				// cout << "initial hex OutBuffer: \n";
				// for (int i=0;i<18;i++)
				// 	cout << hex << (unsigned int) OutBuffer[i] << " ";
				// cout << endl;

				//AngleCommand(angle_values);
				for (int i =0; i<7; i++)
				{
					angle_degrees[i] = angle_rad[i]*180/M_PI;
					angle_rad_prev[i] = angle_rad[i];
				}

	            angle_degrees[6] = mode;
				AngleToPots();

				if (true) //(!ReachedGoalThresh(angle_rad))
				{

					PopulateOutgoingCommandPacket(OutBuffer);
					FinalizePacket(OutBuffer);

					//ReadCommandFromPkt(OutBuffer);

					// cout << "Command Values: \n";
					// for (int i=0;i<7;i++)
					// 	cout << dec << (unsigned int) Command[i] << " ";
					// cout << endl;

					// cout << "final hex OutBuffer: \n";
					// for (int i=0;i<18;i++)
					// 	cout << hex << (unsigned int) OutBuffer[i] << " ";
					// cout << endl;

					// now send
					try
					{
						CycleCounter++;
						//Console.WriteLine("TX: " + BitConverter.ToString(OutBuffer));
						////////////txk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(OutBuffer));
						// if (sendto(UdpClient, OutBuffer, strlen((char*)OutBuffer), 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)	
						if (sendto(UdpClient, OutBuffer, 18, 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)	
							cerr << "sendto failed\n";
						// this will throw an exception if Ethernet is interrupted,
						// because the destination becomes unreachable.
						TxAck++;
					}
					catch(exception e)
					{
						cout << "Send UDP Failed: " << e.what() << endl;
						// if (this.EnableLogging)
						// 	kss.Log("Send UDP Failed: "+ e.what() );
						this->TxNack++;
					}
				}


				// changed 5-> 8ms to see if it would reduce collisions. I think it does 1/25/2012.
				//System.Threading.Thread.Sleep(8);
				// 10 ms works fine, mono, 2012/2/2
				// 20 ms works fine, mono, 2012/2/2
				// 40 ms doesn't work at all. comms but hydraulics never enable. 2012/2/2
				// 30 ms doesn't work at all.
				// 25 ms doesn't work at all.
				// 21 ms works sometimes, freezes occasionally. 2012/2/2
				// 19 ms works reliably for a few minutes but does rarely freeze 2012/2/2
			}

		}// UdpTx

		public: bool ReachedGoalThresh(double angle_rad[7])
		{
			int flag = true;
			for (int i=0; i<5; i++) {
				if (std::abs(angle_rad[i] - joint_angles[i]) > angle_reached_thresh[i])
					flag = false;
			}
			return flag;
		}
			
		public: void UdpRx()
		{
			uint8_t indata[2048] = {};
			long double RecvNow;
			int Elapsed;

			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, robot_ip_.c_str(), &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);
			socklen_t addrlen = sizeof(servaddr); /* length of addresses */
			int recvlen; /* # uint8_ts received */
			
			if (RxReady)
			{
				try
				{
					recvlen = recvfrom(UdpClient, indata, 2048, 0, (struct sockaddr *)&servaddr, &addrlen);
				//	cout << "recvlen: " << recvlen << endl;
					////////////rxk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(indata) );
					if (recvlen == -1){
						ROS_ERROR_THROTTLE(1.0, "Receive failed. Error: %i\n", errno);
					}
					else if (recvlen > 16){
						SequenceCounter = indata[16];
						ReadFeedbackFromPkt(indata);
						PathPlanner(); // takes care of axes 0-5.
						EnforcePotsLimits();
						RxAck++;
						RecvNow = time(0)*1000;
						Elapsed = (int) (RecvNow - SubseaPreviousRx);
						if (SubseaPeriodMax < Elapsed)
							SubseaPeriodMax = Elapsed;
						if (SubseaPeriodMin > Elapsed)
							SubseaPeriodMin = Elapsed;
						SubseaPreviousRx = RecvNow;
						// for (int i=0;i<recvlen+1;i++)
						// 	cout << hex << (unsigned int) indata[i] << " ";
						// cout << endl;
					}
					else
					{
						// if (this.EnableLogging)
						// 	kss.Log("ERR", "Rx packet too short. Ignored.");
						RxNack++;
					}
				}
				catch (exception e)
				{
					//cout << "UDP receive Failed: " << e.what() << " " << CycleCounter << endl;// (timeout/unreachable).");
					// if (this.EnableLogging)
					// 	kss.Log("ERR", "rx timeout: " + e.Message);
					RxNack++;
				}
	       }
		}	
		
		private: void PopulateOutgoingCommandPacket(uint8_t *pkt)
		{
			// first set the status uint8_t, #15. Four possible states. Maybe more.
			if (hydraulicsEnabled)
			{
				if (GripOnLostComms)
					pkt[15] = 0x1a;
				else
					pkt[15] = 0x12;	
			}
			else
			{
				if (GripOnLostComms)
					pkt[15] = 0x02;
				else
					pkt[15] = 0x0a;
			}
			
			/// Now the reverse of the decoding:
			/// 		public void ReadCommandFromPkt(uint8_t[] pkt)
		
			//Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			//Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			//Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			//Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			//Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits

		
			pkt[1] = (uint8_t) (Command[0] & 0xFF);
			pkt[2] = (uint8_t) (((Command[0] >> 8) & 0x0F) | ((Command[1] >> 4) & 0xF0));
			pkt[3] = (uint8_t) (Command[1] & 0xFF);
			pkt[4] = (uint8_t) (Command[2] & 0xFF);
			pkt[5] = (uint8_t) (((Command[2] >> 8) & 0x0F) | ((Command[3] >> 4) & 0xF0));
			pkt[6] = (uint8_t) (Command[3] & 0xFF);
			pkt[7] = (uint8_t) (Command[4] & 0xFF);
			pkt[8] = (uint8_t) ((Command[4] >> 8) & 0x0F);
			pkt[9] = (uint8_t) (Command[5] & 0xFF); // rotate 8 bits
			pkt[10] = (uint8_t) (Command[6] & 0xFF); // gripper 8 bits
			
			//Console.WriteLine("Grip Command = " + Command[6]);


		}
		
		private: void FinalizePacket(uint8_t *pkt)
		{
			uint8_t expecting;
			string alert;
			expecting = (uint8_t)((int)pkt[16]+1);
			
			if (SequenceCounter != expecting)
			{
				TxNack++;
				//cout << (unsigned int) SequenceCounter << endl;
				//cout << (unsigned int) expecting << endl;
				alert = "Out of sequence packet, got " + to_string(unsigned(SequenceCounter)) + ", expecting " + to_string(unsigned(expecting)) + ".";
			//	cout << alert << endl;
				// if (this.EnableLogging)
				// 	kss.Log("ERR", alert);
			}
			
			pkt[16] = (uint8_t) SequenceCounter;
			
			FillInKraftChecksum(pkt);

		}
		
		private: static void FillInKraftChecksum(uint8_t *pkt)
		{
			int cksum = 0;
			pkt[17]=0;
			for (int i=0;i<17;i++)
				cksum += pkt[i];
			pkt[17] = (uint8_t) cksum;
		}

		public: void ReadCommandFromPkt(uint8_t *pkt)
		{
			Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits
			Command[5] = pkt[9]; // wrist rotate 8 bits.
			Command[6] = pkt[10]; // grip 8 bits
		}
		
		public: void ReadFeedbackFromPkt(uint8_t *pkt)
		{
			Feedback[0] = pkt[1] + (pkt[2] & 0x0E) * 128; // wrist 11 bits
			Feedback[1] = pkt[3] + (pkt[2] & 0xE0) * 8;   // elevator 11 bits
			Feedback[2] = pkt[4] + (pkt[5] & 0x0E) * 128; // elbow position 11 bits
			Feedback[3] = pkt[6] + (pkt[5] & 0xE0) * 8;   // wrist pitch 11 bits
			Feedback[4] = pkt[7] + (pkt[8] & 0x0E) * 128; // wrist yaw 11 bits
			Feedback[5] = pkt[9] + (pkt[8] & 0xE0) * 8;   // wrist rotate 11 bits.
			// NOTE NO GRIP FEEDBACK !!!

			// Populate joint angles
			PotsToAngles();
		}
		
		private: void PathPlanner()
		{
			if (hydraulicsEnabled)
			{
				for (int axis=0; axis<7; axis++)	
				{

					/* For testbed RSD, sending target values directly works smoothly.
					   TODO: For NUI RSD, may need logic commented below to mimic commands recieved from a joystick.*/
					Command[axis] = Target[axis];

					// if (axis < 5)
					// 	Command[axis] = Feedback[axis]*2;

					// int togo = Target[axis]-Command[axis];
					// Speed[axis] = (togo > 0) ? 1 : ((togo < 0) ? -1 : 0);
					// Command[axis] += Speed[axis];

					// // If we can stop in one step, and can get there in one step, 
					// if ((fabs(Speed[axis]) <= Acceleration) && (((float) togo <= (Speed[axis] + Acceleration)) && ((float)togo >= Speed[axis] - Acceleration)))
					// {
					// 	Command[axis] = Target[axis];
					// 	Speed[axis] = 0.0;
					// 	// then get there in one step and stop!
					// }
					// else // Target not accessible in one step, adjust speed and keep going
					// {
					// 	if ((togo >= 0) && (Speed[axis] <= 0)) // going the wrong way, reverse
					// 		Speed[axis] += Acceleration;
					// 	else if ((togo <= 0) && (Speed[axis] >=0)) // going the wrong way, reverse
					// 		Speed[axis] -= Acceleration;
					// 	else if (togo > ((Speed[axis] * Speed[axis]) / (2 * Acceleration)))
					// 		Speed[axis] += Acceleration; // positive speedup
					// 	else if (togo < ((Speed[axis] * Speed[axis]) / (-2 * Acceleration)))
					// 		Speed[axis] -= Acceleration; // negative speedup
					// 	else if (togo > 0) // forward deceleration
					// 		Speed[axis] -= Acceleration;
					// 	else if (togo < 0) // reverse deceleration
					// 		Speed[axis] += Acceleration;
					// 	Command[axis] += (int) Speed[axis];
					// }
				}
			}
		}
		

		
		public: static string PrettyPrintPacket ( uint8_t *ind )
		{
			string str = "";
			for ( unsigned int i=0; i<strlen((char*)ind); i++)
			{
				str += printf("%02X", ind[i]);
				str += " ";
			}
			// return str.trim(null);
			// remove that very last space for neatness
			return str;
		}

		private: void PotsToAngles()
		{	
			double joint_degrees[7] = {0, 0, 0, 0, 0, 0, 0};
			for (int i = 0; i<6; i++)
			{
				joint_degrees[i] = (2*Feedback[i] - CommandZero[i])*AngleRange(i)/CommandSpan(i);
				joint_angles[i] = (joint_degrees[i] * M_PI)/180;
			}
			joint_angles[6] = 0;
		}

		public: void getJointAngles(double angles[7])
		{	
			for (int i = 0; i<7; i++)
				angles[i] = joint_angles[i];
		}

		public: void AngleToPots()
		{	
			for (int i=0; i<7; i++)
			{	 
				 TargetPots[i] = CommandZero[i] + (CommandSpan(i)*angle_degrees[i])/AngleRange(i);	
			}

			PositionToVelocityPID();
			for (int i=0;i<7; i++)
			{
				if (i<5)
					Target[i] = TargetPots[i];
				else
					Target[i] = vel[i-5];		
				
			}

			if (angle_degrees[6] == 0)
				Command[6] = 255;
			else
				Command[6] = 0; 

			EnforcePotsLimits();	
		}

		public: void EnforcePotsLimits()
		{
			for (int i=0; i<5; i++)
			{	 

				if (Command[i]>CommandMaximum[i])
				 	Command[i] = CommandMaximum[i];	
				else if (Command[i]<CommandMin[i])
					Command[i] = CommandMin[i];
			}
		}

		public: void PositionToVelocityPID()
		{
			
			for (int i = 5; i<6; i++)
			{
				int j = i-5;
				error_pos[j] = TargetPots[i] - 2*Feedback[i]; 
				error_der[j] = (error_pos[j] - error_prev[j])/dt;
				error_int[j] = error_int[j] + error_pos[j];

				if (error_int[j]>limit_int[j])
					error_int[j] =  limit_int[j];
				else 
					if (error_int[j]<-limit_int[j])
							error_int[j] = - limit_int[j];

				vel[j] = ZeroVel[j] + (int)(Kp[j]*error_pos[j] + Kd[j]*error_int[j] + Ki[j]*error_int[j]);

				
				if (vel[j] > VelMax[j])
					vel[j] = VelMax[j];
				else
					if (vel[j] < VelMin[j])
						vel[j] = VelMin[j];

				error_prev[j] = error_pos[j];
			}
		}

		public: void getJointPotsFeedback(double potsfb[7])
		{	
			for (int i = 0; i<7; i++)
				potsfb[i] = Feedback[i];
		}

		public: void getJointPotsCommands(double potscmd[7])
		{	
			for (int i = 0; i<7; i++)
				potscmd[i] = Command[i];
		}

		public: void closedHydraulics()
		{
			setHydraulicsEnabled(false);
			//UdpTx(angle_rad);
			for (int i=0;i<400;i++) {
				UdpRx();
				sendIdle();
				usleep(10000);	
			}
		}
	};
}

#endif