﻿using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace SplitSitter
{	
	class Kraft
	{
		int CommAck=0; // good exchanges
		int CommNack=0; // bad exchanges
		static int NumAxes = 7;
		public static string[] JointNames = new string[] {"Azimuth","Elevator","Elbow", "Pitch","Yaw","Rotate","Grip"};
		public static int[] CommandMin = new int[]    {700,    2000,           1400,    1300,   1200,  0,     0};
		public static int[] CommandCenter = new int[] {1848,   2700,           1515,    2200,   2039,  128,   1};
		public static int[] CommandMaximum = new int[]{2600,   2850,           2700,    2700,   2600,  255,   200};
		public static float[] Acceleration = new float[]{ 0.04f, 0.5f,          0.1f,    0.1f,  0.08f,   2,     2};
		// note tested acceleration 1 is way too high. 0.1 is fast. 0.01 is slow but usable (for yaw 1/17/2012)
		
		public int CommandSpan(int axis)
			{
				return CommandMaximum[axis] - CommandMin[axis];
			}
		
		private int[] Command = new int[NumAxes]; // the number sent to RSD, 12 bit, equal to Fbk*2
		protected int[] Feedback = new int[NumAxes]; //the number returnd from RSD, 11 bit
		//public int[] Target = new int[;
		public int[] Target = new int[NumAxes]; // the target for the trajectory, same units as command
		//public int yawTgt=1600; // safe place for yaw
		public float[] Speed = new float[NumAxes]; // need this for path planning
		
		public string LogString
		{
			get
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder();
				if (HydraulicsEnabled)
					sb.Append("1 ");
				else
					sb.Append("0 ");
				for (int i=0; i<NumAxes; i++)
				{
					sb.Append(Target[i].ToString("000000 "));
					sb.Append(Speed[i].ToString("000.00 ;-00.00 "));
					sb.Append(Command[i].ToString("000000 "));
					sb.Append(Feedback[i].ToString("000000  "));
				}
				sb.Append(CommAck.ToString("000000 "));
				sb.Append(CommNack.ToString("000000"));
				return sb.ToString();
					
			}
		}
		
		private bool hydraulicsEnabled = false;
		public bool HydraulicsEnabled
		{
			get
			{
				return hydraulicsEnabled;
			}
			set
			{
				if ((value) && (!hydraulicsEnabled))
				{
					for (int i =0; i<5; i++)
					{
						Command[i] = Feedback[i]*2;
						Target[i] = Command[i];
					}
				}
				hydraulicsEnabled = value;
			}
		}
		
		public int WristSpeed
		{
			get
			{
				return Command[5];
			}
			set
			{
				Command[5] = value;
			}
		}
		public bool GripClosed
		{
			get
			{
				return (Command[6] > 128);
			}
			set
			{
				if (value)
					Command[6] = 50; // this is pretty darn slow closing. What is neutral?
				else
					Command[6] = CommandMin[6];
			}
		}

		public bool GripOnLostComms = true;
		private byte SequenceCounter = 0x10; // set by subsea, echoed by topside
		
		public int CycleCounter = 0; // counts UPD exchange cycles
		
		System.Threading.Thread UdpTxThread; // gets started by Start() and killed by Dispose()
		System.Threading.Thread UdpRxThread; // gets started by Start() and killed by Dispose()
		DgiUtil.MultiLogger kss; // used only in udp threads
		
		[DllImport("winmm.dll")]
		internal static extern uint timeBeginPeriod(uint period);
		
		[DllImport("winmm.dll")]
		internal static extern uint timeEndPeriod(uint period);
		
		
		public void Start()
		{
			UdpTxThread = new System.Threading.Thread(new System.Threading.ThreadStart(UdpTxLoop));	
			UdpTxThread.IsBackground = true; // this makes it die when parent is disposed.
			UdpTxThread.Start();

			UdpRxThread = new System.Threading.Thread(new System.Threading.ThreadStart(UdpRxLoop));	
			UdpRxThread.IsBackground = true; // this makes it die when parent is disposed.
			UdpRxThread.Start();
		}
		
		System.Net.Sockets.UdpClient kftSock;
		System.Net.IPEndPoint kftRecvpt;
		bool RxReady = false;
			
		private void UdpTxLoop()
		{
			// Set up a UDP socket to use for communication with the arm.
			// The UdpClient object is used for both TX and RX.
			// The port specified here becomes the "source" for TX, and also filters the RX.
			kftSock = new UdpClient(1500);
			// This is where we specify the destination for TX only.
			kftSock.Connect("192.168.10.151", 1500); // normal address for "RSD"
			
			kss = new DgiUtil.MultiLogger("KSS");
			
			// set up receive timeout - default is infinity - we will use a short timeout instead
			// NO TIMEOUT kftSock.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 8);
			//kftSock.Client.ReceiveTimeout = 25; this does not work
			// Create an IPEndPoint to receive messages. A port of '0' means "any available".
			kftRecvpt = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);//1501) ;
			// later, we can get new packets with this command:   data = kftSock.Receive(ref kftRecvpt);

			//int RecentFbIdx = 0x00; now called sequencecounter
			
			
			// Declare some byte arrays to use for UDP messages.
			var WakeupBuffer = new byte[] {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
			// Note: Leave a zero byte at the end for the checksum which will replace it.
			var OutBuffer = new byte[18];
			//OutBuffer = new byte[] { 0x55,1,0,1,1,0,1,1,0,0x80, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle 
			//OutBuffer = new byte[] { 0x55,0xd4,0xa2,0xdc,0x6a,0x85,0x15,0x00,0x08,0x97, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x1a, 0, 0 }; // enabled
			OutBuffer = new byte[] { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 }; // gripping?

			Command[5] = 128; // no rotate
			Command[6] = 0; // open gripper
			
			Console.WriteLine("Start streaming to subsea RSD . . . ");
			//Console.ReadKey(true);
			
			kftSock.Send(WakeupBuffer, WakeupBuffer.Length);
			
			RxReady = true; // objects are initialized and ready for Rx Thread to use them.
			
			timeBeginPeriod(1);
			while (true)
			{
				PopulateOutgoingCommandPacket(OutBuffer);
				FinalizePacket(OutBuffer);			
				// now send
				try
				{
					CycleCounter++;
					//Console.WriteLine("TX: " + BitConverter.ToString(OutBuffer));
					////////////txk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(OutBuffer));
					kftSock.Send(OutBuffer, OutBuffer.Length);
					// this will throw an exception if Ethernet is interrupted,
					// because the destination becomes unreachable.
				}
				catch
				{
					Console.WriteLine("Send UDP Failed.");
					kss.Log("Send UDP Failed.");
					this.CommNack++;
				}
				
				System.Threading.Thread.Sleep(5); //milliseconds should be plenty!
				
				
				/*
				// now resync by requesting a nonexistent packet and timing out
				try
				{
					indata = kftSock.Receive(ref kftRecvpt);
					rxk.Log(CycleCounter.ToString("000000 ") + "UNEXPECTED " + PrettyPrintPacket(indata) );
					if (indata.Length > 16){
						SequenceCounter = indata[16];
						ReadFeedbackFromPkt(indata);
					}
				}
				catch (System.Net.Sockets.SocketException)
				{
					// A timeout here would be completely normal. No action necessary.
				}	
				*/
				
				if ((CycleCounter % 4) == 0)
					kss.Log(this.LogString);
				if ((CycleCounter % 100) == 0)
					Console.WriteLine("("+CycleCounter+")");
			}// whiletrue

		}// UdpTxLoop
		
		
		
		private void UdpRxLoop()
		{
			byte[] indata;
			
			while (!RxReady)
			{
				System.Threading.Thread.Sleep(10);
			}

			while (RxReady)
			{
				try
				{
					indata = kftSock.Receive(ref kftRecvpt);
					////////////rxk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(indata) );
					if (indata.Length > 16){
						SequenceCounter = indata[16];
						ReadFeedbackFromPkt(indata);
						PathPlanner();
						CommAck++;
					}
					else
						CommNack++;
				}
				catch (System.Net.Sockets.SocketException e)
				{
					Console.WriteLine("UDP receive Failed" + CycleCounter);// (timeout/unreachable).");
					kss.Log("ERR", "rx timeout" + e.Message);
					CommNack++;
				}
	       }
		}
		
		private void PopulateOutgoingCommandPacket(byte[] pkt)
		{
			// first set the status byte, #15. Four possible states. Maybe more.
			if (HydraulicsEnabled)
			{
				if (GripOnLostComms)
					pkt[15] = 0x1a;
				else
					pkt[15] = 0x12;	
			}
			else
			{
				if (GripOnLostComms)
					pkt[15] = 0x02;
				else
					pkt[15] = 0x0a;
			}
			
			/// Now the reverse of the decoding:
			/// 		public void ReadCommandFromPkt(byte[] pkt)
		
			//Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			//Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			//Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			//Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			//Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits

		
			pkt[1] = (byte) (Command[0] & 0xFF);
			pkt[2] = (byte) (((Command[0] >> 8) & 0x0F) | ((Command[1] >> 4) & 0xF0));
			pkt[3] = (byte) (Command[1] & 0xFF);
			pkt[4] = (byte) (Command[2] & 0xFF);
			pkt[5] = (byte) (((Command[2] >> 8) & 0x0F) | ((Command[3] >> 4) & 0xF0));
			pkt[6] = (byte) (Command[3] & 0xFF);
			pkt[7] = (byte) (Command[4] & 0xFF);
			pkt[8] = (byte) ((Command[4] >> 8) & 0x0F);
			pkt[9] = (byte) (Command[5] & 0xFF); // rotate 8 bits
			pkt[10] = (byte) (Command[6] & 0xFF); // gripper 8 bits


		}
		
		private void FinalizePacket(byte[] pkt)
		{
			pkt[16] = (byte) SequenceCounter;
			pkt[17]=0;
			int cksum = 0;
			unchecked
			{
				foreach (int i in pkt)
					cksum += i;
				pkt[17] = (byte) cksum;
			}

		}
		


		public void Dispose()
		{
			timeEndPeriod(1);
			
			//UdpThread is background, so this shouldn't be needed. But it is effective in killing the thread.
			//if (UdpThread != null)
			//{
			//	UdpThread.Abort();
			//	UdpThread.Join();
			//}
			
		}

		
		public void ReadCommandFromPkt(byte[] pkt)
		{
			Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits
			Command[5] = pkt[9]; // wrist rotate 8 bits.
			Command[6] = pkt[10]; // grip 8 bits
		}
		
		public void ReadFeedbackFromPkt(byte[] pkt)
		{
			Feedback[0] = pkt[1] + (pkt[2] & 0x0E) * 128; // wrist 11 bits
			Feedback[1] = pkt[3] + (pkt[2] & 0xE0) * 8;   // elevator 11 bits
			Feedback[2] = pkt[4] + (pkt[5] & 0x0E) * 128; // elbow position 11 bits
			Feedback[3] = pkt[6] + (pkt[5] & 0xE0) * 8;   // wrist pitch 11 bits
			Feedback[4] = pkt[7] + (pkt[8] & 0x0E) * 128; // wrist yaw 11 bits
			Feedback[5] = pkt[9] + (pkt[8] & 0xE0) * 8;   // wrist rotate 11 bits.
			// NOTE NO GRIP FEEDBACK !!!
		}
		
		private void PathPlanner()
		{
			if (HydraulicsEnabled)
			{
				for (int a=0; a<5; a++)	
				{
					int togo = Target[a]-Command[a];
					
					// If we can stop in one step, and can get there in one step, 
					if ((Math.Abs(Speed[a]) <= Acceleration[a]) && (((float) togo <= (Speed[a] + Acceleration[a])) && ((float)togo >= Speed[a] - Acceleration[a])))
					{
						Command[a] = Target[a];
						Speed[a] = 0f;
						// then get there in one step and stop!
					}
					else // Target not accessible in one step, adjust speed and keep going
					{
						if ((togo >= 0) && (Speed[a] <= 0)) // going the wrong way, reverse
							Speed[a] += Acceleration[a]; 
						else if ((togo <= 0) && (Speed[a] >=0)) // going the wrong way, reverse
							Speed[a] -= Acceleration[a];
						else if (togo > ((Speed[a] * Speed[a]) / (2 * Acceleration[a])))
							Speed[a] += Acceleration[a]; // positive speedup
						else if (togo < ((Speed[a] * Speed[a]) / (-2 * Acceleration[a])))
							Speed[a] -= Acceleration[a]; // negative speedup
						else if (togo > 0) // forward deceleration
							Speed[a] -= Acceleration[a];
						else if (togo < 0) // reverse deceleration
							Speed[a] += Acceleration[a];
						Command[a] += (int) Speed[a];
					}
				}
			}
		}
		
		private static void FillInKraftChecksum(byte[] pkt)
		{
			unchecked
			{
				int cksum = 0;
				pkt[17]=0;
				foreach (int i in pkt)
					cksum += i;
				pkt[17] = (byte) cksum;
			}
		}
		
		public static string PrettyPrintPacket ( byte[] ind )
		{
			string str = "";
			for ( int i=0; i<ind.Length; i++)
			{
				str += ind[i].ToString("X2");
				str += " ";
			}
			return str.Trim(null);
			// remove that very last space for neatness
		}


		public Kraft()
		{

		}
	}
}