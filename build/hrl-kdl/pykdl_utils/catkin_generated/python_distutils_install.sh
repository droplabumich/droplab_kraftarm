#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
    DESTDIR_ARG="--root=$DESTDIR"
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/kraft/workspace/droplab_kraftarm/src/hrl-kdl/pykdl_utils"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/kraft/workspace/droplab_kraftarm/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/kraft/workspace/droplab_kraftarm/install/lib/python2.7/dist-packages:/home/kraft/workspace/droplab_kraftarm/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/kraft/workspace/droplab_kraftarm/build" \
    "/usr/bin/python" \
    "/home/kraft/workspace/droplab_kraftarm/src/hrl-kdl/pykdl_utils/setup.py" \
    build --build-base "/home/kraft/workspace/droplab_kraftarm/build/hrl-kdl/pykdl_utils" \
    install \
    $DESTDIR_ARG \
    --install-layout=deb --prefix="/home/kraft/workspace/droplab_kraftarm/install" --install-scripts="/home/kraft/workspace/droplab_kraftarm/install/bin"
