// #include <pcl/pcl_config.h>

#ifndef __PCL_RECONSTRUCT_FILTER__
#define __PCL_RECONSTRUCT_FILTER__

// #include <pcl/common/time.h>
// #include <pcl/common/io.h>
// #include <boost/thread.hpp>
// #include <pcl/PolygonMesh.h>
// #include <pcl/io/grabber.h>

// ROS
#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <urdf/model.h>

// PCL
#include <pcl/point_types.h>
#include <pcl/io/vtk_lib_io.h>

// C++
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <string>

namespace pcl
{
  class ReconstructFilter
  {
    public:
      typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
      
      ReconstructFilter(ros::NodeHandle &nh, urdf::Model *urdf_model = NULL);

      ~ReconstructFilter() {}

    private:
      // Short name of this class
      std::string name_;
      // Startup and shutdown of the internal node inside a roscpp program
      ros::NodeHandle nh_;
      ros::Subscriber sub_;
      // Robot description
      urdf::Model *urdf_model_;
      std::vector<std::string> link_names_;
      std::size_t num_links_;

      void Callback(const PointCloud::ConstPtr& msg);

      void ReconstructFilter::loadURDF(std::string param_name);

      void ReconstructFilter::loadLinkHulls();

      int ReconstructFilter::loadPolygonFile(const std::string &file_name, pcl::PolygonMesh& mesh);

  };
}  // namespace pcl

#endif // __PCL_RECONSTRUCT_FILTER__