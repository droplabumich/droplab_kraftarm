﻿/*
 * Created by SharpDevelop.
 * User: Daniel
 * Date: 2/2/2012
 * Time: 2:13 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace MonoMinder
{
	class Program
	{
		static string BarGraphString(char BarChar, int DisplayVal, int MinVal, int MaxVal, int width)
		{
			System.Text.StringBuilder str = new System.Text.StringBuilder("", width);
			if (DisplayVal < MinVal)
				DisplayVal = MinVal;
			else if (DisplayVal > MaxVal)
				DisplayVal = MaxVal;
		
			for (int i = 0; i< width; i++)
			{
				if (width * DisplayVal > (width * MinVal + i*(MaxVal-MinVal)))
					str.Append(BarChar);
				else
					str.Append(' ');
			}
			return str.ToString();	
		}
		
		public static void Main(string[] args)
		{
			Console.WriteLine("MasterMinder Starting...");
			SplitSitter.MiniMaster mm = new SplitSitter.MiniMaster();
			
			char BarDisplayChar;
			int LeftPaneWidth = 15;
			while (true)
			{
				System.Threading.Thread.Sleep(500);
				Console.Clear();
				Console.WriteLine();
				Console.Write("".PadRight(LeftPaneWidth));
				Console.WriteLine("Hydraulics = " + mm.HydEnabled.ToString() + "    Index = " + mm.IndexActive.ToString() + "      Grip = " + mm.GripClosed.ToString());
				Console.WriteLine();
				
				if (mm.IndexActive)
					BarDisplayChar = '-';
				else
					BarDisplayChar = '=';
				
				for (int axis = 5; axis >=0; axis--)
				{
					Console.WriteLine();
					Console.Write(SplitSitter.MiniMaster.JointNames[axis].PadLeft(LeftPaneWidth-2) + "  " );
					Console.WriteLine(BarGraphString('.', mm.MasterAbsolute[axis], SplitSitter.MiniMaster.CommandMin[axis],  SplitSitter.MiniMaster.CommandMaximum[axis], 46));
					Console.Write("".PadRight(LeftPaneWidth));
					Console.WriteLine(BarGraphString(BarDisplayChar, mm.CumulativePosition[axis], SplitSitter.MiniMaster.CommandMin[axis],  SplitSitter.MiniMaster.CommandMaximum[axis], 46));
					Console.WriteLine();
				}
			}
		}
	}
}