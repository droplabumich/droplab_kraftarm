/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2015, University of Colorado, Boulder
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Univ of CO, Boulder nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Dave Coleman
   Desc:   Example ros_control hardware interface blank template for the RRBot
           For a more detailed simulation example, see sim_hw_interface.cpp
*/

#include <kraft_rrbot_control/rrbot_hw_interface.h>

namespace rrbot_control
{

RRBotHWInterface::RRBotHWInterface(ros::NodeHandle &nh, urdf::Model *urdf_model)
  : ros_control_boilerplate::GenericHWInterface(nh, urdf_model)
{
  mode = 0;
  robot=new ROSKraft::Kraft(nh);
  ROS_INFO_NAMED("rrbot_hw_interface", "RRBotHWInterface Ready.");
  joint_cmds_pub = nh_.advertise<kraft_ros_control_boilerplate::Float64MultiArrayStamped>(CMD_TOPIC,1);
  joint_fbk_pub = nh_.advertise<kraft_ros_control_boilerplate::Float64MultiArrayStamped>(FBK_TOPIC,1);
  gripper_open_close = nh_.subscribe<std_msgs::Int8>("/kraft_arm/gripper", 1, &RRBotHWInterface::gripperCallback, this);
}

void RRBotHWInterface::gripperCallback(const std_msgs::Int8 data)
{
    gripper_command = data.data;
    mode = gripper_command;
    std::cout<<"Receiving   ......."<<mode<<std::endl;
    std::cout.flush();
}

void RRBotHWInterface::send_cmd_pots(double potscmd[7])
{  
   kraft_ros_control_boilerplate::Float64MultiArrayStamped msg;
   msg.header.stamp = ros::Time::now(); 
   std::vector<double> vec(7, 0);
   for (int i = 0; i<7; i++)
   {
    vec[i] = potscmd[i];
   }
   msg.array.data = vec;
   joint_cmds_pub.publish(msg);
}

void RRBotHWInterface::send_fbk_pots(double potsfbk[7])
{
   kraft_ros_control_boilerplate::Float64MultiArrayStamped msg;
   msg.header.stamp = ros::Time::now();
   std::vector<double> vec(7, 0);
   for (int i = 0; i<7; i++)
   {
    vec[i] = potsfbk[i];
   }
   msg.array.data = vec;
   joint_fbk_pub.publish(msg);
}

void RRBotHWInterface::read(ros::Duration &elapsed_time)
{
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
  //
  // FILL IN YOUR READ COMMAND FROM USB/ETHERNET/ETHERCAT/SERIAL ETC HERE
  //
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------

  double q[7]; // Expect size(q) = 7
  double potsfb[7];

  robot->getJointAngles(q);
  // q[0] = 0.34;
  // q[2] = 0.75;
//  q[0] = 1.57;
  // q[3] = 0.0;
  // q[4] = 0.0;
  robot->getJointPotsFeedback(potsfb);
  for (std::size_t joint_id = 0; joint_id < num_joints_; joint_id++) {
     joint_position_[joint_id] = q[joint_id];
    // cout << "Joint " << joint_id << ": " << q[joint_id] << endl;
   }
 // cout << "Reading socket..." << endl;
  robot->UdpRx();
  send_fbk_pots(potsfb);

  //std::cerr << "Read Joints: q0: " << q[0] << " q1: " << q[1] << " q2: " << q[2] << " q3: " << q[3] << " q4: " << q[4] << " q5: " << q[5] << " q6: " << q[6] std::endl;
   
}

void RRBotHWInterface::write(ros::Duration &elapsed_time)
{
  // Safety
  enforceLimits(elapsed_time);

  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
  //
  // FILL IN YOUR WRITE COMMAND TO USB/ETHERNET/ETHERCAT/SERIAL ETC HERE
  //
  // FOR A EASY SIMULATION EXAMPLE, OR FOR CODE TO CALCULATE
  // VELOCITY FROM POSITION WITH SMOOTHING, SEE
  // sim_hw_interface.cpp IN THIS PACKAGE
  //
  // DUMMY PASS-THROUGH CODE
  // for (std::size_t joint_id = 0; joint_id < num_joints_; ++joint_id)
  //   joint_position_[joint_id] = joint_position_command_[joint_id];
  // END DUMMY CODE
  //
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------

  double q_command[7];
  double potscmd[7];
  robot->getJointPotsCommands(potscmd);
 // cout << "Setting joint values:\n";
  for (std::size_t joint_id = 0; joint_id < num_joints_; ++joint_id) {
     q_command[joint_id] = joint_position_command_[joint_id];
    // cout << joint_position_command_[joint_id] << ", ";
  }
  //cout << endl;

  //std::cerr << "Write Joints: q0: " << q_command[0] << " q1: " << q_command[1] << " q2: " << q_command[2] << " q3: " << q_command[3] << " q4: " << q_command[4] << " q5: " << q_command[5] << " q6: " << q_command[6] <<std::endl;
  //cout << "Writing socket..." << endl;
  //robot->SendWakeup();
  robot->UdpTx(q_command);
  send_cmd_pots(potscmd);

  // robot->setJointValues(q_command, false);
}

void RRBotHWInterface::enforceLimits(ros::Duration &period)
{
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
  //
  // CHOOSE THE TYPE OF JOINT LIMITS INTERFACE YOU WANT TO USE
  // YOU SHOULD ONLY NEED TO USE ONE SATURATION INTERFACE,
  // DEPENDING ON YOUR CONreceivedROL METHOD
  //
  // EXAMPLES:
  //
  // Saturation Limits ---------------------------
  //
  // Enforces position and velocity
  pos_jnt_sat_interface_.enforceLimits(period);
  //
  // Enforces velocity and acceleration limits
  // vel_jnt_sat_interface_.enforceLimits(period);
  //
  // Enforces position, velocity, and effort
  // eff_jnt_sat_interface_.enforceLimits(period);

  // Soft limits ---------------------------------
  //
  // pos_jnt_soft_limits_.enforceLimits(period);
  // vel_jnt_soft_limits_.enforceLimits(period);
  // eff_jnt_soft_limits_.enforceLimits(period);
  //
  // ----------------------------------------------------
  // ----------------------------------------------------
  // ----------------------------------------------------
}

}  // namespace
