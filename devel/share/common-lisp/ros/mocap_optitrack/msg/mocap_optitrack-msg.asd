
(cl:in-package :asdf)

(defsystem "mocap_optitrack-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "RigidBodyMsg" :depends-on ("_package_RigidBodyMsg"))
    (:file "_package_RigidBodyMsg" :depends-on ("_package"))
  ))