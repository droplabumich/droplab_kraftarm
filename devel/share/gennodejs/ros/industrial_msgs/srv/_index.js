
"use strict";

let SetRemoteLoggerLevel = require('./SetRemoteLoggerLevel.js')
let GetRobotInfo = require('./GetRobotInfo.js')
let SetDrivePower = require('./SetDrivePower.js')
let StartMotion = require('./StartMotion.js')
let CmdJointTrajectory = require('./CmdJointTrajectory.js')
let StopMotion = require('./StopMotion.js')

module.exports = {
  SetRemoteLoggerLevel: SetRemoteLoggerLevel,
  GetRobotInfo: GetRobotInfo,
  SetDrivePower: SetDrivePower,
  StartMotion: StartMotion,
  CmdJointTrajectory: CmdJointTrajectory,
  StopMotion: StopMotion,
};
