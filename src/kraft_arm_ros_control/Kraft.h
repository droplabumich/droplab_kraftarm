#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <arpa/inet.h>
#include <streambuf>
#include <cstring>
#include <cstdint>
#include <math.h>

using namespace std;

namespace ROSKraft
{	
	class Kraft
	{
		private: 
		int TxAck=0; // good exchanges
		int TxNack=0; // bad exchanges
		int RxAck=0; // good exchanges
		int RxNack=0; // bad exchanges
		int UdpClient=0;
		const string JointNames[7] = {"Azimuth","Elevator","Elbow", "Pitch","Yaw","Rotate","Grip"};
		static const int NumAxes = 7;
		const int CommandMin[7] = {700, 1800, 1400, 1300, 1200, 0, 0};
		const int CommandMaximum[7] = {2600, 2850, 2700, 2700, 2600, 255, 200};
		
		//public static float[] Acceleration = new float[]{ 0.04f, 0.5f,          0.1f,    0.1f,  0.08f};
		// note tested acceleration 1 is way too high. 0.1 is fast. 0.01 is slow but usable (for yaw 1/17/2012)
		// 2012/1/22: try reducing elevator from 0.5 down to 0.08. Yaw from 0.08 down to 0.03.
		// 2012/3/19: changed from an acceleration array to a single scalar value.
		
		float Acceleration = 0.1;
		// Update 2012/3/19. All of this assumes a nominal 10 ms subsea update rate, with a 8 ms Sleep in the TX loop.
		// Found that there is probably no need to go slower than 0.1.
		// 2 is plenty fast enough to do "shaking" maneuvers, only appropriate for a master period of 10 ms.
		// 1 is good for 20 ms. scale to 0.1 at 100 ms.
		// Therefore, the formula should be: Acceleration = 20 / MasterPeriodMs; // 1 for 20 ms, 0.2 for 100ms
		// Acceleration is continuously updated in the log thread, consistent with the Master update rate.
		

		
		public: int CommandSpan(int axis)
			{
				return CommandMaximum[axis] - CommandMin[axis];
			}
		
		private: int Command[NumAxes]; // the number sent to RSD, 12 bit, equal to Fbk*2
		protected: int Feedback[NumAxes]; //the number returnd from RSD, 11 bit
		//public int[] Target = new int[;
		public: int Target[NumAxes]; // the target for the trajectory, same units as command
		//public int yawTgt=1600; // safe place for yaw
		public: float Speed[NumAxes]; // need this for path planning
		
		// public: std::string getLogString() // not migrated to Micro Framework
		// {
		// 	std::string sb;          //String sb = new System.Text.StringBuilder();
		// 	if (HydraulicsEnabled)
		// 		sb.append("1 ");
		// 	else
		// 		sb.append("0 ");
		// 	for (int i=0; i<NumAxes; i++)
		// 	{
		// 		sb.append(printf("%06d ", Target[i]));
		// 		sb.append(printf("%06.2f ", Speed[i]));
		// 		sb.append(printf("%06d ", Command[i]));
		// 		sb.append(printf("%06d ", Feedback[i]));
		// 	}
		// 	sb.append(printf("%06d ", TxAck));
		// 	sb.append(printf("%06d ", TxNack));
		// 	sb.append(printf("%06d ", RxAck);
		// 	sb.append(printf("%06d ", RxNack));
		// 	sb.append(printf("%d ", SubseaPeriodMin));
		// 	sb.append(printf("%d ", SubseaPeriodMax));

		// 	sb.append("  ");
		// 	sb.append(printf("%d ", MasterPeriodMin));
		// 	sb.append(printf("%d ", MasterPeriodMax));
		// 	sb.append(printf("%05.3f", Acceleration));
			
		// 	return sb;
		// }
		
		
		// These comms statistics are periodically refreshed in the LogLoop, but
		// individual periods are checked against min/max in their respective loops.
		long double SubseaPreviousRx = time(0)*1000;
		int SubseaPeriodMax = 0;
		int SubseaPeriodMin = 999;
		
		long double MasterPreviousUpdate = time(0)*1000;
		int MasterPeriodMax = 0;
		int MasterPeriodMin = 999;
		
		
		private: bool hydraulicsEnabled = false;
		public: bool getHydraulicsEnabled()
		{
			return hydraulicsEnabled;
		}
		public: void setHydraulicsEnabled(bool value)
		{
			if ((value) && (!hydraulicsEnabled))
			{
				for (int i =0; i<5; i++)
				{
					Command[i] = Feedback[i]*2;
					Target[i] = Command[i];
				}
			}
			hydraulicsEnabled = value;
		}
		
		// WristSpeed is going to be used in an unexpected way - to spy on the master recv to get the MasterRate.
		public: int getWristSpeed()
		{
			return Command[5];
		}
		public: void setWristSpeed(int value)
		{
			Command[5] = value;
			
			
			long double MasterRecvNow = time(0)*1000;
			int Elapsed = (int) (MasterRecvNow - MasterPreviousUpdate);
			if (MasterPeriodMax < Elapsed)
				MasterPeriodMax = Elapsed;
			if (MasterPeriodMin > Elapsed)
				MasterPeriodMin = Elapsed;
			MasterPreviousUpdate = MasterRecvNow;	
		}
		
		// GripForce is a value 0-128
		// 0 = open
		// 128 = closed
		// there is a neutral value which happens to be 39 or 40 depending on the day.
		public: int getGripForce()
		{
			return Command[6];
		}
		public: void setGripForce(int value)
		{
			Command[6] = value;
		}

		public: bool GripOnLostComms = true;
		private: unsigned char SequenceCounter = 0x10; // set by subsea, echoed by topside
		
		public: int CycleCounter = 0; // counts UPD exchange cycles
		
		// DgiUtil.MultiLogger kss; // used only in udp threads
		
		// bool UnixOS = false;
		// readonly bool EnableLogging; // Readonly means it can only be changed in the constructor, but not later.
		// readonly string LogPath;
		
		public: void Start()
		{
			cout << " initializing RSD comms." << endl;
			
			// Initialize UDP socket
			if ((UdpClient = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
				cerr << "cannot create socket" << endl;
			/* bind to an arbitrary return address */
			/* because this is the client side, we don't care about the address */
			/* since no application will initiate communication here - it will */
			/* just send responses */
			/* INADDR_ANY is the IP address and 0 is the socket */
			/* htonl converts a long integer (e.g. address) to a network representation */
			/* htons converts a short integer (e.g. port) to a network representation */
			struct sockaddr_in myaddr;
			memset((char *)&myaddr, 0, sizeof(myaddr));
			myaddr.sin_family = AF_INET;
			myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
			myaddr.sin_port = htons(0);
			
			if (bind(UdpClient, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0)
				cerr << "bind failed" << endl;

			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
			inet_pton(AF_INET, "192.168.10.151", &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			cout << "Start streaming to subsea RSD . . . " << endl;
			// send a message to the server
			unsigned char WakeupBuffer[] = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
			if (sendto(UdpClient, WakeupBuffer, strlen((char*)WakeupBuffer), 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)	
					cerr << "sendto failed\n";




			RxReady = true; // objects are initialized and ready for Rx Thread to use them.
			

			// Now do experiments:
			//for (int i=0; i<5; i++)
			//	Console.WriteLine(JointNames[i] + " span = " + CommandSpan(i));
			
			// can put these declarations outside Main() as properties if needed for disposing.
			// System.Threading.Thread UdpTxThread; // MUST RUN FAST!! gets started by Start() and killed by Dispose()
			// System.Threading.Thread UdpRxThread; // MUST RUN FAST!! gets started by Start() and killed by Dispose()
			// System.Threading.Thread LogThread; // This thread does not need to be fast.
			
			// UdpTxThread = new System.Threading.Thread(new System.Threading.ThreadStart(UdpTxLoop));	
			// UdpTxThread.IsBackground = true; // makes it die when parent is disposed.
			// UdpTxThread.Start();

			// UdpRxThread = new System.Threading.Thread(new System.Threading.ThreadStart(UdpRxLoop));	
			// UdpRxThread.IsBackground = true; // makes it die when parent is disposed.
			// UdpRxThread.Start();
			

			// LogThread = new System.Threading.Thread(new System.Threading.ThreadStart(LogLoop));	
			// LogThread.IsBackground = true; // makes it die when parent is disposed.
			// LogThread.Start();

		}
		
		bool RxReady = false;

		public: void UdpTx()
		{
			struct hostent *hp; /* host information */
			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
            inet_pton(AF_INET, "192.168.10.151", &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);

			/// UPDATE 1/25/2012: Microsoft implementation of UDPClient is really difficult to understand. It is much clearer to use raw sockets, as documented in Mono docs. Converted microsoft calls to the mono/ubuntu format. Will they still work in windows? (May-9-2012, Answer: No.)
			// Set up a UDP socket to use for communication with the arm.
			// The UdpClient object is used for both TX and RX.
			// The port specified here [becomes the "source" for TX, and also] filters the RX. This isn't the case for linux though, which assigns a random source.
			// Update 3/19/2012: On linux this constructor does not set the source port, it only filters the incoming packets.
			// We're using a different method for RX, where these rx data structures are completely separate from the sender.
			
			//kftRxSock = new UdpClient(1500);
			
			// this is an alternate way to do it, using only raw sockets, so as to be able to control the source port.
			
			//kftTxSnd = new Sender(new System.Net.IPEndPoint(IPAddress.Parse("192.168.10.151"),1500));
			
			//kftTxSnd = new Sender(new System.Net.IPEndPoint(IPAddress.Parse("127.0.0.1"),1500));
			
			// This is where we specify the destination for TX only. [[DEPRECATED]]
			//kftTxSock.Connect("192.168.10.151", 15000); // normal address for "RSD"
			//kftTxSock.Connect("127.0.0.1", 1500); // normal address for "RSD"
			
			
			// set up receive timeout - default is infinity - we will use a short timeout instead
			// NO TIMEOUT kftSock.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 8);
			//kftSock.Client.ReceiveTimeout = 25; this does not work
			// Create an IPEndPoint to receive messages. A port of '0' means "any available".
			
			//kftRecvpt = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);//1501) ;
			
			// later, we can get new packets with this command:   data = kftRxSock.Receive(ref kftRecvpt);

			//int RecentFbIdx = 0x00; now called sequencecounter
			
			
			// Declare some unsigned char arrays to use for UDP messages.
			//var WakeupBuffer = new unsigned char[] {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
			// Note: Leave a zero unsigned char at the end for the checksum which will replace it.
			//var OutBuffer[18] = new unsigned char[18];
			unsigned char OutBuffer[] = { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 };
			//OutBuffer = new unsigned char[] { 0x55,1,0,1,1,0,1,1,0,0x80, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle 
			//OutBuffer = new unsigned char[] { 0x55,0xd4,0xa2,0xdc,0x6a,0x85,0x15,0x00,0x08,0x97, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x1a, 0, 0 }; // enabled
			//OutBuffer = new unsigned char[] { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 }; // gripping?

			Command[5] = 128; // no rotate
			Command[6] = 0; // open gripper
			
			PopulateOutgoingCommandPacket(OutBuffer);
			FinalizePacket(OutBuffer);			
			// now send
			try
			{
				CycleCounter++;
				//Console.WriteLine("TX: " + BitConverter.ToString(OutBuffer));
				////////////txk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(OutBuffer));
				if (sendto(UdpClient, OutBuffer, strlen((char*)OutBuffer), 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)	
					cerr << "sendto failed\n";
				// this will throw an exception if Ethernet is interrupted,
				// because the destination becomes unreachable.
				TxAck++;
			}
			catch(exception e)
			{
				cout << "Send UDP Failed: " << e.what() << endl;
				// if (this.EnableLogging)
				// 	kss.Log("Send UDP Failed: "+ e.what() );
				this->TxNack++;
			}
			
			// changed 5-> 8ms to see if it would reduce collisions. I think it does 1/25/2012.
			//System.Threading.Thread.Sleep(8);
			// 10 ms works fine, mono, 2012/2/2
			// 20 ms works fine, mono, 2012/2/2
			// 40 ms doesn't work at all. comms but hydraulics never enable. 2012/2/2
			// 30 ms doesn't work at all.
			// 25 ms doesn't work at all.
			// 21 ms works sometimes, freezes occasionally. 2012/2/2
			// 19 ms works reliably for a few minutes but does rarely freeze 2012/2/2

		}// UdpTx
			
		// private: void UdpTxLoop()
		// {
			
		// 	/// UPDATE 1/25/2012: Microsoft implementation of UDPClient is really difficult to understand. It is much clearer to use raw sockets, as documented in Mono docs. Converted microsoft calls to the mono/ubuntu format. Will they still work in windows? (May-9-2012, Answer: No.)
		// 	// Set up a UDP socket to use for communication with the arm.
		// 	// The UdpClient object is used for both TX and RX.
		// 	// The port specified here [becomes the "source" for TX, and also] filters the RX. This isn't the case for linux though, which assigns a random source.
		// 	// Update 3/19/2012: On linux this constructor does not set the source port, it only filters the incoming packets.
		// 	// We're using a different method for RX, where these rx data structures are completely separate from the sender.
		// 	kftRxSock = new UdpClient(1500);
		// 	// this is an alternate way to do it, using only raw sockets, so as to be able to control the source port.
		// 	kftTxSnd = new Sender(new System.Net.IPEndPoint(IPAddress.Parse("192.168.10.151"),1500));
		// 	//kftTxSnd = new Sender(new System.Net.IPEndPoint(IPAddress.Parse("127.0.0.1"),1500));
			
		// 	// This is where we specify the destination for TX only. [[DEPRECATED]]
		// 	//kftTxSock.Connect("192.168.10.151", 15000); // normal address for "RSD"
		// 	//kftTxSock.Connect("127.0.0.1", 1500); // normal address for "RSD"
			
			
		// 	// set up receive timeout - default is infinity - we will use a short timeout instead
		// 	// NO TIMEOUT kftSock.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 8);
		// 	//kftSock.Client.ReceiveTimeout = 25; this does not work
		// 	// Create an IPEndPoint to receive messages. A port of '0' means "any available".
		// 	kftRecvpt = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);//1501) ;
		// 	// later, we can get new packets with this command:   data = kftRxSock.Receive(ref kftRecvpt);

		// 	//int RecentFbIdx = 0x00; now called sequencecounter
			
			
		// 	// Declare some unsigned char arrays to use for UDP messages.
		// 	//var WakeupBuffer = new unsigned char[] {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
		// 	unsigned char WakeupBuffer[] = {0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f,0x7f};
		// 	// Note: Leave a zero unsigned char at the end for the checksum which will replace it.
		// 	//var OutBuffer[18] = new unsigned char[18];
		// 	unsigned char OutBuffer[18] = { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 };
		// 	//OutBuffer = new unsigned char[] { 0x55,1,0,1,1,0,1,1,0,0x80, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x02, 0, 0 }; // idle 
		// 	//OutBuffer = new unsigned char[] { 0x55,0xd4,0xa2,0xdc,0x6a,0x85,0x15,0x00,0x08,0x97, 0, 0x85, 0x8C, 0x0C, 0xF4, 0x1a, 0, 0 }; // enabled
		// 	//OutBuffer = new unsigned char[] { 0x55,0x48,0xa4,0x99,0x6f,0x86,0x79,0x89,0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 }; // gripping?

		// 	Command[5] = 128; // no rotate
		// 	Command[6] = 0; // open gripper
			
		// 	std::cout << "Start streaming to subsea RSD . . . " << std::endl;
		// 	//Console.ReadKey(true);
			
		// 	kftTxSnd.Send(WakeupBuffer);//, WakeupBuffer.Length);
			
		// 	RxReady = true; // objects are initialized and ready for Rx Thread to use them.
			
		// 	while (true)
		// 	{
		// 		PopulateOutgoingCommandPacket(OutBuffer);
		// 		FinalizePacket(OutBuffer);			
		// 		// now send
		// 		try
		// 		{
		// 			CycleCounter++;
		// 			//Console.WriteLine("TX: " + BitConverter.ToString(OutBuffer));
		// 			////////////txk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(OutBuffer));
		// 			kftTxSnd.Send(OutBuffer); //, OutBuffer.Length);
		// 			// this will throw an exception if Ethernet is interrupted,
		// 			// because the destination becomes unreachable.
		// 			TxAck++;
		// 		}
		// 		catch(ExceptionName e)
		// 		{
		// 			std::cout << "Send UDP Failed: " << e.what() << std::endl;
		// 			// if (this.EnableLogging)
		// 			// 	kss.Log("Send UDP Failed: "+ e.what() );
		// 			this.TxNack++;
		// 		}
				
		// 		// changed 5-> 8ms to see if it would reduce collisions. I think it does 1/25/2012.
		// 		System.Threading.Thread.Sleep(8);
		// 		// 10 ms works fine, mono, 2012/2/2
		// 		// 20 ms works fine, mono, 2012/2/2
		// 		// 40 ms doesn't work at all. comms but hydraulics never enable. 2012/2/2
		// 		// 30 ms doesn't work at all.
		// 		// 25 ms doesn't work at all.
		// 		// 21 ms works sometimes, freezes occasionally. 2012/2/2
		// 		// 19 ms works reliably for a few minutes but does rarely freeze 2012/2/2
				
		// 	}// whiletrue

		// }// UdpTxLoop
		
		
		// // LogLoop has responsibility for logging periodically,
		// // and also updating the comms statistics,
		// // and also updating acceleration (within limits).
		// private: void LogLoop()
		// {
		// 	// Two events are at 10 Hz and 5 sec.
		// 	int LogDecimationCount=0;
		// 	float UnfilteredAcceleration;
			
		// 	// if (this.EnableLogging)
		// 	// {
		// 	// 	kss = new DgiUtil.MultiLogger("KSS", this.LogPath);
		// 	// }
			
		// 	while (true)
		// 	{
		// 		System.Threading.Thread.Sleep(100);
				
		// 		// log something at 10 Hz
		// 		// if (this.EnableLogging)
		// 		// 	kss.Log(this.LogString);
			
		// 		// print something on the screen and update acceleration every 5 seconds
		// 		if (LogDecimationCount == 50)
		// 		{
		// 			std::cout << "Cycles/tx/rx " << CycleCounter << "/" << TxAck << "/" << RxAck 
		// 			          << " Master/Subsea Min/Max (ms): " << MasterPeriodMin
		// 			          << "/" << MasterPeriodMax << " " << SubseaPeriodMin << "/" << SubseaPeriodMax
		// 			          << " acc=" << printf("%04.2f", Acceleration) << std::endl;
					
		// 			if (MasterPeriodMax == 0) // This means no updates are occurring.
		// 				UnfilteredAcceleration = 0.0; // Avoid division by zero.
		// 			else
		// 				UnfilteredAcceleration = 20.0 / (float) MasterPeriodMax;
		// 			if (UnfilteredAcceleration > 2.0)
		// 				Acceleration = 2.0;
		// 			else if (UnfilteredAcceleration < 0.1)
		// 				Acceleration = 0.1;
		// 			else
		// 				Acceleration = UnfilteredAcceleration;
					
		// 			// Therefore, max slew rate is with master period 200 ms.
		// 			// Acceleration is 0.1. Full scale slew rate is 8 seconds
		// 			// Min slew rate is with master period 10 ms. Acceleration is 2 steps per cycle per cycle.
		// 			// Full scale slew rate is 0.4 seconds.
						
		// 			MasterPeriodMax = 0;
		// 			MasterPeriodMin = 999;
		// 			SubseaPeriodMax = 0;
		// 			SubseaPeriodMin = 999;
					
		// 			LogDecimationCount = 0;
		// 		}
		// 		else
		// 			LogDecimationCount++;		
		// 	}
		// }

		
		public: void UdpRx()
		{
			unsigned char indata[2048] = {};
			long double RecvNow;
			int Elapsed;


			struct hostent *hp; /* host information */
			struct sockaddr_in servaddr; /* server address */
			/* fill in the server's address and data */
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			//servaddr.sin_addr.s_addr = htonl(inet_pton("192.168.10.151"));
                        inet_pton(AF_INET, "192.168.10.151", &(servaddr.sin_addr));
			servaddr.sin_port = htons(1500);
			socklen_t addrlen = sizeof(servaddr); /* length of addresses */
			int recvlen; /* # unsigned chars received */
			
			if (RxReady)
			{
				try
				{
					recvlen = recvfrom(UdpClient, indata, 2048, 0, (struct sockaddr *)&servaddr, &addrlen);
					////////////rxk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(indata) );
					if (recvlen > 16){
						SequenceCounter = indata[16];
						ReadFeedbackFromPkt(indata);
						PathPlanner(); // takes care of axes 0-5.
						RxAck++;
						RecvNow = time(0)*1000;
						Elapsed = (int) (RecvNow - SubseaPreviousRx);
						if (SubseaPeriodMax < Elapsed)
							SubseaPeriodMax = Elapsed;
						if (SubseaPeriodMin > Elapsed)
							SubseaPeriodMin = Elapsed;
						SubseaPreviousRx = RecvNow;
					}
					else
					{
						// if (this.EnableLogging)
						// 	kss.Log("ERR", "Rx packet too short. Ignored.");
						RxNack++;
					}
				}
				catch (exception e)
				{
					cout << "UDP receive Failed: " << e.what() << " " << CycleCounter << endl;// (timeout/unreachable).");
					// if (this.EnableLogging)
					// 	kss.Log("ERR", "rx timeout: " + e.Message);
					RxNack++;
				}
	       }
		}	
		

		// private: void UdpRxLoop()
		// {
		// 	unsigned char[] indata;
		// 	long double RecvNow;
		// 	int Elapsed;
			
		// 	while (!RxReady)
		// 	{
		// 		System.Threading.Thread.Sleep(10);
		// 	}

		// 	while (RxReady)
		// 	{
		// 		try
		// 		{
		// 			indata = kftRxSock.Receive(ref kftRecvpt);
		// 			////////////rxk.Log(CycleCounter.ToString("000000 ") + PrettyPrintPacket(indata) );
		// 			if (indata.Length > 16){
		// 				SequenceCounter = indata[16];
		// 				ReadFeedbackFromPkt(indata);
		// 				PathPlanner(); // takes care of axes 0-5.
		// 				RxAck++;
		// 				RecvNow = time(0)*1000;
		// 				Elapsed = (int) (RecvNow - SubseaPreviousRx));
		// 				if (SubseaPeriodMax < Elapsed)
		// 					SubseaPeriodMax = Elapsed;
		// 				if (SubseaPeriodMin > Elapsed)
		// 					SubseaPeriodMin = Elapsed;
		// 				SubseaPreviousRx = RecvNow;
		// 			}
		// 			else
		// 			{
		// 				// if (this.EnableLogging)
		// 				// 	kss.Log("ERR", "Rx packet too short. Ignored.");
		// 				RxNack++;
		// 			}
		// 		}
		// 		catch (System.Net.Sockets.SocketException e)
		// 		{
		// 			std::cout << "UDP receive Failed: " << e.Message << " " << CycleCounter << std::endl;// (timeout/unreachable).");
		// 			// if (this.EnableLogging)
		// 			// 	kss.Log("ERR", "rx timeout: " + e.Message);
		// 			RxNack++;
		// 		}
	 //       }
		// }
		
		private: void PopulateOutgoingCommandPacket(unsigned char *pkt)
		{
			// first set the status unsigned char, #15. Four possible states. Maybe more.
			if (hydraulicsEnabled)
			{
				if (GripOnLostComms)
					pkt[15] = 0x1a;
				else
					pkt[15] = 0x12;	
			}
			else
			{
				if (GripOnLostComms)
					pkt[15] = 0x02;
				else
					pkt[15] = 0x0a;
			}
			
			/// Now the reverse of the decoding:
			/// 		public void ReadCommandFromPkt(unsigned char[] pkt)
		
			//Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			//Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			//Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			//Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			//Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits

		
			pkt[1] = (unsigned char) (Command[0] & 0xFF);
			pkt[2] = (unsigned char) (((Command[0] >> 8) & 0x0F) | ((Command[1] >> 4) & 0xF0));
			pkt[3] = (unsigned char) (Command[1] & 0xFF);
			pkt[4] = (unsigned char) (Command[2] & 0xFF);
			pkt[5] = (unsigned char) (((Command[2] >> 8) & 0x0F) | ((Command[3] >> 4) & 0xF0));
			pkt[6] = (unsigned char) (Command[3] & 0xFF);
			pkt[7] = (unsigned char) (Command[4] & 0xFF);
			pkt[8] = (unsigned char) ((Command[4] >> 8) & 0x0F);
			pkt[9] = (unsigned char) (Command[5] & 0xFF); // rotate 8 bits
			pkt[10] = (unsigned char) (Command[6] & 0xFF); // gripper 8 bits
			
			//Console.WriteLine("Grip Command = " + Command[6]);


		}
		
		private: void FinalizePacket(unsigned char *pkt)
		{
			unsigned char expecting;
			string alert;
			expecting = (unsigned char)((int)pkt[16]+1);
			
			if (SequenceCounter != expecting)
			{
				TxNack++;
				sprintf(alert,"Out of sequence packet, got %u, expecting %u.", SequenceCounter, expecting)
				cout << SequenceCounter << endl;
				//alert = "Out of sequence packet, got " + string(1,SequenceCounter) + ", expecting " + string(1,expecting) + ".";
				cout << alert << endl;
				// if (this.EnableLogging)
				// 	kss.Log("ERR", alert);
			}
			
			pkt[16] = (unsigned char) SequenceCounter;
			
			FillInKraftChecksum(pkt);

		}
		
		private: static void FillInKraftChecksum(unsigned char *pkt)
		{
			int cksum = 0;
			pkt[17]=0;
			for (int i=0;i<17;i++)
				cksum += pkt[i];
			pkt[17] = (unsigned char) cksum;
		}

		public: void ReadCommandFromPkt(unsigned char *pkt)
		{
			Command[0] = pkt[1] +  (pkt[2] & 0x0F) * 256; // // azimuth 12 bits
			Command[1] = pkt[3] + (pkt[2] & 0xF0) * 16; // // elevator 12 bits
			Command[2] = pkt[4] + (pkt[5] & 0x0F) * 256; // elbow command 12 bits
			Command[3] = pkt[6] + (pkt[5] & 0xF0) * 16; // pitch command 12 bits
			Command[4] = pkt[7] + (pkt[8] & 0x0F) * 256; // yaw 12 bits
			Command[5] = pkt[9]; // wrist rotate 8 bits.
			Command[6] = pkt[10]; // grip 8 bits
		}
		
		public: void ReadFeedbackFromPkt(unsigned char *pkt)
		{
			Feedback[0] = pkt[1] + (pkt[2] & 0x0E) * 128; // wrist 11 bits
			Feedback[1] = pkt[3] + (pkt[2] & 0xE0) * 8;   // elevator 11 bits
			Feedback[2] = pkt[4] + (pkt[5] & 0x0E) * 128; // elbow position 11 bits
			Feedback[3] = pkt[6] + (pkt[5] & 0xE0) * 8;   // wrist pitch 11 bits
			Feedback[4] = pkt[7] + (pkt[8] & 0x0E) * 128; // wrist yaw 11 bits
			Feedback[5] = pkt[9] + (pkt[8] & 0xE0) * 8;   // wrist rotate 11 bits.
			// NOTE NO GRIP FEEDBACK !!!
		}
		
		private: void PathPlanner()
		{
			if (hydraulicsEnabled)
			{
				for (int axis=0; axis<5; axis++)	
				{
					int togo = Target[axis]-Command[axis];
					
					// If we can stop in one step, and can get there in one step, 
					if ((fabs(Speed[axis]) <= Acceleration) && (((float) togo <= (Speed[axis] + Acceleration)) && ((float)togo >= Speed[axis] - Acceleration)))
					{
						Command[axis] = Target[axis];
						Speed[axis] = 0.0;
						// then get there in one step and stop!
					}
					else // Target not accessible in one step, adjust speed and keep going
					{
						if ((togo >= 0) && (Speed[axis] <= 0)) // going the wrong way, reverse
							Speed[axis] += Acceleration;
						else if ((togo <= 0) && (Speed[axis] >=0)) // going the wrong way, reverse
							Speed[axis] -= Acceleration;
						else if (togo > ((Speed[axis] * Speed[axis]) / (2 * Acceleration)))
							Speed[axis] += Acceleration; // positive speedup
						else if (togo < ((Speed[axis] * Speed[axis]) / (-2 * Acceleration)))
							Speed[axis] -= Acceleration; // negative speedup
						else if (togo > 0) // forward deceleration
							Speed[axis] -= Acceleration;
						else if (togo < 0) // reverse deceleration
							Speed[axis] += Acceleration;
						Command[axis] += (int) Speed[axis];
					}
				}
			}
		}
		

		
		public: static string PrettyPrintPacket ( unsigned char *ind )
		{
			string str = "";
			for ( int i=0; i<strlen((char*)ind); i++)
			{
				str += printf("%02X", ind[i]);
				str += " ";
			}
			// return str.trim(null);
			// remove that very last space for neatness
		}


		public: Kraft()
		{
		}
		
	};

}
