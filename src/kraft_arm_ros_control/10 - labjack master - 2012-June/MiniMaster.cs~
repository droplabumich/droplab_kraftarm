﻿/*
 * Created by SharpDevelop.
 * User: Daniel
 * Date: 1/11/2012
 * Time: 8:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Threading;
// Import the UD .NET wrapper object.  The dll referenced is installed by the
// LabJackUD installer in Program Files (x86) / LabJack/Drivers/DotNet.dll
using LabJack.LabJackUD;

namespace LabJackMaster
{
	/// <summary>
	/// Description of MiniMaster.
	/// </summary>
	public class MiniMaster
	{
		public static int OutPort = 50808;
		
		public static string[] JointNames = new string[] {"Azimuth","Elevator","Elbow", "Pitch","Yaw","Rotate","Grip"};
		public static int[] CommandMin = new int[]    {700,    1800,           1400,    1300,   1200,  0,     0};
		public static int[] CommandHome = new int[]  {1564,    2850,           1419,    1965,   1981,  128,   42};
		public static int[] CommandMaximum = new int[]{2600,   2850,           2700,    2700,   2600,  255,   15};
		static int SpanCmd(int idx)
		{
			return CommandMaximum[idx] - CommandMin[idx];
		}
		
		public static int[] Saturate(int[] wild)
		{
			for (int i=0; i<7; i++)
			{
				if (wild[i] > CommandMaximum[i])
					wild[i] = CommandMaximum[i];
				else if (wild[i] < CommandMin[i])
					wild[i] = CommandMin[i];
			}
			return wild;
		}
				
		public int[] MasterAbsolute = new int[7];
		int[] UnLatchPosition = new int[7];
		int[] LatchedPosition = new int[7];
		public int[] CumulativePosition = new int[7];
		
		public bool HydEnabled = false;
		public bool IndexActive = false;
		public bool GripLock = false;
		public bool ContinuousWrist = false;
		
		// LabJack U3 object
		private U3 u3;
		
		public double[] Pots = new double[8]; // raw voltages, copy to limits below
		public int switches;
		
		static double[] MinVolt = new double[8] {0.039, 0.389, 0.495, 0.090, 0.039, 0.015,  0.222, 0.003};	
		static double[] MaxVolt = new double[8] {2.20,  1.76,  1.740, 2.159, 2.106, 1.013,  2.15 /* was 2.048*/,  2.19}; // deviate from actual limits to correct for rocker center offset	
		static double SpanVolt(int idx)
		{
			return MaxVolt[idx] - MinVolt[idx];
		}
		
		public readonly int EmitPeriod;
		
		public float PeriodAverage = 999;

		
		
		public MiniMaster(int ep)
		{
			EmitPeriod = ep;
			Console.WriteLine("Starting Mini Master.");
			
			//Read and display the UD version.
			double dblDriverVersion = LJUD.GetDriverVersion();
			Console.Out.WriteLine("UD Driver Version = {0:0.000}\n\n",dblDriverVersion);

			//Open the first found LabJack U3.
			u3 = new U3(LJUD.CONNECTION.USB, "0", true); // Connection through USB

			//Start by using the pin_configuration_reset IOType so that all
			//pin assignments are in the factory default condition.
			LJUD.ePut (u3.ljhandle, LJUD.IO.PIN_CONFIGURATION_RESET, 0, 0, 0);
			

			//First some configuration commands.  These will be done with the ePut
			//function which combines the add/go/get into a single call.

			// Set the DAC for 2.44 volts output. Automatically enables the DAC too.
			LJUD.eDAC(u3.ljhandle, 0, 2.44, 0, 0, 0);
			// no further DAC calls are necessary.
			
			//Configure FIO8-FIO15 as analog, all else digital.  That means we
			//will start from channel 0 and update all 16 flexible bits.  We will
			//pass a value of b1111111100000000 or d65280.
			LJUD.ePut (u3.ljhandle, LJUD.IO.PUT_ANALOG_ENABLE_PORT, 0, 65280, 16);
			
			//Configure FIO0,1,2,3 Inputs. They are already inputs by default.
			// no code necessary. Can read them later with a GET_DIGITAL_PORT, starting from #0, length 4.
			// for example LJUD.eGet(u3.ljhandle, LJUD.IO.GET_DIGITAL_PORT, 0, dblValue, 4); then use dblValue.
			
			// Set FIO4,5,6,7 outputs. This is done automatically by setting a value of 1 ( LED not illuminated).
			// Set second 4 bits: b11110000 is the same as d184.
			LJUD.ePut(u3.ljhandle, LJUD.IO.PUT_DIGITAL_PORT, (LJUD.CHANNEL) 4, 15, 4);
			// outputs can be set later using the same call, from any thread.

			
            // initialize CumulativePosition to prepare for indexing.
            for (int i=0; i<6; i++)
            	CumulativePosition[i] = CommandHome[i];
            
            System.Threading.Thread DaqThread = new System.Threading.Thread(BgUpdate);
			DaqThread.IsBackground = true;
			DaqThread.Start();
		}
		
		// If error occured print a message indicating which one occurred. If the error is a group error (communication/fatal), quit
		public void LJShowErrorMessage(LabJackUDException e)
		{
			Console.Out.WriteLine("Error: " + e.ToString());
			if (e.LJUDError > U3.LJUDERROR.MIN_GROUP_ERROR)
			{
				Console.ReadLine(); // Pause for the user
				Environment.Exit(-1);
			}
		}
		
		
		public void ReadADC()
    	{
    		double[] Positions = new double[7];
    		
			LJUD.IO ioType=0;
			LJUD.CHANNEL channel=0;
			double dblValue=0;
			// Variables to satisfy certain method signatures
			int dummyInt = 0;
			double dummyDouble = 0;

			try
			{
				//Execute the LabJack request string.
				LJUD.GoOne (u3.ljhandle);

				//Get all the results.  The input measurement results are stored.  All other
				//results are for configuration or output requests so we are just checking
				//whether there was an error.
				LJUD.GetFirstResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble);
			}
			catch (LabJackUDException e) 
			{
				Console.WriteLine("ERROR");
				LJShowErrorMessage(e);
			}

			bool finished = false;
			while(!finished)
			{
				switch(ioType)
				{

					case LJUD.IO.GET_AIN :
						Pots[((int)channel) - 8] = dblValue; // since we're using ADCs "#AIN8" to "#AIN15"
						break;
						
					case LJUD.IO.GET_DIGITAL_PORT :
						switches=(int) dblValue;
						//Console.WriteLine("GOT SWITCHES");
						break;
				}
				try {LJUD.GetNextResult(u3.ljhandle, ref ioType, ref channel, ref dblValue, ref dummyInt, ref dummyDouble);}
				catch (LabJackUDException e) 
				{
					// If we get an error, report it.  If the error is NO_MORE_DATA_AVAILABLE we are done
					if(e.LJUDError == U3.LJUDERROR.NO_MORE_DATA_AVAILABLE)
						finished = true;
					else
						LJShowErrorMessage(e);
				}
			}
				
    		
    		for (int i=0; i<7; i++)
    		{
    			if (i < 5)
    				Positions[i] = (Pots[i] - MinVolt[i]) / SpanVolt(i);
    			else if (i == 5) // wrist rotate
    				Positions[i] = (MaxVolt[6]-Pots[6]) / SpanVolt(6); // reversed
    			else // (i==6) : gripper
    				Positions[i] = (Pots[i+1] - MinVolt[i+1]) / SpanVolt(i+1);
    			if (Positions[i] > 1)
    				Positions[i] = 1;
    			else if (Positions[i] < 0)
    				Positions[i] = 0;
    			MasterAbsolute[i] = CommandMin[i] + (int) (SpanCmd(i) * Positions[i]);
    		}    			
    	}
		

		
		
		public void WriteLEDs(bool hyd, bool idx, bool grlk, bool cont)
		{	
			int LEDbits = 0x0F; // only the first four bits matter	
			if (hyd)
				LEDbits ^= 0x08;
			if (idx)
				LEDbits ^= 0x04;
			if (grlk)
				LEDbits ^= 0x01;
			if (cont)
				LEDbits ^= 0x02;
			LJUD.ePut(u3.ljhandle, LJUD.IO.PUT_DIGITAL_PORT, (LJUD.CHANNEL) 4, LEDbits, 4);
		}
	
		// probably should work a dispose in here, containing DAQ.Close();
		
		
		public void UpdateLEDs()
		{
			WriteLEDs(HydEnabled, IndexActive, GripLock, ContinuousWrist);
		}

		
		
		void BgUpdate()
		{
			int DebounceMilliseconds = 40;


			//The following commands will use the add-go-get method to group
			//multiple requests into a single low-level function.
			//Request a single-ended reading from AIN8-15.
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 8, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 9, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 10, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 11, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 12, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 13, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 14, 0, 0, 0);
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_AIN, 15, 0, 0, 0);
			//Read digital inputs FIO0 through FIO3.
			LJUD.AddRequest (u3.ljhandle, LJUD.IO.GET_DIGITAL_PORT, 0, 0, 4, 0);
					
			CommandHome.CopyTo(UnLatchPosition,0);
			CommandHome.CopyTo(LatchedPosition,0);
			CommandHome.CopyTo(CumulativePosition,0);
			
			Thread CommandThread = new Thread(new ThreadStart(UDPLoop));
			CommandThread.Start();
			
			while (true)
			{

				ReadADC(); // populate Pots with voltages, Position with 0-1, and "switches".
				// also populates MasterAbsolute in Counts
	
				Thread.Sleep(10);
				
			
				if (HydEnabled && (!IndexActive))
				{
					for (int i=0; i<5; i++)
						CumulativePosition[i] = LatchedPosition[i]+(MasterAbsolute[i]-UnLatchPosition[i]);
					
					CumulativePosition[5] = MasterAbsolute[5]; // wrist rotate is a special case - no offset allowed.
					if (!GripLock)
						CumulativePosition[6] = MasterAbsolute[6]; // gripper is a special case, not indexable
					CumulativePosition = MiniMaster.Saturate(CumulativePosition);
				}
        
				
				if ((switches & 0x8)==0)
				{
					HydEnabled = !HydEnabled;
					IndexActive = true; // no matter which way it changes
					
					Thread.Sleep(DebounceMilliseconds); // debounce button
					while ((switches & 0x8)==0)
						ReadADC();
					Thread.Sleep(DebounceMilliseconds); // debounce button
				}
				if ((switches & 0x4)==0) // index button
				{
					IndexActive = !IndexActive;
					if (IndexActive)
					{
						// newly paused.
						CumulativePosition.CopyTo(LatchedPosition,0);
					}
					else
					{
						// newly restarted
						MasterAbsolute.CopyTo(UnLatchPosition,0);
					}
					Thread.Sleep(DebounceMilliseconds); // debounce button
					while ((switches & 0x4)==0)
						ReadADC();
					Thread.Sleep(DebounceMilliseconds); // debounce button
				}
				if ((switches & 0x1)==0) // grip lock
				{
					GripLock = !(GripLock);
					Thread.Sleep(DebounceMilliseconds); // debounce button
					while ((switches & 0x1)==0)
						ReadADC();
					Thread.Sleep(DebounceMilliseconds); // debounce button
				}
				if ((switches & 0x2)==0) // continuous wrist
				{
					ContinuousWrist = !(ContinuousWrist);
					Thread.Sleep(DebounceMilliseconds); // debounce button
					while ((switches & 0x2)==0)
						ReadADC();
					Thread.Sleep(DebounceMilliseconds); // debounce button
				}
					
				
			}
		}

		
		/// /////////////////// UDP STUFF //////////////////

		System.Net.Sockets.UdpClient mmSock;
		public byte[] LastOut = new byte[] {0,0,0,0,0,0,0,0,0,0};
		

		void UDPLoop()
		{
			byte[] OutBuffer = CookUDP(); // 10 bytes for Classic Packet
			
			// Set up a UDP socket to use for communication with the arm.
			// The UdpClient object mmSock can be used for both TX and RX.
			// The port specified here becomes the "source" for TX, if used, as well as the RX port.
			mmSock = new System.Net.Sockets.UdpClient(); // no port specified
			
			// This is where we specify the destination for TX only.
			mmSock.Connect("192.168.10.255", OutPort); // broadcast, destination port, bypass the acomms
						
			DateTime PeriodCheckpoint = DateTime.UtcNow;
			int PeriodCountSinceCheckpoint = 0;
			
			while (true)
			{
				// Choose one of the following two implementations:
				
				/* Send OutBuffer with one period delay: */
				//Thread.Sleep(EmitPeriod);
				//mmSock.Send(OutBuffer, OutBuffer.Length);
				//LastOut = (byte[]) OutBuffer.Clone();
				
				/* Or send most recent OutBuffer, no delay: */
				mmSock.Send(OutBuffer, OutBuffer.Length);	
				LastOut = (byte[]) OutBuffer.Clone();				
				Thread.Sleep(EmitPeriod);
				
				// Now grab the most recent joint positions:
				OutBuffer = CookUDP();
				PeriodCountSinceCheckpoint++;
				DateTime dtn = DateTime.UtcNow;
				if (dtn > PeriodCheckpoint.AddSeconds(2))
				{
					PeriodAverage = (float) dtn.Subtract(PeriodCheckpoint).TotalMilliseconds  / ((float) PeriodCountSinceCheckpoint);
					PeriodCountSinceCheckpoint = 0;
					PeriodCheckpoint = dtn;
				}
			}
			
		}
		
		byte[] CookUDP()
		{
			byte[] retval = new byte[10];
			byte headbyte = (byte) 'H'; // sentinel value with no hyd, no grip
			
			if (HydEnabled)
				headbyte |= 0x01; // this makes H into I
			
			if (ContinuousWrist)
				headbyte |= 0x02; // now we have J or K
			
			retval[0] = headbyte;
			
			//OutBuffer[3]=(byte) tgt[5]; // wrist rotate
			
			retval[1]=(byte) (CumulativePosition[0]&(0xFF)); // azimuth
			retval[2]=(byte) (((CumulativePosition[0]>>8)&(0x0F)) + ((CumulativePosition[1]>>4)&0xF0));
			retval[3]=(byte) (CumulativePosition[1]&(0xFF)); // elevator
			retval[4]=(byte) (CumulativePosition[2]&(0xFF)); // elbow
			retval[5]=(byte) (((CumulativePosition[2]>>8)&0x0F) + ((CumulativePosition[3]>>4)&0xF0));
			retval[6]=(byte) (CumulativePosition[3]&(0xFF)); // pitch
			retval[7]=(byte) (CumulativePosition[4]&(0xFF)); // yaw
			retval[8]=(byte) (((CumulativePosition[4]>>8)&0x0F) + ((CumulativePosition[6]<<4)&0xF0)); // GRIP 4 bits !!
			retval[9]=(byte) (CumulativePosition[5]&(0xFF)); // rotate - 8 bits only !!
			
			return retval;
		}
		
		
		// This was for Optical Modem special test, not needed any more. If used note two occurrances above.
		byte[] CookUDPExtended()
		{
			byte[] retval = new byte[30];
			byte ombyte = (byte) 'L';
			
			retval[0] = ombyte;
	
			byte[] ClassicMessage = CookUDP();
			
			for (int i=0; i<10; i++)
				retval[i+1] = ClassicMessage[i];
			
			retval[11]=(byte) ' ';
			retval[12]=(byte) 'D';
			retval[13]=(byte) 'G';
			retval[14]=(byte) 'I';
			retval[15]=(byte) ' ';
			retval[16]=(byte) 'i';
			retval[17]=(byte) 's';
			retval[18]=(byte) ' ';
			retval[19]=(byte) 'A';
			retval[20]=(byte) 'W';
			retval[21]=(byte) 'E';
			retval[22]=(byte) 'S';
			retval[23]=(byte) 'O';
			retval[24]=(byte) 'M';
			retval[25]=(byte) 'E';
			retval[26]=(byte) ' ';
			retval[27]=(byte) '!';
			retval[28]=(byte) '!';
			retval[29]=(byte) '!';
			
			return retval;
		}
		
	}
}
    			