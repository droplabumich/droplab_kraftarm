# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kraft/workspace/droplab_kraftarm/src/iai_kinect2/kinect2_registration/src/depth_registration_cpu.cpp" "/home/kraft/workspace/droplab_kraftarm/build/iai_kinect2/kinect2_registration/CMakeFiles/kinect2_registration.dir/src/depth_registration_cpu.cpp.o"
  "/home/kraft/workspace/droplab_kraftarm/src/iai_kinect2/kinect2_registration/src/depth_registration_opencl.cpp" "/home/kraft/workspace/droplab_kraftarm/build/iai_kinect2/kinect2_registration/CMakeFiles/kinect2_registration.dir/src/depth_registration_opencl.cpp.o"
  "/home/kraft/workspace/droplab_kraftarm/src/iai_kinect2/kinect2_registration/src/kinect2_registration.cpp" "/home/kraft/workspace/droplab_kraftarm/build/iai_kinect2/kinect2_registration/CMakeFiles/kinect2_registration.dir/src/kinect2_registration.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DEPTH_REG_CPU"
  "DEPTH_REG_OPENCL"
  "REG_OPENCL_FILE=\"/home/kraft/workspace/droplab_kraftarm/src/iai_kinect2/kinect2_registration/src/depth_registration.cl\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"kinect2_registration\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "/usr/include/eigen3"
  "/home/kraft/workspace/droplab_kraftarm/src/iai_kinect2/kinect2_registration/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/kraft/workspace/droplab_kraftarm/src/iai_kinect2/kinect2_registration/include/internal"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
