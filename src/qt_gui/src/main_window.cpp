/**
 * @file /src/main_window.cpp
 *
 * @brief Implementation for the qt gui.
 *
 * @date February 2011
 **/
/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <QString>
#include <iostream>
#include "../include/qt_gui/main_window.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace qt_gui {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
	ui.setupUi(this); // Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.
    // QObject::connect(ui.actionAbout_Qt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt())); // qApp is a global variable for the application

    ReadSettings();
	mode = 0; 
	goalPoseCounter = 0;
	//std::cout << "current mode" << mode;
	std::cout.flush();
	setWindowIcon(QIcon(":/images/icon.png"));

	qnode.init();
	QObject::connect(&qnode, SIGNAL(newPointSelected(float, float, float)), this, SLOT(handleNewPoint(float, float, float)));
	// ui.tab_manager->setCurrentIndex(0); // ensure the first tab is showing - qt-designer should have this already hardwired, but often loses it (settings?).
    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

    // Initialize class parameters
    selectedPoint.x = 0.0;
    selectedPoint.y = 0.0;
    selectedPoint.z = 0.0;

    // Initialize displayed list of points
	ui.confirmedPointsWidget->addItem("Point 1");
	ui.confirmedPointsWidget->addItem("Point 2");
	ui.confirmedPointsWidget->addItem("Point 3");
	ui.confirmedPointsWidget->setCurrentRow(0);


	/*********************
	** Logging
	**********************/
	// ui.view_logging->setModel(qnode.loggingModel());
    // QObject::connect(&qnode, SIGNAL(loggingUpdated()), this, SLOT(updateLoggingView()));

    /*********************
    ** Auto Start
    **********************/
    // if ( ui.checkbox_remember_settings->isChecked() ) {
    //     on_button_connect_clicked(true);
    // }

}

MainWindow::~MainWindow() {}

/*****************************************************************************
** Implementation [Slots]
*****************************************************************************/

void MainWindow::handleNewPoint(float x, float y, float z) {
	std::stringstream ss;
	ss << std::fixed << std::setprecision(2) << x << ", " << y << ", " << z << std::endl; 
	ui.label->setText(QString::fromStdString(ss.str()));

	selectedPoint.x = x;
	selectedPoint.y = y;
	selectedPoint.z = z;

}

void MainWindow::showNoMasterMessage() {
	QMessageBox msgBox;
	msgBox.setText("Couldn't find the ros master.");
	msgBox.exec();
    close();
}

/*
 * These triggers whenever the button is clicked, regardless of whether it
 * is already checked or not.
 */


void MainWindow::on_button_connect_clicked(bool check ) {
	// if ( ui.checkbox_use_environment->isChecked() ) {
	// 	if ( !qnode.init() ) {
	// 		showNoMasterMessage();
	// 	} else {
	// 		ui.button_connect->setEnabled(false);
	// 	}
	// } else {
	// 	if ( ! qnode.init(ui.line_edit_master->text().toStdString(),
	// 			   ui.line_edit_host->text().toStdString()) ) {
	// 		showNoMasterMessage();
	// 	} else {
	// 		ui.button_connect->setEnabled(false);
	// 		ui.line_edit_master->setReadOnly(true);
	// 		ui.line_edit_host->setReadOnly(true);
	// 		ui.line_edit_topic->setReadOnly(true);
	// 	}
	// }
}


void MainWindow::on_checkbox_use_environment_stateChanged(int state) {
	bool enabled;
	if ( state == 0 ) {
		enabled = true;
		std::cout << "True";
		std::cout.flush();
	} else {
		enabled = false;
	}
	// ui.line_edit_master->setEnabled(enabled);
	// ui.line_edit_host->setEnabled(enabled);
	//ui.line_edit_topic->setEnabled(enabled);
}

/*****************************************************************************
** Implemenation [Slots][manually connected]
*****************************************************************************/

/**
 * This function is signalled by the underlying model. When the model changes,
 * this will drop the cursor down to the last line in the QListview to ensure
 * the user can always see the latest log message.
 */
void MainWindow::updateLoggingView() {
        // ui.view_logging->scrollToBottom();
}

/*****************************************************************************
** Implementation [Menu]
*****************************************************************************/

void MainWindow::on_actionAbout_triggered() {
    QMessageBox::about(this, tr("About ..."),tr("<h2>PACKAGE_NAME Test Program 0.10</h2><p>Copyright Yujin Robot</p><p>This package needs an about description.</p>"));
}

/*****************************************************************************
** Implementation [Configuration]
*****************************************************************************/

void MainWindow::ReadSettings() {
    QSettings settings("Qt-Ros Package", "qt_gui");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
    QString master_url = settings.value("master_url",QString("http://192.168.1.2:11311/")).toString();
    QString host_url = settings.value("host_url", QString("192.168.1.3")).toString();
    //QString topic_name = settings.value("topic_name", QString("/chatter")).toString();
    // ui.line_edit_master->setText(master_url);
    // ui.line_edit_host->setText(host_url);
    //ui.line_edit_topic->setText(topic_name);
    bool remember = settings.value("remember_settings", false).toBool();
    // ui.checkbox_remember_settings->setChecked(remember);
    bool checked = settings.value("use_environment_variables", false).toBool();
    // ui.checkbox_use_environment->setChecked(checked);
    // if ( checked ) {
    // 	ui.line_edit_master->setEnabled(false);
    // 	ui.line_edit_host->setEnabled(false);
    	//ui.line_edit_topic->setEnabled(false);
    // }
}

void MainWindow::WriteSettings() {
    QSettings settings("Qt-Ros Package", "qt_gui");
    // settings.setValue("master_url",ui.line_edit_master->text());
    // settings.setValue("host_url",ui.line_edit_host->text());
    //settings.setValue("topic_name",ui.line_edit_topic->text());
    // settings.setValue("use_environment_variables",QVariant(ui.checkbox_use_environment->isChecked()));
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());
    // settings.setValue("remember_settings",QVariant(ui.checkbox_remember_settings->isChecked()));

}

void MainWindow::closeEvent(QCloseEvent *event)
{
	WriteSettings();
	QMainWindow::closeEvent(event);
}

void MainWindow::on_removeButton_clicked()
{
	int point_idx = ui.confirmedPointsWidget->currentRow();

	std::stringstream ss;
	ss << "Point " << point_idx + 1;
	QListWidgetItem* point_item = ui.confirmedPointsWidget->currentItem();
	point_item->setText(QString::fromStdString(ss.str()));
}

void MainWindow::on_confirmButton_clicked()
{
	int point_idx = ui.confirmedPointsWidget->currentRow();
	selectedPointsVec[point_idx] = selectedPoint;

	std::stringstream ss;
	ss << std::fixed << std::setprecision(2) << selectedPoint.x << ", " << selectedPoint.y << ", " << selectedPoint.z << std::endl; 
	QListWidgetItem* point_item = ui.confirmedPointsWidget->currentItem();
	point_item->setText(QString::fromStdString(ss.str()));
}

void MainWindow::on_genButton_clicked()
{
	geometry_msgs::Quaternion quat; 
	
	//goalPose
	if (mode == 0){ //T-Handle
		
		
		// goalPose.pose.position.x = 
		// goalPose.pose.position.y = 
		// goalPose.pose.position.z = 
		// goalPose.pose.orientation = 
			
	}else if(mode == 1){ //Plane
		
		// goalPose.pose.position.x = 
		// goalPose.pose.position.y = 
		// goalPose.pose.position.z = 
		// goalPose.pose.orientation = 
	}else if(mode == 2){ //Point
		float poseDown[3][3] = {{0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}, {1.0, 0.0, 0.0}};
		quat = rotation_to_quaternion(poseDown);

		goalPose.pose.position.x = selectedPointsVec[0].x;
		goalPose.pose.position.y = selectedPointsVec[0].y;
		goalPose.pose.position.z = selectedPointsVec[0].z;
		goalPose.pose.orientation = quat;
	}

	std::cout << "Goal Pose \n" << "T: " << goalPose.pose.position.x << ", " << goalPose.pose.position.y << ", " << goalPose.pose.position.z
			  << " Quat: " << quat.x << ", " << quat.y << ", " << quat.z << ", " << quat.w << std::endl;
	std::cout.flush();
}

void MainWindow::on_moveButton_clicked()
{
	goalPose.header.seq = goalPoseCounter;
	goalPose.header.stamp = ros::Time::now();
	goalPose.header.frame_id = "Base";
	qnode.sendGoalPose(goalPose);	
	goalPoseCounter++;

	std::cout << "Goal pose sent\n";
	std::cout.flush();
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
	mode = ui.comboBox->currentIndex();
	std::cout << "Switching mode to " << ui.comboBox->currentIndex() << std::endl;
	std::cout.flush();
}


void MyLogWindow::appendMessage(const QString& text)
{
    this->appendPlainText(text); // Adds the message to the widget
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->maximum()); // Scrolls to the bottom
}


}  // namespace qt_gui


