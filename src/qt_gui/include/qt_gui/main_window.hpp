/**
 * @file /include/qt_gui/main_window.hpp
 *
 * @brief Qt based gui for qt_gui.
 *
 * @date November 2010
 **/
#ifndef qt_gui_MAIN_WINDOW_H
#define qt_gui_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui/QMainWindow>
#include <QtGui/QPlainTextEdit>
#include <vector>
#include <string>
#include "ui_main_window.h"
#include "qnode.hpp"
#include "util.h"

#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseStamped.h>

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace qt_gui {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void ReadSettings(); // Load up qt program settings at startup
	void WriteSettings(); // Save qt program settings when closing

	void closeEvent(QCloseEvent *event); // Overloaded function
	void showNoMasterMessage();
public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
	void on_actionAbout_triggered();
	void on_button_connect_clicked(bool check );
	void on_checkbox_use_environment_stateChanged(int state);

	void handleNewPoint(float x, float y, float z);

	void on_removeButton_clicked();

	void on_confirmButton_clicked();

	void on_genButton_clicked();

	void on_moveButton_clicked();

	void on_comboBox_activated(const QString &arg1);
	

    /******************************************
    ** Manual connections
    *******************************************/
    void updateLoggingView(); // no idea why this can't connect automatically

private:
	Ui::MainWindow ui;
	QNode qnode;
	geometry_msgs::Point selectedPoint;
	geometry_msgs::Point selectedPointsVec[3];	
	geometry_msgs::PoseStamped goalPose;
	int mode;
	int goalPoseCounter;


};

/*****************************************************************************
** Interface [LogWindow]
*****************************************************************************/
class MyLogWindow : public QPlainTextEdit
{
    Q_OBJECT
/* snip */
public:
    void appendMessage(const QString& text);
};

}  // namespace qt_gui

#endif // qt_gui_MAIN_WINDOW_H
