
(cl:in-package :asdf)

(defsystem "kraft_ros_control_boilerplate-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Float64MultiArrayStamped" :depends-on ("_package_Float64MultiArrayStamped"))
    (:file "_package_Float64MultiArrayStamped" :depends-on ("_package"))
  ))