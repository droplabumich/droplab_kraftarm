﻿/*
 * Created by SharpDevelop.
 * User: Daniel
 * Date: 1/11/2012
 * Time: 8:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Emant;

namespace SplitSitter
{
	/// <summary>
	/// Description of MiniMaster.
	/// </summary>
	public class MiniMaster
	{
		public static string[] JointNames = new string[] {"Azimuth","Elevator","Elbow", "Pitch","Yaw","Rotate","Grip"};
		public static int[] CommandMin = new int[]    {700,    2000,           1400,    1300,   1200,  0,     0};
		public static int[] CommandCenter = new int[] {1848,   2700,           1515,    2200,   2039,  128,   1};
		public static int[] CommandMaximum = new int[]{2600,   2850,           2700,    2700,   2600,  255,   200};
		
		public static int[] Saturate(int[] wild)
		{
			for (int i=0; i<6; i++)
			{
				if (wild[i] > CommandMaximum[i])
					wild[i] = CommandMaximum[i];
				else if (wild[i] < CommandMin[i])
					wild[i] = CommandMin[i];
			}
			return wild;
		}
				
		public int[] MasterAbsolute = new int[6];
		int[] UnLatchPosition = new int[6];
		int[] LatchedPosition = new int[6];
		public int[] CumulativePosition = new int[6];
		
		public bool HydEnabled = false;
		public bool IndexActive = false;
		public bool GripClosed = false;
		
		Emant300 DAQ = new Emant300();
		static double[] MaxVolt = new double[6] {0.4603, 0.3693, 0.3647, 0.4520, 0.4412, 0.42936};
		static double[] MinVolt = new double[6] {0.0081, 0.0816, 0.1038, 0.0189,  0.0085, 0.04686};
		static double SpanVolt(int idx)
		{
			return MaxVolt[idx] - MinVolt[idx];
		}
		

		
		public MiniMaster()
		{
			bool GoodConfig = true;
            GoodConfig &= DAQ.ConfigDIO(0x8F); // 1=input, 0=output, 0-3,7 in and 4-6 out
            // must set config before calling Open()
            if (!DAQ.Open())
            {
            	Console.WriteLine("Failed to find USB DAQ. Is it plugged in?");
            	Console.Read();
            	return;
            }
            GoodConfig &= DAQ.ConfigPWMCounter(Emant300.PWMORCNT.PWM, Emant300.EVENTORTIMED.Event, 0, 0);
            GoodConfig &= DAQ.ConfigAnalog(2.5, Emant300.POLARITY.Unipolar, 100, Emant300.VREF.V2_5, true);
           
            if (!GoodConfig)
            {
            	Console.WriteLine("Bad Config. Exiting...");
            	System.Threading.Thread.Sleep(100);
            	return;
            }
            
            // Put current thru the pots,
            DAQ.WriteAnalog(0.99); // current in milliamps, max=1mA
            //  so they have a voltage.
            
            // initialize CumulativePosition.
            for (int i=0; i<6; i++)
            	CumulativePosition[i] = CommandCenter[i];
            
            System.Threading.Thread DaqThread = new System.Threading.Thread(BgUpdate);
			DaqThread.IsBackground = true;
			DaqThread.Start();
			
		}
		
		public double[] ReadAnalogs()
    	{
    		double[] retval = new double[6];
    		retval[0] = DAQ.ReadAnalog(Emant300.AIN.AIN0, Emant300.AIN.COM);
    		retval[1] = DAQ.ReadAnalog(Emant300.AIN.AIN1, Emant300.AIN.COM);
    		retval[2] = DAQ.ReadAnalog(Emant300.AIN.AIN2, Emant300.AIN.COM);
    		retval[3] = DAQ.ReadAnalog(Emant300.AIN.AIN3, Emant300.AIN.COM);
    		retval[4] = DAQ.ReadAnalog(Emant300.AIN.AIN4, Emant300.AIN.COM);
    		retval[5] = DAQ.ReadAnalog(Emant300.AIN.AIN5, Emant300.AIN.COM);
    		
    		for (int i=0; i<6; i++)
    		{
    			retval[i] = (retval[i] - MinVolt[i]) / SpanVolt(i);
    			if (retval[i] > 1)
    				retval[i] = 1;
    			else if (retval[i] < 0)
    				retval[i] = 0;
    		}
    		return retval;
    	}
		
		public int[] GetPositions()
		{
			int[] Positions = new int[6];
			double[] pots = ReadAnalogs();
			for (int joint = 0; joint <6; joint++)
				Positions[joint] = CommandMin[joint] + (int) (pots[joint] * (CommandMaximum[joint] - CommandMin[joint]));
			return Positions;
		}
		
		public int ReadSwitches()
		{
			return (DAQ.ReadDigitalPort() & 0x0F) ;
		}
		
		public void WriteLEDs(bool hyd, bool idx, bool grlk, bool cont)
		{
			if (hyd)           		
           		DAQ.WritePWM(100000, 0); // Hyd LED on
			else
           		DAQ.WritePWM(100000, 100); // Hyd LED off
			// third state not implemented
			//DAQ.WritePWM(100000, 50); // Hyd LED flash - note period out of range and invalid
			int LEDbits = 0x70; // only these three bits matter
			if (idx)
				LEDbits &= 0x30;
			if (grlk)
				LEDbits &= 0x60;
			if (cont)
				LEDbits &= 0x50;
			DAQ.WriteDigitalPort(LEDbits);
		}
	
		// probably should work a dispose in here, containing DAQ.Close();
		
				void UpdateLEDs()
		{
			WriteLEDs(HydEnabled, IndexActive, !(GripClosed), GripClosed);
		}

		
		
		void BgUpdate()
		{
			int switches;
					
			InitializeUDP();
			
			while (true)
			{
				MasterAbsolute = GetPositions();
				
				if (!HydEnabled)
				{	
					MasterAbsolute.CopyTo(UnLatchPosition,0);
					MasterAbsolute.CopyTo(LatchedPosition,0);
					MasterAbsolute.CopyTo(CumulativePosition,0);
				}
				else if (!IndexActive)
				{
					for (int i=0; i<6; i++)
						CumulativePosition[i] = LatchedPosition[i]+(MasterAbsolute[i]-UnLatchPosition[i]);
					CumulativePosition = MiniMaster.Saturate(CumulativePosition);
				}
        
				switches = ReadSwitches();
				
				if ((switches & 0x8)==0)
				{
					HydEnabled = !HydEnabled;
					UpdateLEDs();
					while ((switches & 0x8)==0)
						switches = ReadSwitches();
				}
				if ((switches & 0x4)==0)
				{
					IndexActive = !IndexActive;
					if (IndexActive)
					{
						// newly paused.
						CumulativePosition.CopyTo(LatchedPosition,0);
					}
					else
					{
						// newly restarted
						MasterAbsolute.CopyTo(UnLatchPosition,0);
					}
					UpdateLEDs();
					while ((switches & 0x4)==0)
						switches = ReadSwitches();
				}
				if ((switches & 0x1)==0)
				{
					GripClosed=false;
					UpdateLEDs();
					while ((switches & 0x1)==0)
						switches = ReadSwitches();
				}
				if ((switches & 0x2)==0)
				{
					GripClosed=true;
					UpdateLEDs();
					while ((switches & 0x2)==0)
						switches = ReadSwitches();
				}
					
				UpdateLEDs();
				EmitUDP();
			}
		}

		
		/// /////////////////// UDP STUFF //////////////////

		System.Net.Sockets.UdpClient mmSock;
		byte[] OutBuffer;

		void InitializeUDP()
		{
			// Set up a UDP socket to use for communication with the arm.
			// The UdpClient object mmSock can be used for both TX and RX.
			// The port specified here becomes the "source" for TX, if used, as well as the RX port.
			mmSock = new System.Net.Sockets.UdpClient(); // no port specified
			
			// This is where we specify the destination for TX only.
			mmSock.Connect("192.168.10.255", 12346); // broadcast, destination port
			
			//OutBuffer = new byte[] { 0x07,0x7c, 0x9b, 0x85, 0x8C, 0x0C, 0xF4, 0x12, 0, 0 };
			OutBuffer = new byte[] { 35,35,35,35,35,35,35,35,35,35 };
			
		}
		
		void EmitUDP()
		{
			byte headbyte = (byte) 'H'; // sentinel value with no hyd, no grip
			
			if (HydEnabled)
				headbyte |= 0x01; // this makes H into I
			
			if (GripClosed)
				headbyte |= 0x02; // now we have J or K
			
			OutBuffer[0] = headbyte;
			
			//OutBuffer[3]=(byte) tgt[5]; // wrist rotate
			
			OutBuffer[1]=(byte) (CumulativePosition[0]&(0xFF)); // azimuth
			OutBuffer[2]=(byte) (((CumulativePosition[0]>>8)&(0x0F)) + ((CumulativePosition[1]>>4)&0xF0));
			OutBuffer[3]=(byte) (CumulativePosition[1]&(0xFF)); // elevator
			OutBuffer[4]=(byte) (CumulativePosition[2]&(0xFF)); // elbow
			OutBuffer[5]=(byte) (((CumulativePosition[2]>>8)&0x0F) + ((CumulativePosition[3]>>4)&0xF0));
			OutBuffer[6]=(byte) (CumulativePosition[3]&(0xFF)); // pitch
			OutBuffer[7]=(byte) (CumulativePosition[4]&(0xFF)); // yaw
			OutBuffer[8]=(byte) (((CumulativePosition[4]>>8)&0x0F) + ((CumulativePosition[5]>>4)&0xF0));
			OutBuffer[9]=(byte) (CumulativePosition[5]&(0xFF)); // rotate
			
			
			// set OutBuffer
			mmSock.Send(OutBuffer, OutBuffer.Length);
		}
		
		
	}
}
    			