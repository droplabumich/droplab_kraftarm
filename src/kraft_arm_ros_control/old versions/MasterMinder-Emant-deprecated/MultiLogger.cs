/*
 * Created by SharpDevelop.
 * User: dgi
 * Date: 11/15/2007
 * Time: 7:52 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;

namespace DgiUtil
{
	/// <summary>
	/// a neat way to log timestamped data from multiple threads
	/// </summary>
	public class MultiLogger
	{
		private System.IO.TextWriter logfile;
		public readonly string FileExtension;
		private object mutex = new Object();
		private int splitInterval = 0;
		private DateTime lastEvent = DateTime.MinValue; // because we want to throw an exception if not initialized.
		
		public MultiLogger(string FileExtensionCode, int SplitInterval)
		{
			FileExtension = FileExtensionCode;
			DateTime fastNow = DateTime.UtcNow;
			
			if ((SplitInterval<0) || (SplitInterval > 24))
				throw new ArgumentOutOfRangeException("SplitInterval","Must be 0-24");
			
			this.logfile = InitLogFile();
			
			this.Log("VER",System.AppDomain.CurrentDomain.FriendlyName + " build Date is "  + GetBuildDate().ToString("yyyy-MM-dd HH:mm UTC."));
			splitInterval = SplitInterval;
			lastEvent = fastNow;
		}
		
		public MultiLogger(string FileExtensionCode)
		{
			FileExtension = FileExtensionCode;
			DateTime fastNow = DateTime.UtcNow;
			this.logfile = InitLogFile();
			this.Log("VER",System.AppDomain.CurrentDomain.FriendlyName + " build Date is "  + GetBuildDate().ToString("yyyy-MM-dd HH:mm UTC."));
			lastEvent = fastNow;
		}
		
		System.IO.TextWriter InitLogFile()
		{
			string name = DateTime.UtcNow.ToString("yyyyMMdd_HHmm.") + FileExtension;
			System.IO.FileStream strm = new System.IO.FileStream(name, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.Read);
			return new System.IO.StreamWriter(strm);
		}
		
		public void Log(string data)
		{
			this.Log(FileExtension, data);
		}
		
		public void Log(string datatype, string data)
		{
			// one thread at a time thru this method
			lock(mutex)
			{
				DateTime fastNow = DateTime.UtcNow;
				if (splitInterval > 0)
				{
					if (fastNow.Hour != lastEvent.Hour) // TODO: or if its a different day
					{
						if (   ((splitInterval == 24) && (fastNow.Hour == 0))    ||
						    ((fastNow.Hour % splitInterval) == 0)   )
						{
							// split the log file
							this.logfile.Flush();
							this.logfile.Close();
							this.logfile = InitLogFile();
						}
					}
				}
					
				this.logfile.Write(datatype + ' ');
				this.logfile.Write(fastNow.ToString("yyyy/MM/dd HH:mm:ss.fff")  + " ");
				this.logfile.WriteLine(data);
				this.logfile.Flush();
				
				lastEvent = fastNow;
			}
		}
		
		public static DateTime GetBuildDate()
		{
			Version version = System.Reflection.Assembly.GetEntryAssembly().GetName().Version;
			DateTime BuildDate =  new DateTime(2000, 1, 1).Add(new TimeSpan(
				TimeSpan.TicksPerDay * version.Build + // days since 1 January 2000
				TimeSpan.TicksPerSecond * 2 * version.Revision)); // seconds since midnight, (multiply by 2 to get original)
			return BuildDate.ToUniversalTime();
		}
		
		public static string GetBuildDateString()
		{
			return GetBuildDate().ToString("yyyy-MM-ddTHH:mm:ssZ");
		}
		
		public void Close()
		{
			this.logfile.Close();
		}
		
	}
}
