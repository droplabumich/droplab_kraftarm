/*
 * Created by SharpDevelop.
 * User: dgi
 * Date: 1/13/2011
 * Time: 3:55 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace DgiUtil
{
	/// <summary>
	/// Description of IniFile.
	/// </summary>
	public class IniFile
	{
		System.IO.Stream IniStream;
		
		public IniFile()
		{
			string searchPattern = "*.ini";
			//string InitDirectory = ".";
			string[] MyFiles = new string[10];
			//System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(InitDirectory);

			// Get Files
			MyFiles =  System.IO.Directory.GetFiles(".",searchPattern);

			int ValidFilesCount = 0;
			string ValidFileName = "";
			foreach(string s in MyFiles)
				{
						System.Diagnostics.Debug.WriteLine("Found INI file " + s);
						ValidFilesCount++;
						ValidFileName = s;
				}
			if (ValidFilesCount > 1)
				throw new ApplicationException("Only one INI file allowed. Found " + ValidFilesCount + " valid files.");
			else if (ValidFilesCount < 1)
				throw new ApplicationException("No valid INI file found in working directory.");
		
			IniStream = new System.IO.FileStream(ValidFileName,System.IO.FileMode.Open);
		}
		
		public string GetValueFromIniFile(string IniSection, string Attribute)
		{
			IniStream.Seek(0, System.IO.SeekOrigin.Begin); //rewind
			System.IO.StreamReader r = new System.IO.StreamReader(IniStream);
			string NextLine;
			char [] RmChars = {'\n','\r',' '};
			//int i=0;
			
			//find section
			while ((NextLine = r.ReadLine()) != null)
			{
				if (NextLine.StartsWith("["+IniSection+"]"))
				{
					//find property
					while ((NextLine = r.ReadLine()) != null)
					{
						if ((NextLine.StartsWith(Attribute)) && (NextLine.Contains("=")))
						{
							//return value
							string ValueLine = NextLine.Split('%')[0]; // discard comments if present
							return ValueLine.Split('=')[1].Trim(RmChars); // take the substring after the equals sign
						}
					}
				}
			}
			// never reach this point
			if (NextLine == null)
				throw new ApplicationException("Can't find " + Attribute + " in INI section " + IniSection);
			return null;
		}//GetValueFromIniFile
		
	} // inifile class
} // namespace
