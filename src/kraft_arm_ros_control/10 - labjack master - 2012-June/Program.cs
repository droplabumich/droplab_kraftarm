﻿/*
 * Created by SharpDevelop.
 * User: Daniel
 * Date: 2/2/2012
 * Time: 2:13 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace LabJackMaster
{
	class Program
	{
		static string BarGraphString(char BarChar, int DisplayVal, int MinVal, int MaxVal, int width)
		{
			System.Text.StringBuilder str = new System.Text.StringBuilder("", width);
			if (DisplayVal < MinVal)
				DisplayVal = MinVal;
			else if (DisplayVal > MaxVal)
				DisplayVal = MaxVal;
		
			for (int i = 0; i< width; i++)
			{
				if (width * DisplayVal > (width * MinVal + i*(MaxVal-MinVal)))
					str.Append(BarChar);
				else
					str.Append(' ');
			}
			return str.ToString();	
		}
		
		public static void Main(string[] args)
		{
			Console.WriteLine("MasterMinder Starting...");
			int ep = 50;
			if (args.Length == 1)
				ep = int.Parse(args[0]);
			else if (args.Length > 1)
			{
				Console.WriteLine("Only one argument allowed.");
				return;
			}
			MiniMaster mm = new MiniMaster(ep);
			
			char BarDisplayChar;
			int LeftPaneWidth = 15;
			while (true)
			{
				System.Threading.Thread.Sleep(200); // limit the display refresh rate

				mm.UpdateLEDs();
				Console.Clear();
				Console.WriteLine();
				//Console.Write("".PadRight(LeftPaneWidth));
				Console.Write("  Sw = "+ mm.switches.ToString("00"));
				Console.WriteLine("    Hyd En = " + mm.HydEnabled.ToString() + "    Halt = " + mm.IndexActive.ToString() + "   Grip Lk = "+mm.GripLock.ToString() +"   Cont Wr = " + mm.ContinuousWrist.ToString());
				Console.WriteLine();
				
				if (mm.IndexActive)
					BarDisplayChar = '-';
				else
					BarDisplayChar = '=';
				
				for (int axis = 6; axis >=0; axis--)
				{
					
					Console.Write(MiniMaster.JointNames[axis].PadLeft(LeftPaneWidth-2) + "  " );
					Console.WriteLine(BarGraphString('.', mm.MasterAbsolute[axis], MiniMaster.CommandMin[axis],  MiniMaster.CommandMaximum[axis], 46));
					//Console.Write("".PadRight(LeftPaneWidth));
					Console.Write((mm.CumulativePosition[axis].ToString("0  ")).PadLeft(LeftPaneWidth));
					Console.WriteLine(BarGraphString(BarDisplayChar, mm.CumulativePosition[axis], MiniMaster.CommandMin[axis],  MiniMaster.CommandMaximum[axis], 46));
					//Console.WriteLine((mm.Pots[axis].ToString("0.00000  ")).PadLeft(LeftPaneWidth)); // Display Raw Voltage - for testing only.
					
					Console.WriteLine();
				}
				Console.Write("UDP TX ("+MiniMaster.OutPort.ToString() + "):");
				
				
				for (int i=0; i<10; i++)
					Console.Write(":"+mm.LastOut[i].ToString("X2"));
				Console.WriteLine("  Period Nominal/Avg= " + mm.EmitPeriod + " / " + mm.PeriodAverage.ToString("0.0") + " ms");
			}
		}
	}
}