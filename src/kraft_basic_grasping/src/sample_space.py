#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import numpy as np
import time
import getch
import tf
import actionlib

from std_msgs.msg import String
from pyquaternion import Quaternion
from geometry_msgs.msg import PoseStamped, Pose, Quaternion
from std_msgs.msg import Int8
from sensor_msgs.msg import JointState
from visualization_msgs.msg import Marker
from moveit_msgs.msg import Constraints, JointConstraint, OrientationConstraint
from moveit_commander.conversions import pose_to_list
from control_msgs.msg import GripperCommandAction, GripperCommandGoal

TOPIC_DTP = 'kraft_arm/move_group/display_planned_path'
TOPIC_POSE = "kraft_arm/des_pose"  # topics for desired pose
TOPIC_POSE_DISP = "kraft_arm/pose_display"
TOPIC_GRIPPER = "kraft_arm/gripper"
TOPIC_CURREN_POSE = "kraft_arm/ee_pose"

noDOF = 7  # variable discribing number of joints
ACTION = 0

class ManipulationClass(object):
    def __init__(self, **kwargs):

        self.topic_output = kwargs.get('topic_output', TOPIC_POSE)

        self.open_gripper = 0

        self.des_joint = np.zeros(noDOF)
        self.action = ACTION
        self.index = 0

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group_name = "arm"
        group = moveit_commander.MoveGroupCommander(group_name)
        display_trajectory_publisher = rospy.Publisher(TOPIC_DTP, moveit_msgs.msg.DisplayTrajectory, queue_size=20)

        # We can get the name of the reference frame for this robot:
        planning_frame = group.get_planning_frame()
        print "============ Reference frame: %s" % planning_frame

        # We can also print the name of the end-effector link for this group:
        eef_link = group.get_end_effector_link()
        print "============ End effector: %s" % eef_link

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print "============ Robot Groups:", robot.get_group_names()

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print "=================== Printing robot state in initialization ================================"
        print robot.get_current_state()

        # Misc variables
        #self.box_name = ''
        self.robot = robot
        self.scene = scene
        self.group = group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

        self.robot_name = rospy.get_param('robot', None)

        #Loaded parameters PAYLOAD
        self.frame_id = rospy.get_param('payload/frame_id', None)
        self.grasping_group = rospy.get_param('payload/grasping_group', None)
        self.box_name = rospy.get_param('payload/object_name', "box")
        self.obj_dim_x = rospy.get_param('payload/object_dimension_x', 0.5)
        self.obj_dim_y = rospy.get_param('payload/object_dimension_y', 0.02)
        self.obj_dim_z = rospy.get_param('payload/object_dimension_z', 0.02)
        self.obj_pos_x = rospy.get_param('payload/object_pos_x', 0.225)
        self.obj_pos_y = rospy.get_param('payload/object_pos_y', 0.0)
        self.obj_pos_z = rospy.get_param('payload/object_pos_z', 0.0)
        self.obj_pos_w = rospy.get_param('payload/object_pos_w', 1.0)

        #Desired position from config file
        self.des_pose = Pose()
        self.des_pose.position.x = rospy.get_param('desired_pose/position_x')
        self.des_pose.position.y = rospy.get_param('desired_pose/position_y')
        self.des_pose.position.z = rospy.get_param('desired_pose/position_z')
        self.des_pose.orientation.x = rospy.get_param('desired_pose/orientation_x')
        self.des_pose.orientation.y = rospy.get_param('desired_pose/orientation_y')
        self.des_pose.orientation.z = rospy.get_param('desired_pose/orientation_z')
        self.des_pose.orientation.w = rospy.get_param('desired_pose/orientation_w')

        self.des_pose_place = Pose()
        self.des_pose_place.position.x = rospy.get_param('desire_pose_place/position_x')
        self.des_pose_place.position.y = rospy.get_param('desire_pose_place/position_y')
        self.des_pose_place.position.z = rospy.get_param('desire_pose_place/position_z')
        self.des_pose_place.orientation.x = rospy.get_param('desire_pose_place/orientation_x')
        self.des_pose_place.orientation.y = rospy.get_param('desire_pose_place/orientation_y')
        self.des_pose_place.orientation.z = rospy.get_param('desire_pose_place/orientation_z')
        self.des_pose_place.orientation.w = rospy.get_param('desire_pose_place/orientation_w')

        self.des_pose_end = Pose()
        self.des_pose_end.position.x = rospy.get_param('desire_pose_end/position_x')
        self.des_pose_end.position.y = rospy.get_param('desire_pose_end/position_y')
        self.des_pose_end.position.z = rospy.get_param('desire_pose_end/position_z')
        self.des_pose_end.orientation.x = rospy.get_param('desire_pose_end/orientation_x')
        self.des_pose_end.orientation.y = rospy.get_param('desire_pose_end/orientation_y')
        self.des_pose_end.orientation.z = rospy.get_param('desire_pose_end/orientation_z')
        self.des_pose_end.orientation.w = rospy.get_param('desire_pose_end/orientation_w')

        #Line defined by 2 points
        self.left_point = np.zeros(3)
        self.left_point[0] = rospy.get_param('line/left_pos_x')
        self.left_point[1] = rospy.get_param('line/left_pos_y')
        self.left_point[2] = rospy.get_param('line/left_pos_z')

        self.right_point = np.zeros(3)
        self.right_point[0] = rospy.get_param('line/right_pos_x')
        self.right_point[1] = rospy.get_param('line/right_pos_y')
        self.right_point[2] = rospy.get_param('line/right_pos_z')

        # ros interface
        self.sub_des_pose = rospy.Subscriber(TOPIC_POSE, PoseStamped, self.handle_des_pose, tcp_nodelay=True,
                                             queue_size=10)
        # subscriber for defined position
        self.pub_rviz_marker = rospy.Publisher(TOPIC_POSE_DISP, Marker, tcp_nodelay=True, queue_size=100)
        self.pub_gripper = rospy.Publisher(TOPIC_GRIPPER, Int8, tcp_nodelay=True, queue_size=100)
        self.pub_ee_pose = rospy.Publisher(TOPIC_CURREN_POSE, PoseStamped, tcp_nodelay=True, queue_size=100)

        if (self.robot_name == "fetch"):
            self.client = actionlib.SimpleActionClient("/gripper_controller/gripper_action", GripperCommandAction)
            rospy.loginfo("Waiting for gripper_controller...")
            self.client.wait_for_server()


##### function handle for initial desired position (in case sent by the image_processing interface)
    def handle_des_pose(self, data):
        self.des_pose.position.x = data.pose.position.x
        self.des_pose.position.y = data.pose.position.y
        self.des_pose.position.z = data.pose.position.z
        self.des_pose.orientation.x = data.pose.orientation.x
        self.des_pose.orientation.y = data.pose.orientation.x
        self.des_pose.orientation.z = data.pose.orientation.z
        self.des_pose.orientation.w = data.pose.orientation.w

    def send_gripper_command(self):
        self.gripper = Int8()
        self.gripper.data = self.open_gripper
        self.pub_gripper.publish(self.gripper)

    def send_ee_pose(self, current_pose):
        self.curren_ee_pose = PoseStamped()
        self.curren_ee_pose.header.stamp = rospy.get_rostime()
        self.curren_ee_pose.pose = current_pose
        self.pub_ee_pose.publish(self.curren_ee_pose)

    def open_gripper_kraft(self):
        self.open_gripper = 1
        self.send_gripper_command()

    def close_gripper_kraft(self):
        self.open_gripper = 0
        self.send_gripper_command()

###### FUNCTIONS SPECIFIC TO FETCH FOR OPENING AND CLOSE THE GRIPPER
    def close_gripper_fetch(self):
        goal = GripperCommandGoal()
        goal.command.position = 0.0
        goal.command.max_effort = 100
        self.client.send_goal(goal)
        self.client.wait_for_result()

    def open_gripper_fetch(self):
        goal = GripperCommandGoal()
        goal.command.position = 0.5
        goal.command.max_effort = 100
        self.client.send_goal(goal)
        self.client.wait_for_result()

###### COMPUTING THE NORMAL TO THE LINE PROVIDED IN THE .yaml FILE
    # The algorithm follows the following steps:
    # 1. comuting the perpendicular from the current end-effector configuration to the vector defined by the 2 points from the yaml file.
    # 2. define a vector in the x-y plane passing through the point where the perpendicular intersect the vector in step 1
    # 3. compute the normal from the vector defined in step 1 and the one defined in step 2
    # 4. compute the orientation of the end-effector w.r.t the normal defned in 3, the position being computed in step 1
    def normalize(self, data):
        if (len(data)<4):
            return np.sqrt(data[0]**2+data[1]**2+data[2]**2)
        else:
            return np.sqrt(data[0] ** 2 + data[1] ** 2 + data[2] ** 2 + data[3] ** 2)

    def normal_to_line(self):
        group = self.group
        current_state = group.get_current_pose().pose
        dist_points = np.sqrt((self.right_point[0] -self.left_point[0])**2+(self.right_point[1] -self.left_point[1])**2+(self.right_point[2] -self.left_point[2])**2)
        direction_line = (self.right_point - self.left_point)/dist_points

        dist_ee = np.zeros(3)
        dist_ee[0] = -current_state.position.x + self.right_point[0]
        dist_ee[1] = -current_state.position.y + self.right_point[1]
        dist_ee[2] = -current_state.position.z + self.right_point[2]

        delta_t = dist_ee[0]*direction_line[0] + dist_ee[1]*direction_line[1] + dist_ee[2]*direction_line[2]

        point_to_grab = self.right_point - delta_t*dist_ee

        point_to_grab_line = np.zeros(3)
        point_to_grab_line[0] = point_to_grab[0] + 0.1
        point_to_grab_line[1] = point_to_grab[1] - 0.1
        point_to_grab_line[2] = point_to_grab[2]

        dist_projection = np.sqrt((point_to_grab_line[0] - point_to_grab[0])**2+(point_to_grab_line[1]-point_to_grab[1])**2+(point_to_grab_line[2]-point_to_grab[2])**2)

        direction_projection = np.zeros(3)
        direction_projection[0] = (point_to_grab_line[0] - point_to_grab[0]) / dist_projection
        direction_projection[1] = (point_to_grab_line[1] - point_to_grab[1]) / dist_projection
        direction_projection[2] = (point_to_grab_line[2] - point_to_grab[2]) / dist_projection

        orientation_quat = np.zeros(4)
        noormalized_line = direction_line/self.normalize(direction_line)
        normalized_projection = direction_projection/self.normalize(direction_projection)
        orientation_quat[0:3] = np.cross(noormalized_line, normalized_projection)
        #print orientation_quat[0:3]

        #################### version 1 ############################################
        # orientation_quat[3] =  np.dot(noormalized_line, normalized_projection) + np.sqrt((dist_points ** 2) * (dist_projection ** 2))
        # orientation_quat = orientation_quat/self.normalize(orientation_quat)

        ################ version 2 #########################################
        tz = np.array(orientation_quat[0:3])
        tx = np.array(np.cross(tz, np.array([0, 1, 0]).transpose()))
        ty = np.array(np.cross(tx,tz))
        R = np.zeros((4,4))
        R[0, 0] = tx[0]
        R[0, 1] = ty[0]
        R[0, 2] = tz[0]
        R[1, 0] = tx[1]
        R[1, 1] = ty[1]
        R[1, 2] = tz[1]
        R[2, 0] = tx[2]
        R[2, 1] = ty[2]
        R[2, 2] = tz[2]
        R[3, 3] = 1
        orientation_quat = tf.transformations.quaternion_from_matrix(R)
        self.des_pose.position.x = point_to_grab[0]
        self.des_pose.position.y = point_to_grab[1]
        self.des_pose.position.z = point_to_grab[2]
        self.des_pose.orientation.x = tx[0]
        self.des_pose.orientation.y = ty[1]
        self.des_pose.orientation.z = tz[2]
        self.des_pose.orientation.w = orientation_quat[3]

    def go_normal_to_object(self):
        group = self.group
        self.normal_to_line()
        cartesian_plan, fraction = self.plan_cartesian_normal()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        rospy.sleep(2)
        if (self.robot_name == "fetch"):
            rospy.loginfo("Close the gripper")
            self.close_gripper_fetch()
        else:
            rospy.loginfo("Close the gripper KRAFT")
            self.close_gripper_kraft()



#### FUNCTION TO ENSURE THE OBJECT HAS BEEN ADDED/DELETED FROM THE PLANNING SCENE
    def wait_for_state_update(self, box_name, scene, box_is_known=False, box_is_attached=False, timeout=4):
        start = rospy.get_time()
        seconds = rospy.get_time()
        while (seconds - start < timeout) and not rospy.is_shutdown():
            attached_objects = scene.get_attached_objects([box_name])
            is_attached = len(attached_objects.keys()) > 0

            is_known = box_name in scene.get_known_object_names()

            if (box_is_attached == is_attached) and (box_is_known == is_known):
                return True

        rospy.sleep(0.1)
        seconds = rospy.get_time()

        return False

#### FUNCTION TO MOVE THE ARM
    def execute_plan(self, plan):
        group = self.group
        group.execute(plan, wait=True)

### GENERAL FUNCTION TO PLAN THE PATH OF THE ROBOT (NO RESTRICTIONS)
    def plan_cartesian_path(self, scale=1):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.001)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = self.des_pose
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        return plan, fraction

    def plan_cartesian_goal(self):
        if (self.robot_name == "fetch"):
            rospy.loginfo("Open the gripper")
            self.open_gripper_fetch()
        else:
            rospy.loginfo("Open the gripper KRAFT")
            self.open_gripper_kraft()
        cartesian_plan, fraction = self.plan_cartesian_path()
        rospy.sleep(2)
        self.execute_plan(cartesian_plan)
        group = self.group
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        rospy.sleep(2)

# FUNCTION TO COMPUTE THE PATH WHEN THE NORMAL TO THE PLANE HAS TO BE ACHIEVED!
    # an intermediate waypoint is added to be sure the arm doesn't hit the objects
    def plan_cartesian_normal(self, scale=1):
        group = self.group
        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.01)
        group.set_goal_orientation_tolerance(0.001)
        group.set_start_state_to_current_state()
        waypoints = []
        wpose = self.des_pose
        waypoints.append(copy.deepcopy(wpose))
        wpose.position.z = wpose.position.z - 0.2
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0)  # jump_threshold

        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)
        return plan, fraction

# FUNCTION TO COMPUTE THE PATH WHEN THE OBJECT IS ATTACHED TO THE END-EFFECTOR. NO ORIENTATION RESTRICTIONS
    def plan_cartesian_object_in_scene(self):
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group = self.group
        timeout = 5

        box_pose = PoseStamped()
        box_pose.header.frame_id = self.frame_id #"gripper_link" #"Inner_Grip" - Kraft
        box_pose.pose.orientation.w = self.obj_pos_w
        box_pose.pose.position.x = self.obj_pos_x
        box_pose.pose.position.y = self.obj_pos_y
        box_name = self.box_name
        self.wait_for_state_update(box_name, scene, box_is_known=False, timeout=timeout)

        scene.add_box(box_name, box_pose, size=(self.obj_dim_x, self.obj_dim_y, self.obj_dim_z)) #
        grasping_group = self.grasping_group #'gripper'
        touch_links = robot.get_link_names(group=grasping_group)
        scene.attach_box(self.eef_link, box_name, touch_links=touch_links)
        self.wait_for_state_update(box_name, scene, box_is_attached=True, box_is_known=False, timeout=timeout)

        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.1)
        group.set_start_state_to_current_state()

        waypoints = []
        wpose = self.des_pose_place

        waypoints.append(copy.deepcopy(wpose))
        rospy.sleep(2)
        (plan_object, fraction_object) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                        0.01,  # eef_step
                                                        0.0,
                                                        avoid_collisions=False)  # jump_threshold
        rospy.sleep(2)
        self.execute_plan(plan_object)
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)

        rospy.loginfo("=========== Remove object from the scene/releasing the object ===========")
        scene.remove_attached_object(self.eef_link, box_name)
        self.wait_for_state_update(box_name, scene, box_is_attached=False, box_is_known=True, timeout=timeout)
        scene.remove_world_object(box_name)
        self.wait_for_state_update(box_name, scene, box_is_attached=False, box_is_known=False, timeout=timeout)

# FUNCTION TO COMPUTE THE PATH WHEN THE OBJECT IS ATTACHED TO THE END-EFFECTOR. WITH ORIENTATION RESTRICTIONS
    # MUTIPLE WAYPOINTS TO FOLLOW
    def plan_cartesian_object_orientated_in_scene(self):
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        # Define constraints
        self.upright_constraints = Constraints()
        joint_constraint = JointConstraint()

        group = self.group
        timeout = 5

        box_pose = PoseStamped()
        box_pose.header.frame_id = self.frame_id #"gripper_link" #"Inner_Grip" - Kraft
        box_pose.pose.orientation.w = self.obj_pos_w
        box_pose.pose.position.x = self.obj_pos_x
        box_pose.pose.position.y = self.obj_pos_y
        box_name = self.box_name
        self.wait_for_state_update(box_name, scene, box_is_known=False, timeout=timeout)

        try:
            scene.add_box(box_name, box_pose, size=(self.obj_dim_x, self.obj_dim_y, self.obj_dim_z))  #
            grasping_group = self.grasping_group  # 'gripper'
            touch_links = robot.get_link_names(group=grasping_group)
            scene.attach_box(self.eef_link, box_name, touch_links=touch_links)
            self.wait_for_state_update(box_name, scene, box_is_attached=True, box_is_known=False, timeout=timeout)
            success = True
        except:
            print("Could not attach object. Try something else ....")
            success = False


        if success == True:
            # 2. Constraints to end-effector orientation
            orientation_constraint = OrientationConstraint()
            # orientation_constraint.header = wpose.header
            orientation_constraint.link_name = group.get_end_effector_link()
            orientation_constraint.orientation = Quaternion(self.des_pose.orientation.x, self.des_pose.orientation.y,
                                                            self.des_pose.orientation.z, self.des_pose.orientation.w)
            orientation_constraint.absolute_x_axis_tolerance = 0.2
            orientation_constraint.absolute_y_axis_tolerance = 0.2
            orientation_constraint.absolute_z_axis_tolerance = 0.2
            orientation_constraint.weight = 1

            self.upright_constraints.name = "upright_movement"
            self.upright_constraints.orientation_constraints.append(orientation_constraint)

            group.allow_replanning(True)
            group.set_pose_reference_frame(self.planning_frame)
            group.set_goal_position_tolerance(0.1)
            group.set_goal_orientation_tolerance(0.2)
            group.set_start_state_to_current_state()
            group.set_path_constraints(self.upright_constraints)

            waypoints = []
            wpose = Pose()
            wpose.position.x = self.des_pose_place.position.x
            wpose.position.y = self.des_pose_place.position.y
            wpose.position.z = self.des_pose_place.position.z
            wpose.orientation.x = self.des_pose.orientation.x
            wpose.orientation.y = self.des_pose.orientation.y
            wpose.orientation.z = self.des_pose.orientation.z
            wpose.orientation.w = self.des_pose.orientation.w

            # wpose = self.des_pose_place
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z -= 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z += 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.y += 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z -= 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z += 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.y += 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z -= 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z += 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.y -= 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z -= 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.z += 0.1
            waypoints.append(copy.deepcopy(wpose))

            wpose.position.y -= 0.1
            waypoints.append(copy.deepcopy(wpose))

            rospy.sleep(2)
            (plan_object, fraction_object) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                                              0.01,  # eef_step
                                                                              0.0,
                                                                          avoid_collisions=False)
            rospy.loginfo("Path computed successfully. Moving the arm.")
            rospy.sleep(2)
            self.execute_plan(plan_object)
            current_pose = group.get_current_pose().pose
            self.send_ee_pose(current_pose)
        return box_name, scene

# FUNCTION TO COMPUTE THE PATH WHEN THE OBJECT IS ATTACHED TO THE END-EFFECTOR. WITH ORIENTATION RESTRICTIONS
    # SINGLE WAYPOINTS TO FOLLOW
    def plan_cartesian_goal_object_orientated_in_scene(self):
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        # Define constraints
        self.upright_constraints = Constraints()
        joint_constraint = JointConstraint()

        group = self.group
        timeout = 5

        box_pose = PoseStamped()
        box_pose.header.frame_id = self.frame_id #"gripper_link" #"Inner_Grip" - Kraft
        box_pose.pose.orientation.w = self.obj_pos_w
        box_pose.pose.position.x = self.obj_pos_x
        box_pose.pose.position.y = self.obj_pos_y
        box_name = self.box_name
        self.wait_for_state_update(box_name, scene, box_is_known=False, timeout=timeout)

        try:
            scene.add_box(box_name, box_pose, size=(self.obj_dim_x, self.obj_dim_y, self.obj_dim_z))  #
            grasping_group = self.grasping_group  # 'gripper'
            touch_links = robot.get_link_names(group=grasping_group)
            scene.attach_box(self.eef_link, box_name, touch_links=touch_links)
            self.wait_for_state_update(box_name, scene, box_is_attached=True, box_is_known=False, timeout=timeout)
            success = True
        except:
            print("Could not attach object. Try something else ....")
            success = False


        if success == True:
            # 2. Constraints to end-effector orientation
            orientation_constraint = OrientationConstraint()
            # orientation_constraint.header = wpose.header
            orientation_constraint.link_name = group.get_end_effector_link()
            orientation_constraint.orientation = Quaternion(self.des_pose.orientation.x, self.des_pose.orientation.y,
                                                            self.des_pose.orientation.z, self.des_pose.orientation.w)
            orientation_constraint.absolute_x_axis_tolerance = 0.2
            orientation_constraint.absolute_y_axis_tolerance = 0.2
            orientation_constraint.absolute_z_axis_tolerance = 0.2
            orientation_constraint.weight = 1

            self.upright_constraints.name = "upright_movement"
            self.upright_constraints.orientation_constraints.append(orientation_constraint)

            group.allow_replanning(True)
            group.set_pose_reference_frame(self.planning_frame)
            group.set_goal_position_tolerance(0.1)
            group.set_goal_orientation_tolerance(0.2)
            group.set_start_state_to_current_state()
            group.set_path_constraints(self.upright_constraints)

            waypoints = []
            wpose = Pose()
            wpose.position.x = self.des_pose_place.position.x
            wpose.position.y = self.des_pose_place.position.y
            wpose.position.z = self.des_pose_place.position.z
            wpose.orientation.x = self.des_pose.orientation.x
            wpose.orientation.y = self.des_pose.orientation.y
            wpose.orientation.z = self.des_pose.orientation.z
            wpose.orientation.w = self.des_pose.orientation.w

            waypoints.append(copy.deepcopy(wpose))
            rospy.sleep(2)
            (plan_object, fraction_object) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                                              0.01,  # eef_step
                                                                              0.0,
                                                                          avoid_collisions=False)
            rospy.loginfo("Path computed successfully. Moving the arm.")
            rospy.sleep(2)
            self.execute_plan(plan_object)
            current_pose = group.get_current_pose().pose
            self.send_ee_pose(current_pose)
        return box_name, scene


# FUNCTION TO PLACE THE SAMPLE BACK TO THE TRAY. WITH ORIENTATION RESTRICTIONS
    def place_sample(self, scene, box_name):
        self.des_pose_end.position.x = rospy.get_param('desire_pose_end/position_x')
        self.des_pose_end.position.y = rospy.get_param('desire_pose_end/position_y')
        self.des_pose_end.position.z = rospy.get_param('desire_pose_end/position_z')
        self.des_pose_end.orientation.x = self.des_pose.orientation.x
        self.des_pose_end.orientation.y = self.des_pose.orientation.y
        self.des_pose_end.orientation.z = self.des_pose.orientation.z
        self.des_pose_end.orientation.w = self.des_pose.orientation.w

        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        # Define constraints
        self.upright_constraints = Constraints()
        joint_constraint = JointConstraint()

        group = self.group
        timeout = 5

        # 2. Constraints to end-effector orientation
        orientation_constraint = OrientationConstraint()
        # orientation_constraint.header = wpose.header
        orientation_constraint.link_name = group.get_end_effector_link()
        orientation_constraint.orientation = Quaternion(self.des_pose_end.orientation.x, self.des_pose_end.orientation.y,
                                                        self.des_pose_end.orientation.z, self.des_pose_end.orientation.w)
        orientation_constraint.absolute_x_axis_tolerance = 0.5
        orientation_constraint.absolute_y_axis_tolerance = 0.5
        orientation_constraint.absolute_z_axis_tolerance = 0.5
        orientation_constraint.weight = 1

        self.upright_constraints.name = "upright_movement"
        self.upright_constraints.orientation_constraints.append(orientation_constraint)

        group.allow_replanning(True)
        group.set_pose_reference_frame(self.planning_frame)
        group.set_goal_position_tolerance(0.1)
        group.set_goal_orientation_tolerance(0.2)
        group.set_start_state_to_current_state()
        group.set_path_constraints(self.upright_constraints)

        waypoints = []
        wpose = Pose()
        wpose.position.x = self.des_pose_end.position.x
        wpose.position.y = self.des_pose_end.position.y
        wpose.position.z = self.des_pose_end.position.z
        wpose.orientation.x = self.des_pose_end.orientation.x
        wpose.orientation.y = self.des_pose_end.orientation.y
        wpose.orientation.z = self.des_pose_end.orientation.z
        wpose.orientation.w = self.des_pose_end.orientation.w

        waypoints.append(copy.deepcopy(wpose))


        rospy.sleep(2)
        (plan_object, fraction_object) = group.compute_cartesian_path(waypoints,  # waypoints to follow
                                                                    0.005,  # eef_step
                                                                    0.0,
                                                                    avoid_collisions=False)  # jump_threshold

        rospy.loginfo("Path computed successfully. Moving the arm.")
        rospy.sleep(2)
        self.execute_plan(plan_object)
        current_pose = group.get_current_pose().pose
        self.send_ee_pose(current_pose)

        rospy.loginfo("=========== Remove object from the scene/releasing the object ===========")
        scene.remove_attached_object(self.eef_link, box_name)
        self.wait_for_state_update(box_name, scene, box_is_attached=False, box_is_known=True, timeout=timeout)
        scene.remove_world_object(box_name)
        self.wait_for_state_update(box_name, scene, box_is_attached=False, box_is_known=False, timeout=timeout)

        if (self.robot_name == "fetch"):
            rospy.loginfo("Open the gripper")
            self.open_gripper_fetch()


    def pick_and_place(self):
        # go in the proximity of the object
        rospy.loginfo("Complete here if you want full autonomy .....")

##### MAIN LOOP OF THE PROGRAM
    def loop(self):
        rate = rospy.Rate(10)  # 10hz
        scene = ''
        box_name = ''
        while not rospy.is_shutdown():
            # group = self.group
            # current_pose = group.get_current_pose().pose
            # self.send_ee_pose(current_pose)
            print '============ Press one of the folllowing numbers, Then press Enter ... '
            print '===== 1 for going in the proximity of object'
            print '===== 2 for going normal to the object and picking it'
            print '===== 3 for single point sampling, keeping orientation '
            print '===== 4 for multiple point sampling keeping orientation '
            print '===== 5 single point free movement '
            print '===== 6 for placing the sample. '
            userinput = raw_input().lower()
            if (userinput[0] == '1'):
                self.des_pose.position.x = rospy.get_param('desired_pose/position_x')
                self.des_pose.position.y = rospy.get_param('desired_pose/position_y')
                self.des_pose.position.z = rospy.get_param('desired_pose/position_z')
                self.des_pose.orientation.x = rospy.get_param('desired_pose/orientation_x')
                self.des_pose.orientation.y = rospy.get_param('desired_pose/orientation_y')
                self.des_pose.orientation.z = rospy.get_param('desired_pose/orientation_z')
                self.des_pose.orientation.w = rospy.get_param('desired_pose/orientation_w')
                joint_tollerance = self.plan_cartesian_goal()
            elif(userinput[0] == '2'):
                joint_tollerance = self.go_normal_to_object()
            elif (userinput[0] == '3'):
                joint_tollerance = self.plan_cartesian_goal_object_orientated_in_scene()
            elif (userinput[0] == '4'):
                box_name, scene = self.plan_cartesian_object_orientated_in_scene()
            elif (userinput[0] == '5'):
                joint_tollerance = self.plan_cartesian_object_in_scene()
            elif (userinput[0] == '6'):
                joint_tollerance = self.place_sample(scene,box_name)
            else:
                print('Please press 1, 2, 3, 4, 5 or 6. You entered an unkown command')

            rate.sleep()


def main():
    try:
        planning = ManipulationClass()
        planning.loop()
        print "===================== Mission complete!===================================="
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == '__main__':
    main()

