(cl:in-package mocap_optitrack-msg)
(cl:export '(HEADER-VAL
          HEADER
          ID-VAL
          ID
          NAME-VAL
          NAME
          TRACKING_VALID-VAL
          TRACKING_VALID
          MRK_MEAN_ERROR-VAL
          MRK_MEAN_ERROR
          POSE-VAL
          POSE
          MARKERS-VAL
          MARKERS
))