#include <kraft_basic_grasping/pcl_reconstruct_filter.h>

// ROS parameter loading
#include <rosparam_shortcuts/rosparam_shortcuts.h>


pcl::ReconstructFilter::ReconstructFilter(ros::NodeHandle &nh, urdf::Model *urdf_model)
  : name_("pcl_reconstruct_filter")
  , nh_(nh)
{
  // Check if the URDF model needs to be loaded
  if (urdf_model == NULL)
    loadURDF("robot_description");
  else
    urdf_model_ = urdf_model;

  // Load rosparams
  ros::NodeHandle rpnh(nh_, name_);
  std::size_t error = 0;
  error += !rosparam_shortcuts::get(name_, rpnh, "links", link_names_);
  rosparam_shortcuts::shutdownIfError(name_, error);

  num_links_ = link_names_.size();

  // Init subscriber
  sub_ = nh.subscribe<PointCloud>("points2", 1, Callback);
}


void pcl::ReconstructFilter::Callback(const PointCloud::ConstPtr& msg)
{
  printf ("Cloud: width = %d, height = %d\n", msg->width, msg->height);
  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
    printf ("\t(%f, %f, %f)\n", pt.x, pt.y, pt.z);
}


void pcl::ReconstructFilter::loadURDF(std::string param_name)
{
  std::string urdf_string;
  urdf_model_ = new urdf::Model();

  // search and wait for robot_description on param server
  while (urdf_string.empty() && ros::ok())
  {
    std::string search_param_name;
    if (nh_.searchParam(param_name, search_param_name))
    {
      ROS_INFO_STREAM_NAMED(name_, "Waiting for model URDF on the ROS param server at location: " <<
                            nh_.getNamespace() << search_param_name);
      nh_.getParam(search_param_name, urdf_string);
    }
    else
    {
      ROS_INFO_STREAM_NAMED(name_, "Waiting for model URDF on the ROS param server at location: " <<
                            nh_.getNamespace() << param_name);
      nh_.getParam(param_name, urdf_string);
    }

    usleep(100000);
  }

  if (!urdf_model_->initString(urdf_string))
    ROS_ERROR_STREAM_NAMED(name_, "Unable to load URDF model");
  else
    ROS_DEBUG_STREAM_NAMED(name_, "Received URDF from param server");
}


void pcl::ReconstructFilter::loadLinkHulls()
{
  // Get limits from URDF
  if (urdf_model_ == NULL)
  {
    ROS_WARN_STREAM_NAMED(name_, "No URDF model loaded, unable to load links");
    return;
  }

  const boost::shared_ptr<const urdf::Link> link;
  std::string mesh_file;
	pcl::PolygonMesh mesh;
	int status;

  // Load links
  for (std::size_t link_id = 0; link_id < num_links_; ++link_id)
  {
    ROS_DEBUG_STREAM_NAMED(name_, "Loading link name: " << link_names_[link_id]);

    link = urdf_model_->getLink(link_names_[link_id]);
    mesh_file = link->visual->geometry->mesh->filename;
    status = loadPolygonFile(mesh_file, mesh);
    if (status == 0)
    {
    	ROS_WARN_STREAM_NAMED(name_, "Unable to load links");
    	return;
    }
    
	}

}

int pcl::ReconstructFilter::loadPolygonFile(const std::string &file_name, pcl::PolygonMesh& mesh)
{
  std::string extension = file_name.substr(file_name.find_last_of('.') + 1);
  boost::algorithm::to_lower(extension);

  if (extension == "ply")
   return (pcl::io::loadPolygonFilePLY (file_name, mesh));
  else if (extension == "obj")
    return (pcl::io::loadPolygonFileOBJ (file_name, mesh));
  else if (extension == "stl" )
    return (pcl::io::loadPolygonFileSTL (file_name, mesh));
  else
  {
    PCL_ERROR ("[pcl::ReconstructFilter::loadPolygonFile]: Unsupported file type (%s)\n", extension.c_str ());
    return (0);
  }
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "pcl_reconstruct_filter");
  ros::NodeHandle nh_;
  pcl::ReconstructFilter(nh)
  ros::spin();
}
