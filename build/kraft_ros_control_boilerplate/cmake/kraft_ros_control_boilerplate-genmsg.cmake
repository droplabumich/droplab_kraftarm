# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "kraft_ros_control_boilerplate: 1 messages, 0 services")

set(MSG_I_FLAGS "-Ikraft_ros_control_boilerplate:/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(kraft_ros_control_boilerplate_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" NAME_WE)
add_custom_target(_kraft_ros_control_boilerplate_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "kraft_ros_control_boilerplate" "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" "std_msgs/MultiArrayDimension:std_msgs/Float64MultiArray:std_msgs/Header:std_msgs/MultiArrayLayout"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(kraft_ros_control_boilerplate
  "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Float64MultiArray.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kraft_ros_control_boilerplate
)

### Generating Services

### Generating Module File
_generate_module_cpp(kraft_ros_control_boilerplate
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kraft_ros_control_boilerplate
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(kraft_ros_control_boilerplate_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(kraft_ros_control_boilerplate_generate_messages kraft_ros_control_boilerplate_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" NAME_WE)
add_dependencies(kraft_ros_control_boilerplate_generate_messages_cpp _kraft_ros_control_boilerplate_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kraft_ros_control_boilerplate_gencpp)
add_dependencies(kraft_ros_control_boilerplate_gencpp kraft_ros_control_boilerplate_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kraft_ros_control_boilerplate_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(kraft_ros_control_boilerplate
  "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Float64MultiArray.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kraft_ros_control_boilerplate
)

### Generating Services

### Generating Module File
_generate_module_eus(kraft_ros_control_boilerplate
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kraft_ros_control_boilerplate
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(kraft_ros_control_boilerplate_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(kraft_ros_control_boilerplate_generate_messages kraft_ros_control_boilerplate_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" NAME_WE)
add_dependencies(kraft_ros_control_boilerplate_generate_messages_eus _kraft_ros_control_boilerplate_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kraft_ros_control_boilerplate_geneus)
add_dependencies(kraft_ros_control_boilerplate_geneus kraft_ros_control_boilerplate_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kraft_ros_control_boilerplate_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(kraft_ros_control_boilerplate
  "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Float64MultiArray.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kraft_ros_control_boilerplate
)

### Generating Services

### Generating Module File
_generate_module_lisp(kraft_ros_control_boilerplate
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kraft_ros_control_boilerplate
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(kraft_ros_control_boilerplate_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(kraft_ros_control_boilerplate_generate_messages kraft_ros_control_boilerplate_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" NAME_WE)
add_dependencies(kraft_ros_control_boilerplate_generate_messages_lisp _kraft_ros_control_boilerplate_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kraft_ros_control_boilerplate_genlisp)
add_dependencies(kraft_ros_control_boilerplate_genlisp kraft_ros_control_boilerplate_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kraft_ros_control_boilerplate_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(kraft_ros_control_boilerplate
  "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Float64MultiArray.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kraft_ros_control_boilerplate
)

### Generating Services

### Generating Module File
_generate_module_nodejs(kraft_ros_control_boilerplate
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kraft_ros_control_boilerplate
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(kraft_ros_control_boilerplate_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(kraft_ros_control_boilerplate_generate_messages kraft_ros_control_boilerplate_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" NAME_WE)
add_dependencies(kraft_ros_control_boilerplate_generate_messages_nodejs _kraft_ros_control_boilerplate_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kraft_ros_control_boilerplate_gennodejs)
add_dependencies(kraft_ros_control_boilerplate_gennodejs kraft_ros_control_boilerplate_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kraft_ros_control_boilerplate_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(kraft_ros_control_boilerplate
  "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Float64MultiArray.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kraft_ros_control_boilerplate
)

### Generating Services

### Generating Module File
_generate_module_py(kraft_ros_control_boilerplate
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kraft_ros_control_boilerplate
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(kraft_ros_control_boilerplate_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(kraft_ros_control_boilerplate_generate_messages kraft_ros_control_boilerplate_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/kraft/workspace/droplab_kraftarm/src/kraft_ros_control_boilerplate/msg/Float64MultiArrayStamped.msg" NAME_WE)
add_dependencies(kraft_ros_control_boilerplate_generate_messages_py _kraft_ros_control_boilerplate_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(kraft_ros_control_boilerplate_genpy)
add_dependencies(kraft_ros_control_boilerplate_genpy kraft_ros_control_boilerplate_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS kraft_ros_control_boilerplate_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kraft_ros_control_boilerplate)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/kraft_ros_control_boilerplate
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(kraft_ros_control_boilerplate_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kraft_ros_control_boilerplate)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/kraft_ros_control_boilerplate
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(kraft_ros_control_boilerplate_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kraft_ros_control_boilerplate)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/kraft_ros_control_boilerplate
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(kraft_ros_control_boilerplate_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kraft_ros_control_boilerplate)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/kraft_ros_control_boilerplate
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(kraft_ros_control_boilerplate_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kraft_ros_control_boilerplate)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kraft_ros_control_boilerplate\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/kraft_ros_control_boilerplate
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(kraft_ros_control_boilerplate_generate_messages_py std_msgs_generate_messages_py)
endif()
