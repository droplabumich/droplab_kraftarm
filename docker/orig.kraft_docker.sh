#! /bin/bash

xhost +
nvidia-docker run -it \
	-v /home/kraft/workspace:/home/workspace:rw \
	--env QT_X11_NO_MITSHM=1 \
	--env="DISPLAY" \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	kraft_arm:dockerfile
